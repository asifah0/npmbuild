import React, { Component, useState } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Logout } from './Logout';
import './custom.css'
import Login from './components/Login/Login';
import { ClientDashboard } from './components/ClientPortal/Dashboard/Dashboard';
import { RemediationManager } from './components/ClientPortal/Remediations/RemediationManager';
import { RemediationDetails } from './components/Shared/RemediationDetails';
import { SubmitFiles } from './components/ClientPortal/SubmitFiles/SubmitFiles';
import { Reporting } from './components/ClientPortal/Reporting/Reporting';
import { ConsumerSearchManager } from './components/ClientPortal/ConsumerSearch/ConsumerSearchManager';
import LoginApp from './LoginApp';
import configData from './components/Shared/Util/config.json';

function App() {

    const [token, setToken] = useState();
    const [flagTimedOut, setTimeoutFlag] = useState(0);

    const getQueryVariable = (variable) => {
        let query = window.location.search.substring(1);
        let vars = query.split("&");
        for (let i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) { return pair[1]; }
        }
        return (false);
    }

    if (flagTimedOut == 1) {
        return <Login setToken={setToken} setTimeoutFlag={setTimeoutFlag} flag="1"  />
    }

    if (!token) {
        const queryParams = new URLSearchParams(window.location.search);
        if (queryParams.has('code')) {
            let code = getQueryVariable('code');
            queryParams.delete('code');
            return <Login setToken={setToken} setTimeoutFlag={setTimeoutFlag} flag="0" authCode={code} />
        }
        else {
            let oldToken = localStorage.getItem('token');
            if (oldToken) {
                return <Login setToken={setToken} setTimeoutFlag={setTimeoutFlag} flag="0" />
            }
            else {
                let loginURL = configData.LOGIN_URL;
                window.location.href = loginURL;
                return;
            }
        }
    }

    

    
    return (
        <>
            <LoginApp></LoginApp>
            <Layout>

                <Route path="/logout" component={Logout} />
                <Route exact path='/clientdashboard' component={ClientDashboard} />
                <Route exact path='/remediations' component={RemediationManager} />
                <Route exact path='/reporting' component={Reporting} />
                <Route exact path='/submitfiles' component={SubmitFiles} />
                <Route exact path='/remediations/:id' component={RemediationDetails} />
                <Route exact path='/consumersearch' component={ConsumerSearchManager} />
                <Route exact path='/' component={ClientDashboard} />
            </Layout>

        </>
    );
}

export default App;


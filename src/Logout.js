import React, { Component, useState } from 'react';
import CommonDataManager from './CommonDataManager';
import { Redirect } from 'react-router-dom';
import { Link } from "react-router-dom";
import axios from 'axios';
import { NavDropdown } from 'react-bootstrap';

export class Logout extends Component {


    constructor(props) {
        super(props)

    }


    handleLogout() {
        localStorage.removeItem('token');
        localStorage.removeItem('tokendec');
        localStorage.clear();
        let commonData = CommonDataManager.getInstance();
        commonData.setTokenEnc("");
        commonData.setTokenObj("");
        commonData.setRoleInfo(null);
        //axios.interceptors.request.use(request => {

        //    

        //    request.headers['Authorization'] = ''
        //    return (request);
        //});
        window.location.href = '/';
    }
    render() {
  
        return (
            <div>

                <NavDropdown title={"Welcome " + CommonDataManager.getInstance().getUserName()}>
                    <NavDropdown.Item onClick={() => this.handleLogout()}>Logout</NavDropdown.Item>
                    </NavDropdown>
            </div>
        );
    }
}





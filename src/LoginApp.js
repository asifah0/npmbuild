﻿import React, { Component } from 'react'
import IdleTimer from 'react-idle-timer'
import configData from "./components/Shared/Util/config.json";

export default class LoginApp extends Component {
    constructor(props) {
        super(props)
        this.idleTimer = null
        this.handleOnAction = this.handleOnAction.bind(this)
        this.handleOnActive = this.handleOnActive.bind(this)
        this.handleOnIdle = this.handleOnIdle.bind(this)
    }

    render() {
        return (
            <div>
                <IdleTimer
                    ref={ref => { this.idleTimer = ref }}
                    timeout={1000 * 60 * 2}
                    onActive={this.handleOnActive}
                    onIdle={this.handleOnIdle}
                    onAction={this.handleOnAction}
                    debounce={250}
                />
                {/* your app here */}
            </div>
        )
    }

    handleOnAction(event) {
        let currentTS = new Date();
        console.log('******user did something' + currentTS)
    }

    handleOnActive(event) {
        let currentTS = new Date();
        console.log('******user is active' + currentTS)
        console.log('******time remaining', this.idleTimer.getRemainingTime())
    }

    handleOnIdle(event) {
        let currentTS = new Date();
        console.log('******user is idle, so session timedout' + currentTS)
        console.log('******last active', this.idleTimer.getLastActiveTime())
      
        let loginURL = configData.LOGIN_URL;

        window.location.href = loginURL;
    }
}
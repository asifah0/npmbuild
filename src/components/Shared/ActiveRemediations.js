﻿import React, { Component } from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import axios from 'axios';

import { RemediationDetails } from './RemediationDetails';
import { ViewAllRemediations } from '../ClientPortal/Remediations/ViewAllRemediations';
import { numberWithCommas } from './Util/CommonFunctions';
import { numberWithCommasAmount } from './Util/CommonFunctions';
import { getClientServiceURL } from './Util/GlobalUtils';
import { getFormattedDate } from './Util/CommonFunctions';

import '../../css/css/ActiveRemediations.Styles.css';

export class ActiveRemediations extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            clientId: '',
            results: [],
            filteredResults: [],
            loading: false,
            isServerError: false,
            inprocessclicked: true,
            mailedclicked: false,
            isRemedDetailsOpen: false,
            remedSK: 0,
            viewAllOpenFlag: false,
            source: props.source
        };
    }


    handleViewAllClose = () => {
        this.setState({ viewAllOpenFlag: false })

    }

    ViewAllOnClick = () => {
        this.setState({
            viewAllOpenFlag: true
        })
    }



    handleClose = () => {
        this.setState({ isRemedDetailsOpen: false })

    }

    remedNoOnClick = (item) => {
        this.setState({
            isRemedDetailsOpen: true,
            remedSK: item.remedSk
        })
    }


    componentDidMount = () => {
        this.fetchResult();
        
    }

    fetchResult = () => {
        this.setState({
            isServerError: false
        });


        const urlSuffix = `RemediationManager/ActiveRemediations`;
        const Url = getClientServiceURL(urlSuffix);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                this.setState({
                    results: res.data,
                    loading: true
                });
            })
            .catch((error) => {
                if (axios.isCancel(error) || error) {
                    
                }
                this.setState({
                    loading: true,
                    isServerError: true
                });
            });
    }
    loadActiveRemediations(clientId) {
        this.setState({ clientId }, () => {
        });
    }


    generateHeader = () => {
        return (
            <>
                <th class="text-left" style={{width:'10%'}}>Remed. #</th>
                <th class="text-left" style={{ width: '15%' }}>Remed. Name</th>
                <th class="text-left" style={{ width: '10%' }}>Date Received</th>
                <th class="text-left" style={{ width: '10%' }}>Mail Date</th>
                <th class="text-left" style={{ width: '10%' }}># of Pymts</th>
                <th class="text-left" style={{ width: '10%' }}># Check Stubs</th>
                <th class="text-left" style={{ width: '15%' }}>Total Pymt Amount</th>
                <th class="text-left" style={{ width: '20%' }}>Status</th>
            </>
        )
    }

    generateTableData = () => {

        const remData = this.state.inprocessclicked ? this.state.results.filter((rem) => rem.statusCode === "InProcess")
            : this.state.results.filter((rem) => rem.statusCode === "Mailed")


        return remData.map((item, index) => {
            return (
                <>
                    <tr key={item.remedSk}>
                        <td class="text-left" style={{ width: '10%' }}><button class="btn btn-link" onClick={() => this.remedNoOnClick(item)}> {item.remedNumber} </button>  </td>
                        <td class="text-left" style={{ width: '15%' }}>{item.remedName}</td>
                        <td class="text-left" style={{ width: '10%' }}>{getFormattedDate(item.receivedDt)}</td>
                        <td class="text-left" style={{ width: '10%' }}>{getFormattedDate(item.checkMailDt)}</td>
                        <td class="text-left" style={{ width: '10%' }}>{numberWithCommas(item.noOfPayments)}</td>
                        <td class="text-left" style={{ width: '10%' }}>{numberWithCommas(item.noOfCheckStubs )}</td>
                        <td class="text-left" style={{ width: '15%' }}>{numberWithCommasAmount(item.paymentAmount )}</td>
                        <td class="text-left" style={{ width: '20%' }}>{item.taskStatus}</td>
                   </tr>
                        

                    
                </>
            )
        })
    }

    inProcessClick = () => {
        this.setState({
            inprocessclicked: true,
            mailedclicked: false
        })
    }

    mailedClick = () => {
        this.setState({
            inprocessclicked: false,
            mailedclicked: true
        })        
    }

   
    render() {
        let inProcessClass = this.state.inprocessclicked ? "buttonClicked" : "buttonNotClicked";
        let mailedClass = this.state.mailedclicked ? "buttonClicked" : "buttonNotClicked";
        let tableClassName = 'tbl-box-heightActiveRmediationFromAllRem';
        
        if (this.state.source === '0') tableClassName = 'tbl-activeremediations';

        let tableClassNameBody = 'allremde-tbody';
        if (this.state.source === '0') tableClassNameBody = 'allremdefromdash-tbody';

        let tableHeight = 'tbl-height';
        this.state.source === '0' ? tableHeight = 'tbl-heightDashRemed' : tableHeight = 'tbl-heightAllRemed';


        const countInProgress = this.state.results.filter((rem) => rem.statusCode === "InProcess").length;
        const countMailed = this.state.results.filter((rem) => rem.statusCode === "Mailed").length;


        return (

            <>
                <div><h1 className="h1-title">Active Remediations</h1></div>

                <div className="activ-remed">
                    <div className="text-right tbl-tabs">
                        <ul className="nav nav-pills mb-3 float-right" id="pills-tab" role="tablist">
                            <li className="nav-item">
                                <button class={inProcessClass} onClick={this.inProcessClick}> In Process<span class="Button-badge-dsb">{countInProgress}</span></button>
                            </li>
                            <li className="nav-item">
                                <button class={mailedClass} onClick={this.mailedClick}>Mailed<span class="Button-badge-dsb2">{countMailed}</span></button>
                            </li>

                        </ul>
                    </div>
                    <div className="tab-content clearfix" id="pills-tabContent" style={{ clear: "both" }}>
                        <div className="tab-pane fade show active" id="inpro" role="tabpanel" aria-labelledby="pills-home-tab">

                            <div className={tableClassName} >
                                <div className={tableHeight}>
                                    <table id="example2" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">

                                    <thead>
                                        <tr>
                                            {this.generateHeader()}
                                        </tr>
                                        </thead>
                                        
                                        <tbody className={tableClassNameBody}>
                                          
                                        {this.state.loading ? this.generateTableData() : <ReactBootStrap.Spinner animation="border" />}
                                    </tbody>
                                    </table>
                                    {this.state.isServerError && <div class="label-error-red"> <span>Unable to display the data due to internal server error. Please try after sometimes.</span></div>}
                                    </div>
                            </div>
                        </div>
                        <div className="text-right strong mt-3">  <button class="btn btn-link" onClick={this.ViewAllOnClick}>View All</button></div>
                    </div>
                </div>
                <ReactBootStrap.Modal
                    show={this.state.viewAllOpenFlag}
                    onHide={this.handleViewAllClose}
                    backdrop="static"
                    keyboard={false}
                    dialogClassName="my-modal-fullscreen"
                    id="tableModal"
                >
                    <ReactBootStrap.Modal.Header closeButton>
                        <ReactBootStrap.Modal.Title>Active Remediations</ReactBootStrap.Modal.Title>
                    </ReactBootStrap.Modal.Header>
                    <ReactBootStrap.Modal.Body>
                        <ViewAllRemediations />
                    </ReactBootStrap.Modal.Body>
                </ReactBootStrap.Modal>
                <ReactBootStrap.Modal
                    show={this.state.isRemedDetailsOpen}
                    onHide={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    dialogClassName="my-modal-fullscreen"
                    id="tableModal"
                >
                    <ReactBootStrap.Modal.Header closeButton>
                        <ReactBootStrap.Modal.Title>Remediations Details</ReactBootStrap.Modal.Title>
                    </ReactBootStrap.Modal.Header>
                    <ReactBootStrap.Modal.Body>
                        <RemediationDetails remedSK={this.state.remedSK} />
                    </ReactBootStrap.Modal.Body>
                </ReactBootStrap.Modal>
            </>
        );
    }
}
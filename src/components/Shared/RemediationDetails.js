﻿import React from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import { useFetch } from './Services/GetServiceCall';
import { getClientServiceURL } from './Util/GlobalUtils';
import { numberWithCommas, numberWithCommasAmount, getFormattedDate } from './Util/CommonFunctions';


export const RemediationDetails = (props) => {

    const urlsuffix = `RemediationDashboard/GetRemediationDetailsByRemedSK?remedSK=${props.remedSK}`;
    const url = getClientServiceURL(urlsuffix);
    const { status, results, remedDocs } = useFetch(url);
    const loading = (status === 'fetching');
    const success = (status === 'fetched');
    const isError = (status === 'error');
    return (

        <>
            {loading && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
            {isError && <div class="label-error-red ml-3"> <span>Unable to display the data due to internal server error. Please try after sometimes.</span></div>}
                <div class="toast-body ">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="gray-bg cums-box-height box-noBdr">
                                <p class="p-ttle mb-4">Remediation Details</p>
                                <div class="form-group row">
                                    <label class="col-sm-6">Remed. #</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? results[0].remedNumber : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6">Remed. Name</label>
                                    <div class="col-sm-6">
                                    {results.length > 0 ? results[0].remedName : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-sm-6"># of Pymts</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? numberWithCommas(results[0].noOfPayments) : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-sm-6">Total Pymt Amount</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? numberWithCommasAmount(results[0].paymentAmount) : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6"># of Check Stubs</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? numberWithCommas(results[0].noOfCheckStubs) : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6">Check Mail Date</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? getFormattedDate(results[0].checkMailDt) : ''}
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-6">Status</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? results[0].status : ''}
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-6">Business Unit</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? results[0].businessUnit : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6">Contact</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? results[0].primaryContact : ''}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="gray-bg cums-box-height box-noBdr">
                                <p class="p-ttle mb-4">Distribution Details</p>
                                <div class="form-group row">
                                    <label class="col-sm-6"># Cashed </label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? numberWithCommas(results[0].cashed) : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6"># Uncashed</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? numberWithCommas(results[0].uncashed) : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6"># Undeliverables</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? numberWithCommas(results[0].undeliverables) : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6"># Reissues</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? numberWithCommas(results[0].reissues) : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6"># Voided </label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? numberWithCommas(results[0].voidedChecks) : ''}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-6">Void Date</label>
                                    <div class="col-sm-6">
                                        {results.length > 0 ? getFormattedDate(results[0].voidDt) : ''}
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="col-lg-4">

                            <div class="gray-bg cums-box-height box-noBdr">
                                <p class="p-ttle mb-4">Documents</p>
                            <div class="doc-files">
                                {remedDocs.length > 0 ? remedDocs.map(record => (
                                        <div className='row doc-link'>
                                            <div className='col-sm-6 col-lg-6 '>
                                                <div class=" btn-link"> <span><i className='far fa-file-alt'></i>&nbsp;</span>{record.docName}</div>
                                            </div>
                                            <div className='col-sm-6 col-lg-6'>
                                            </div>
                                        </div>
                                    )) : <h1 className='h2-title'>No documents are available to display</h1>}
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </>

        );
}

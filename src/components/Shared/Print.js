﻿import React, { useRef } from "react";
import ReactToPrint from "react-to-print";

const imagePrint = require('../../css/images/Print.svg');

const PrintObject = React.forwardRef((props, ref) => (     

    <table className="printContent" ref={ref}>
            <thead style={{ borderBottom: '1px solid #333333'}}>
                <tr>
                    {props.header}
                </tr>
            </thead>
            <tbody>
                {props.data}
            </tbody>
    </table>
));

const Print = (props) => {
    const componentRef = useRef();
    return (
        <>
            <ReactToPrint
                trigger={() => <button className='btn btn-link'><img src={imagePrint} alt="Print" /></button>}
                content={() => componentRef.current}
            />
            <PrintObject ref={componentRef} header={props.header} data={props.data} />
       </> 
    );
}

export default Print;
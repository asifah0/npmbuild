﻿
export const DataFilesData = [
    {
        RemedSK: 101,
        RemedNo: '32454345',
        FileType: 'ConsumerData',
        Filename: 'Consumer1.txt',
        DateReceived: '05/08/2021',
        UploadUser: 'User1',
        NoOfPayments: 3435,
        PaymentAmount:343543553
    },
    {
        RemedSK: 102,
        RemedNo: '32454345',
        FileType: 'ConsumerData',
        Filename: 'Consumer2.txt',
        DateReceived: '05/05/2021',
        UploadUser: 'User2',
        NoOfPayments: 3657,
        PaymentAmount: 2132435
    },
    {
        RemedSK: 103,
        RemedNo: '32454345',
        FileType: 'ConsumerData',
        Filename: 'Consumer3.txt',
        DateReceived: '05/08/2021',
        UploadUser: 'User1',
        NoOfPayments: 87976,
        PaymentAmount: 9798786752
    },
    {
        RemedSK: 104,
        RemedNo: '32454345',
        FileType: 'Check Stub',
        Filename: 'CheckStub1.txt',
        DateReceived: '05/08/2021',
        UploadUser: 'User2',
        NoOfPayments: '',
        PaymentAmount: ''
    },
    {
        RemedSK: 105,
        RemedNo: '6768823',
        FileType: 'CheckStub',
        Filename: 'ConsCheckStub2umer1.txt',
        DateReceived: '05/23/2021',
        UploadUser: 'User3',
        NoOfPayments: '',
        PaymentAmount: ''
    },
    {
        RemedSK: 105,
        RemedNo: '6768823',
        FileType: 'Consumer Data',
        Filename: 'ConsCheckStub2umer1.txt',
        DateReceived: '05/23/2021',
        UploadUser: 'User3',
        NoOfPayments: 343253,
        PaymentAmount: 547887980
    },
]
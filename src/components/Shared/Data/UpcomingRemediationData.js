﻿
export const RemediationData = [
    {
        RemedSK: 111,
        RemedNo: "134678433",
        RemedName:'Test1',
        ClientName: 'JP Morgan Chase Bank',
        DateReceived: "01/01/2021",
        MailDate: "02/01/2021",
        NoOfPayments: 10000,
        TotalPaymentAmt: 3659801946576328,
        NoOfCheckStubs: 567
    },
    {
        RemedSK: 112,
        RemedNo: "45646733",
        RemedName: 'Test2',
        ClientName: 'Morgan Financial',
        DateReceived: "04/06/2021",
        MailDate: "05/07/2021",
        NoOfPayments: 6000,
        TotalPaymentAmt: 365980196328,
        NoOfCheckStubs: 344
    },
    {
        RemedSK: 113,
        RemedNo: "97578433",
        RemedName: 'Test3',
        ClientName: 'Fidelity',
        DateReceived: "06/01/2021",
        MailDate: "07/01/2021",
        NoOfPayments: 15000,
        TotalPaymentAmt: 365946576328,
        NoOfCheckStubs: 890
    },
    {
        RemedSK: 114,
        RemedNo: "17378433",
        RemedName: 'Test4',
        ClientName: 'Bank of America',
        DateReceived: "03/01/2021",
        MailDate: "04/01/2021",
        NoOfPayments: 10000,
        TotalPaymentAmt: 3659946576328,
        NoOfCheckStubs: 234
    },
    {
        RemedSK: 115,
        RemedNo: "567678433",
        RemedName: 'Test5',
        ClientName: 'JP Morgan ',
        DateReceived: "03/01/2021",
        MailDate: "05/03/2021",
        NoOfPayments: 15000,
        TotalPaymentAmt: 59801946576328,
        NoOfCheckStubs: 577
    },
    {
        RemedSK: 116,
        RemedNo: "45684336",
        RemedName: 'Test6',
        ClientName: 'JP Morgan Chase Bank',
        DateReceived: "01/01/2021",
        MailDate: "01/01/2021",
        NoOfPayments: 10000,
        TotalPaymentAmt: 3659801,
        NoOfCheckStubs: 23
    },
    {
        RemedSK: 117,
        RemedNo: "995646733",
        RemedName: 'Test7',
        ClientName: 'Morgan Financial',
        DateReceived: "04/06/2021",
        MailDate: "05/07/2021",
        NoOfPayments: 6000,
        TotalPaymentAmt: 801946576328,
        NoOfCheckStubs: 567
    },
    {
        RemedSK: 118,
        RemedNo: "89978433",
        RemedName: 'Test8',
        ClientName: 'Fidelity',
        DateReceived: "06/01/2021",
        MailDate: "07/01/2021",
        NoOfPayments: 15000,
        TotalPaymentAmt: 3659801328,
        NoOfCheckStubs: 546
    },
]
﻿export const NotificationsData = [
    {
        NotificationSK: 101,
        ReceivedDate:'06/23/2021',
        MessageTitle: 'Pending Upcoming Remediation',
        MessageDescription: 'You have a remediation #87654321 pending for submission. Please submit file for processing.'
    },
    {
        NotificationSK: 102,
        ReceivedDate: '07/05/2021',
        MessageTitle: 'JP Morgan Chase - Remed. # 11118888',
        MessageDescription: 'Consumer Upload occured 05/13/2021 11:25 AM. Payment # - 10, 000, Payment Amt $10, 000'
    },
    {
        NotificationSK: 103,
        ReceivedDate: '05/03/2021',
        MessageTitle: 'Remediation going stale',
        MessageDescription: 'Outstanding checks for Remediatin #3254356 are going stale on 06/302021'
    },
    {
        NotificationSK: 104,
        ReceivedDate: '06/08/2021',
        MessageTitle: 'Data File Validated',
        MessageDescription: 'Remediation #54678098 was successfully validated for 456 of 500 checks in the total amount of $567,00034.00'
    },
]
﻿
export const ConsumersData = [
    {
        ConsumerSK:101,
        ConsumerName: "Stan Smith",
        AccountNo: 39940035,
        BroadridgeNo: "B4683021",
        RemedNo: "8934619",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 789032,
        CheckDate: "04-29-2021",
        CheckAmount: 100,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789021,
                CheckDate: "04-29-2021",
                CheckAmount: 30,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890542,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },


    {
        ConsumerSK: 102,
        ConsumerName: "Test Consumer",
        AccountNo: 39940046,
        BroadridgeNo: "B4683021",
        RemedNo: "893461930",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 786783,
        CheckDate: "04-29-2021",
        CheckAmount: 200,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789432,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890329,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },

    {
        ConsumerName: "Stan Smith",
        AccountNo: 39940035,
        BroadridgeNo: "B4683021",
        RemedNo: "893461930",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 789032,
        CheckDate: "04-29-2021",
        CheckAmount: 100,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789021,
                CheckDate: "04-29-2021",
                CheckAmount: 30,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890542,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },


    {
        ConsumerSK: 103,
        ConsumerName: "Test Consumer",
        AccountNo: 39940046,
        BroadridgeNo: "B4683021",
        RemedNo: "893461930",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 786783,
        CheckDate: "04-29-2021",
        CheckAmount: 200,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789432,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890329,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },

    {
        ConsumerName: "Stan Smith",
        AccountNo: 39940035,
        BroadridgeNo: "B4683021",
        RemedNo: "893461930",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 789032,
        CheckDate: "04-29-2021",
        CheckAmount: 100,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789021,
                CheckDate: "04-29-2021",
                CheckAmount: 30,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890542,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },


    {
        ConsumerSK: 104,
        ConsumerName: "Test Consumer",
        AccountNo: 39940046,
        BroadridgeNo: "B4683021",
        RemedNo: "893461930",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 786783,
        CheckDate: "04-29-2021",
        CheckAmount: 200,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789432,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890329,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },


    {
        ConsumerSK: 105,
        ConsumerName: "Stan Smith",
        AccountNo: 39940035,
        BroadridgeNo: "B4683021",
        RemedNo: "893461930",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 789032,
        CheckDate: "04-29-2021",
        CheckAmount: 100,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789021,
                CheckDate: "04-29-2021",
                CheckAmount: 30,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890542,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },


    {
        ConsumerSK: 106,
        ConsumerName: "Test Consumer",
        AccountNo: 39940046,
        BroadridgeNo: "B4683021",
        RemedNo: "893461930",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 786783,
        CheckDate: "04-29-2021",
        CheckAmount: 200,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789432,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890329,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },


    {
        ConsumerSK: 107,
        ConsumerName: "Stan Smith",
        AccountNo: 39940035,
        BroadridgeNo: "B4683021",
        RemedNo: "893461930",
        Address1: "8 Grand Avenue",
        Address2: "Building 6",
        City: "New York",
        State: "NY",
        Zipcode: "10001-466",
        CheckNo: 789032,
        CheckDate: "04-29-2021",
        CheckAmount: 100,
        CheckStatus: "Cashed",
        CheckStatusDate: "06-15-2021",
        subtask: [
            {
                Address1: "8 Grand Avenue",
                Address2: "Building 6",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 789021,
                CheckDate: "04-29-2021",
                CheckAmount: 30,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            },
            {
                Address1: "8 Grand test Avenue",
                Address2: "",
                City: "New York",
                State: "NY",
                Zipcode: "10001-466",
                CheckNo: 7890542,
                CheckDate: "04-29-2021",
                CheckAmount: 150,
                CheckStatus: "Voided",
                CheckStatusDate: "06-11-2021"
            }
        ]
    },
    
]

﻿import React from 'react';
import CurrencyInput from 'react-currency-input-field';

export default function CurrencyInputComponent({ inputval, setInputval, CSSClassName, decimalValAllowed, groupSeperatorDisable, prefixVal }) {
    function handlesKeyPress(e) {
        let re = /[0-9.]+/g;
        if (!decimalValAllowed) {
            re = /[0-9]+/g;
        }
        if (!re.test(e.key)) {
            e.preventDefault();
        }
    }

    function getAmountVal(amount) {
        if (amount.toLowerCase() === 'k' || amount.toLowerCase() === 'm' || amount.toLowerCase() === 'b') {
            amount = '';
            return amount;
        }
        while (amount.includes(',')) {
            amount = amount.replace(',', '');
        }
        if (amount.toLowerCase().includes('k') || amount.includes('K')) {
            amount = amount.toLowerCase().replace('k', '') * 1000;
            return amount;
        }
        if (amount.toLowerCase().includes('m') || amount.includes('M')) {
            amount = amount.toLowerCase().replace('m', '') * 1000000;
            return amount;
        }
        if (amount.toLowerCase().includes('b') || amount.includes('B')) {
            amount = amount.toLowerCase().replace('b', '') * 1000000000;
            return amount;
        }
        return amount;
    }

    function handleInputChange(event) {
        let inputAmtVal = event.target.value;
        if (inputAmtVal.includes(prefixVal)) {
            inputAmtVal = inputAmtVal.replace(prefixVal, '');
        }
        if (inputAmtVal.toLowerCase().includes('k') ||
            inputAmtVal.toLowerCase().includes('m') ||
            inputAmtVal.toLowerCase().includes('b') ||
            inputAmtVal.toLowerCase().includes(',')) {
            inputAmtVal = getAmountVal(inputAmtVal);
        }
        if (inputAmtVal === '.') {
            inputAmtVal = '0.';
        }
        const regex = /^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/;
        if (regex.test(inputAmtVal) || inputAmtVal === '') {
            setInputval(inputAmtVal);
        }
    }

    return (
        <>
            <CurrencyInput className={CSSClassName} allowNegativeValue={false} value={inputval} allowDecimals={decimalValAllowed}
                decimalsLimit={2} disableGroupSeparators={groupSeperatorDisable} prefix={prefixVal}
                onKeyPress={(e) => handlesKeyPress(e)} onChange={handleInputChange} />
        </>
    );
}
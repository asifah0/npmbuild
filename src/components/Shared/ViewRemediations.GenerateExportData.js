﻿
export const getExportData = (results) => {
    let dataTable = [];
    if (results.length > 0) {
        if (results) {
            for (let i in results) {
                let obj = {
                    'Remed. #': results[i].RemedNo,
                    'Remed. Name': results[i].RemedName,
                    'Remed. Status': results[i].RemediationStatus,
                    '# Pymts': results[i].NoOfPayments,
                    'Pymts Amount': results[i].TotalPaymentAmt,
                    '# Check Stubs': results[i].NoOfCheckStubs,
                    'Mail Date': results[i].MailDate,
                    'Outstanding Payments': results[i].OutstandingPayments,
                    'Voided Checks': results[i].VoidedChecks,
                    'Void Date': results[i].VoidDate
                }
                dataTable.push(obj);
            }
        }
    }
    return dataTable;
}


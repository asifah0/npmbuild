﻿import React, { useEffect, useState, useRef } from 'react';
import { useReactToPrint } from 'react-to-print';
import * as ReactBootStrap from 'react-bootstrap';

import { generateHeader } from './ViewRemediations.GenerateHeader';
import { generateTableData } from './ViewRemediations.GenerateTableData';
import { getExportData } from './ViewRemediations.GenerateExportData';
import { RemediationData } from './Data/RemediationData';
import useSortableData from './UseSortableData';
import { ExportCSV } from './ExportToExcel';
import Print from './Print';


const ViewRemediations = (props) => {

    const [results, setResults] = useState(RemediationData);
    const [todosPerPage, setTodosPerPage] = useState(5);    
    const [arrayForHoldingResults, setArrayForHoldingResults] = useState(RemediationData.slice(0, todosPerPage));    
    const [loaded, setLoaded] = useState(true);    
    const [next, setNext] = useState(5);
    const [filename, setFilename] = useState('testViewRemediations');
      
    const handleShowMorePosts = () => {        
        loopWithSlice(next, parseInt(next + todosPerPage));
        setNext(next => parseInt(next + todosPerPage));        
    };

    const loopWithSlice = (start, end) => {
        const slicedResults = results.slice(start, end);
        setArrayForHoldingResults((arrayForHoldingResults) => [...arrayForHoldingResults, ...slicedResults]);
    };

    const columnHeaders = ['Remed. #', 'Remed. Name', 'Remed. Status', '# Pymts', 'Pymts Amount', '# Check Stubs', 'Mail Date', 'Outstanding Payments', 'Voided Checks', 'Void Date'];
    
    const generateHeaderForPrint = () => {
        let res = [];
        for (let i = 0; i < columnHeaders.length; i++) {
            res.push(<th id={columnHeaders[i]}>{columnHeaders[i]}</th>)
        }
        return res;
    }   
        
        const totNoOfRemed = "17";
        const totNoOfPayments = "800,000";
        const totAmount = "$6,500,000";

        const { items, requestSort, sortConfig } = useSortableData(arrayForHoldingResults);
       

     return (            
            <>
                <div className="row mt-4 mb-3">

                    <div className="col-lg-3">
                        <div className="box gray-bg p-4">
                            <p>Remediations</p>
                            <div className="dsb-numbers">{totNoOfRemed}</div>
                        </div>
                    </div>


                    <div className="col-lg-3">
                        <div className="box gray-bg p-4">
                            <p>Pymts</p>
                            <div className="dsb-numbers">{totNoOfPayments}</div>
                        </div>
                    </div>


                    <div className="col-lg-3">
                        <div className="box gray-bg p-4">
                            <p>Total Amount</p>
                            <div className="dsb-numbers">{totAmount}</div>
                        </div>
                    </div>

                </div>

                <div className="row mt-5 mb-3">

                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col">


                                <div className="box box-border consumer">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <h1 className="h1-title"> Remediation Details</h1>
                                        </div>


                                        <div className="col-lg-6 text-right">
                                         <span>
                                             <ExportCSV csvData={getExportData(results)} fileName={filename} />
                                            </span>
                                            <span className="ml-2">
                                                <Print header={generateHeaderForPrint()} data={generateTableData(results)} />
                                            </span>
                                        </div>
                                    </div>

                                    <div className="table-scroll" style={{ height: '300px' }}>
                                        <table id="clientlist" className="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    {generateHeader(requestSort, sortConfig)}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {loaded ? generateTableData(items) : <ReactBootStrap.Spinner animation="border" />}
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <div className="row mt-30">
                                               <div className="col text-center">
                                         <p>Viewing results 1-{arrayForHoldingResults.length} of {results.length}</p>
                                            <button type="button" className="btn btn-border" onClick={handleShowMorePosts}>Load More</button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                       </div>                
                </>

        )
    }

export default ViewRemediations;

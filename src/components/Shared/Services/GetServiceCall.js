﻿import React, { useState } from 'react';
import axios from 'axios';

export const useFetch = (url) => {
    const [status, setStatus] = useState('idle');
    const [results, setResults] = useState([]);
    const [remedDocs, setRemedDocs] = useState([]);

    React.useEffect(() => {
        if (!url) return;      
        const fetchData = async () => {
            setStatus('fetching');
            try {
                //window.alert(url);
                const response = await axios.get(url);
                if (url.includes('GetRemediationDetailsByRemedSK')) {
                    setResults(response.data.remediationDetails);
                    setRemedDocs(response.data.remediationDocDetails);
                }
                else {
                    setResults(response.data);   
                }
                setStatus('fetched');
            }
            catch (e) {
                setStatus('error');
            }
        };
        fetchData();
    }, [url]);

   if (url.includes('GetRemediationDetailsByRemedSK'))
        return { status, results, remedDocs }
    else
        return { status, results };
};


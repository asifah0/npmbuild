﻿import React from 'react';

import { numberWithCommas } from './Util/CommonFunctions';

export const generateTableData = (items) => {
    return items.map((item, index) => {
        const { RemedSK, RemedNo, RemedName, RemediationStatus, NoOfPayments, TotalPaymentAmt, NoOfCheckStubs, MailDate, OutstandingPayments, VoidedChecks, VoidDate } = item //destructuring
        return (
            <tr key={RemedSK}>
                <td>{RemedNo}</td>
                <td>{RemedName}</td>
                <td>{RemediationStatus}</td>
                <td class="text-left">{numberWithCommas(NoOfPayments)}</td>
                <td class="text-left">${numberWithCommas(TotalPaymentAmt)}.00</td>
                <td class="text-left">{numberWithCommas(NoOfCheckStubs)}</td>
                <td>{MailDate}</td>
                <td class="text-left">{numberWithCommas(OutstandingPayments)}</td>
                <td class="text-left">{numberWithCommas(VoidedChecks)}</td>
                <td>{VoidDate}</td>
            </tr>
        )
    })
}

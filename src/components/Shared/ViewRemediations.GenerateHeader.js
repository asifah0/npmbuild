﻿import React from 'react';

const getClassNamesFor = (name, sortConfig) => {
    if (!sortConfig) {
        return;
    }
    return sortConfig.key === name ? sortConfig.direction : undefined;
};

export const generateHeader = (requestSort, sortConfig) => {
    return (
        <>
            <th>
                <button
                    type="button"
                    onClick={() => requestSort('RemedNo')}
                    className={getClassNamesFor('RemedNo', sortConfig)}
                >
                    Remed. #
            </button>
            </th>

            <th>
                <button
                    type="button"
                    onClick={() => requestSort('RemedName')}
                    className={getClassNamesFor('RemedName', sortConfig)}
                >
                    Remed. Name
                    </button>
            </th>

            <th>
                <button
                    type="button"
                    onClick={() => requestSort('RemediationStatus')}
                    className={getClassNamesFor('RemediationStatus', sortConfig)}
                >
                    Remed. Status
                    </button>
            </th>

            <th>
                <button
                    type="button"
                    onClick={() => requestSort('NoOfPayments')}
                    className={getClassNamesFor('NoOfPayments', sortConfig)}
                >
                    # Pymts
                    </button>
            </th>


            <th>
                <button
                    type="button"
                    onClick={() => requestSort('TotalPaymentAmt')}
                    className={getClassNamesFor('TotalPaymentAmt', sortConfig)}
                >
                    Pymts Amount
                    </button>
            </th>


            <th>
                <button
                    type="button"
                    onClick={() => requestSort('NoOfCheckStubs')}
                    className={getClassNamesFor('NoOfCheckStubs', sortConfig)}
                >
                    # Check Stubs
                    </button>
            </th>

            <th>
                <button
                    type="button"
                    onClick={() => requestSort('MailDate')}
                    className={getClassNamesFor('MailDate', sortConfig)}
                >
                    Mail Date
                 </button>
            </th>

            <th>
                <button
                    type="button"
                    onClick={() => requestSort('OutstandingPayments')}
                    className={getClassNamesFor('OutstandingPayments', sortConfig)}
                >
                    Outstanding Pymts
                    </button>
            </th>


            <th>
                <button
                    type="button"
                    onClick={() => requestSort('VoidedChecks')}
                    className={getClassNamesFor('VoidedChecks', sortConfig)}
                >
                    Voided Checks
                    </button>
            </th>

            <th>
                <button
                    type="button"
                    onClick={() => requestSort('VoidDate')}
                    className={getClassNamesFor('VoidDate', sortConfig)}
                >
                    Void Date
                    </button>
            </th>
        </>
    );
}

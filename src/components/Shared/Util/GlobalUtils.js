﻿import CommonDataManager from "../../../CommonDataManager";
import configData from "./config.json";



export const getServiceURL = (url) => {
    
    let prefix = configData.SERVER_URL.replace("\\", '/');
    let suffix = url.replace('\\', '/');
   
    let response = prefix + '/' + url;
    let firstPart = response.substring(0, 9);
    let secondPart = response.substring(9);

    secondPart = secondPart.replace('//', '/');
    secondPart = secondPart.replace('//', '/');
    response = firstPart + secondPart;
    return response;
}


export const getClientServiceURL = (url) => {
    let commonData = CommonDataManager.getInstance();
    const clientID = commonData.getClientID();

    let prefix = configData.SERVER_URL.replace("\\", '/');
    let suffix = url.replace('\\', '/');

    let response = prefix + '/' + clientID + '/' + url;
    let firstPart = response.substring(0, 9);
    let secondPart = response.substring(9);

    secondPart = secondPart.replace('//', '/');
    secondPart = secondPart.replace('//', '/');
    response = firstPart + secondPart;
    return response;
}

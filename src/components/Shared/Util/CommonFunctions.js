﻿import Dialog from 'react-bootstrap-dialog';

export const GetErrorMessageDescription = (error) => {
    let result = '';
    if (error.response) {
        if (error.response.data.Code) {
            if (error.response.data.Code === 'BR-SESSION-01') {

            }
            else {
                result = error.response.data.Message;
            }
        }
        else {
            result = 'Unable to process the request due to some error. Please try after sometimes';
        }

    } else if (error.request) {
        result = 'Unable to sent the request. Please try after sometimes.';
    } else {
        result = 'Something went wrong. Please try after sometimes.';
    }

    return result;
}

export const isTextNullOrEmpty = (text) => {
    if (text === null || text === undefined || text.trim() === '') {
        return (true);
    }
    else {
        return (false);
    }
}

export const numberWithCommas = (x) => {
    if (x)
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    else
        return 0;
}

export const numberWithCommasAmount = (x) => {
    let data;
    if (x) {
        data = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    
        if (data.includes('.'))
            data = '$' + data;
        else
            data = '$' + data + '.00';
    }
    else
        data = '$0.00';
    return data;
}

export const getFormattedDate = (date1) => {
    if (date1) {
        const date = new Date(date1);
        let year = date.getFullYear();
        let month = (1 + date.getMonth()).toString().padStart(2, '0');
        let day = date.getDate().toString().padStart(2, '0');
        return month + '/' + day + '/' + year;
    }
    else {
        return ('');
    }
}

export const getFormattedCurrentDate = () => {
    const date = new Date();
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
    return month + '/' + day + '/' + year;
}

export const getFormattedDateAssign = (date1) => {
    const date = new Date(date1);
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
    return year + '-' + month + '-' + day;
}

export const getFormattedDateWithTime = (date1) => {
    const date = new Date(date1);
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');

    var hours = date.getHours();
    var minutes = date.getMinutes();

    // Check whether AM or PM
    var newformat = hours >= 12 ? 'PM' : 'AM';

    // Find current hour in AM-PM Format
    hours = hours % 12;

    // To display "0" as "12"
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;


    return month + '/' + day + '/' + year + ' ' + hours + ':' + minutes + ' ' + newformat;
}

export function showMsg(dialog, alertTitle, alertMsg) {
    dialog.show({
        title: alertTitle,
        body: alertMsg,
        actions: [
            Dialog.OKAction()
        ],
        bsSize: 'large',
        onHide: (dialog) => {
            dialog.hide()
        }
    })
}

export function getAmountValue(amount) {
    if (amount.toLowerCase() === 'k' || amount.toLowerCase() === 'm' || amount.toLowerCase() === 'b') {
        amount = '';
        return amount;
    }
    while (amount.includes(',')) {
        amount = amount.replace(',', '');
    }
    if (amount.toLowerCase().includes('k') || amount.includes('K')) {
        amount = amount.toLowerCase().replace('k', '') * 1000;
        return amount;
    }
    if (amount.toLowerCase().includes('m') || amount.includes('M')) {
        amount = amount.toLowerCase().replace('m', '') * 1000000;
        return amount;
    }
    if (amount.toLowerCase().includes('b') || amount.includes('B')) {
        amount = amount.toLowerCase().replace('b', '') * 1000000000;
        return amount;
    }
    return amount;
}

//Format selected date value
export const formatDate = (dateVal) => {
    let newDate = new Date(dateVal),
        month = '' + (newDate.getMonth() + 1),
        day = '' + newDate.getDate(),
        year = newDate.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    if (newDate != 'Invalid Date')
        return [month, day, year].join('/');
    else
        return '';
}
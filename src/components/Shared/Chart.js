﻿import React from "react";
import Chart from "react-apexcharts";
import '../../css/css/Charts.Styles.css'

export class ApexChart extends React.Component {  

    render() {


        let series;
        let labels;        
        let remedCheckCount;
        let optionsAllRemed;
        let optionsCheckStatus;
        let customScale;
        let donutOffsetY;
        let fontSizeName;
        let fontSizeValue;
        let legendoffsetX;
        let donutOffsetX;

        if (this.props.fromScreen === 'AllRemediations') {

            let max;
            let inProgressCountLength = this.props.inProgressCount.length;
            let mailedCountLength = this.props.mailedCount.length;
            let closedCountLength = this.props.closedCount.length;
                        
            let numbers = [parseInt(inProgressCountLength), parseInt(mailedCountLength), parseInt(closedCountLength)];

            max = Math.max.apply(null, numbers); 
            //max = 0;
            if (max === 2) {
                customScale = 1.25;
                donutOffsetY = 25;
                fontSizeName = "18px";
                fontSizeValue = "24px";
            }
            else if (max === 1) {
                customScale = 1.20;
                donutOffsetY = 20;
                fontSizeName = "19px";
                fontSizeValue = "25px";
            }
            else if (max === 3) {
                customScale = 1.3;
                donutOffsetY = 30;
                fontSizeName = "17px";
                fontSizeValue = "23px";
            }
            else if (max === 0) {
                customScale = 1.08;
                donutOffsetY = 8;
                fontSizeName = "22px";
                fontSizeValue = "27px";
            }

            series = [parseInt(this.props.inProgressCount), parseInt(this.props.mailedCount), parseInt(this.props.closedCount)];

            labels = [parseInt(this.props.inProgressCount) + ' - In Process', parseInt(this.props.mailedCount) + ' - Mailed', parseInt(this.props.closedCount) + ' - Closed'];


            //labels = ['In Process', 'Mailed', 'Closed'];
            //labels = ['217 - In Process', '536 - Mailed', '895 - Closed'];

            
            optionsAllRemed = {
                dataLabels: { enabled: false },

                colors: this.props.remedData.colors,

                labels: labels,

                stroke: {
                    width: 0
                },


                maintainAspectRatio: true,

                legend: {
                    position: 'right',
                    offsetY: this.props.remedData.legendoffsetY,
                    offsetX: this.props.remedData.legendoffsetX,
                    fontWeight: 'bold',
                    fontSize: '15px',
                    fontFamily: 'arial',
                    width: '400px'
                },

                plotOptions: {
                    pie: {
                        expandOnClick: false,                        
                        customScale: customScale,
                        offsetX: this.props.remedData.donutOffsetX,
                        offsetY: donutOffsetY,

                        donut: {

                            size: '80%',

                            labels: {
                                show: true,
                                name: {
                                    show: true,
                                    offsetY: this.props.remedData.offsetYName
                                },
                                value: {
                                    offsetY: this.props.remedData.offsetYValue,
                                    show: true,
                                    color: '#6D6D6D',
                                    fontWeight: 'bold',
                                    //fontSize: this.props.remedData.fontSizeValue,
                                    fontSize: fontSizeValue,
                                    fontFamily: 'arial'
                                },
                               
                                total: {
                                    label: this.props.remedData.label,
                                    value: 100,
                                    show: true,
                                    color: '#6D6D6D',
                                    //fontSize: this.props.remedData.fontSizeName,
                                    fontSize: fontSizeName,
                                    fontWeight: 'bold',
                                    fontFamily: 'arial',
                                }
                            }
                        }

                    }
                }

            }


        }
        else {

            remedCheckCount = parseInt(this.props.remedCount);
            
            let cashed = parseInt(this.props.cashedCount);
            let uncashed = parseInt(this.props.uncashedCount);
            let undeliverable = parseInt(this.props.undeliverableCount);
            let voided = parseInt(this.props.voidedCount);

            //let undeliverable = 500;

            series = [cashed, uncashed, undeliverable, voided]

            //let total = 20;
            let total = cashed + uncashed + undeliverable + voided;
            
            let cashedPer = total > 0 ? ((cashed / total) * 100).toFixed().toString() + '% - Cashed' : '0% - Cashed';
            let uncashedPer = total > 0 ? ((uncashed / total) * 100).toFixed().toString() + '% - Uncashed' : '0% - Uncashed';
            let undeliverablePer = total > 0 ? ((undeliverable / total) * 100).toFixed().toString() + '% - Undeliverable' : '0% - Undeliverable';
            let voidedPer = total > 0 ? ((voided / total) * 100).toFixed().toString() + '% - Voided' : '0% - Voided';

            let undelperlen = total > 0 ? ((undeliverable / total) * 100).toFixed().toString().length : 0;

            //let max;
            //let cashedPerLength = total > 0 ? ((cashed / total) * 100).toFixed().toString().length : 0;
            //let uncashedPerLength = total > 0 ? ((uncashed / total) * 100).toFixed().toString().length : 0;
            //let undeliverablePerLength = total > 0 ? ((undeliverable / total) * 100).toFixed().toString().length : 0;
            //let voidedPerLength = total > 0 ? ((voided / total) * 100).toFixed().toString().length : 0;

            //let numbers = [parseInt(cashedPerLength), parseInt(uncashedPerLength), parseInt(undeliverablePerLength), parseInt(voidedPerLength)];

            //max = Math.max.apply(null, numbers);
            //// max = 1;


            //if (max === 2) {
            //    customScale = 1.56;
            //    donutOffsetY = 40;
            //    fontSizeName = "15px";
            //    fontSizeValue = "20px";
            //}
            //else if (max === 1) {
            //    customScale = 1.6;
            //    donutOffsetY = 35;
            //    fontSizeName = "16px";
            //    fontSizeValue = "21px";
            //}
            //else if (max === 3) {
            //    customScale = 1.5
            //    donutOffsetY = 45;
            //    fontSizeName = "14px";
            //    fontSizeValue = "19px";
            //}


            if (undeliverable === total) {
                customScale = 1.68;
                fontSizeName = "14px";
                fontSizeValue = "18px";
                legendoffsetX = 65;
                donutOffsetX = 20;
                donutOffsetY = 55;
            }
            else if (undelperlen === 2) {
                customScale = 1.58;
                donutOffsetY = 40;
                fontSizeName = "14.5px";
                fontSizeValue = "19px";
                legendoffsetX = 80;
                donutOffsetX = 15;
                donutOffsetY = 48;
            }
            else {
                customScale = 1.5;
                donutOffsetY = 40;
                fontSizeName = "15px";
                fontSizeValue = "20px";
                legendoffsetX = 80;
                donutOffsetX = 15;
                donutOffsetY = 40;
            }

            labels = [cashedPer, uncashedPer, undeliverablePer, voidedPer];
            //labels = ['0% - Cashed', '0% - Uncashed', '0% - Undeliverable', '0% - Voided'];

            optionsCheckStatus = {
                dataLabels: { enabled: false },

                colors: this.props.remedData.colors,

                    labels: labels,

                        stroke: {
                    width: 0
                },


                maintainAspectRatio: true,

                    legend: {
                    position: 'right',
                    offsetY: this.props.remedData.legendoffsetY,
                    offsetX: legendoffsetX,
                    fontWeight: 'bold',
                    fontSize: '15px',
                    fontFamily: 'arial',
                    width: '400px'
                },

                plotOptions: {
                    pie: {
                        expandOnClick: false,
                        customScale: customScale,
                        offsetX: donutOffsetX,
                        offsetY: donutOffsetY,

                                        donut: {

                            size: '80%',

                                labels: {
                                show: true,
                                    name: {
                                    show: true,
                                        offsetY: this.props.remedData.offsetYName
                                },
                                value: {
                                    offsetY: this.props.remedData.offsetYValue,
                                    show: true,
                                    color: '#6D6D6D',
                                    fontWeight: 'bold',
                                    fontSize: fontSizeValue,
                                    fontFamily: 'arial'
                                },                               
                                total: {
                                    label: this.props.remedData.label,
                                    value: 100,
                                    show: true,
                                    color: '#6D6D6D',
                                    fontSize: fontSizeName,
                                    fontWeight: 'bold',
                                   fontFamily: 'arial',
                                   formatter: function (val) {
                                       return remedCheckCount;
                                    },
                                }
                            }
                        }

                    }
                }

            }

          
        }

       

        return (

            <div className="donut">
                {this.props.fromScreen === 'AllRemediations' ?
                    <Chart options={optionsAllRemed} series={series} type="donut" width="380" />
                    :
                    <Chart options={optionsCheckStatus} series={series} type="donut" width="380" />
                }
            </div>
        )
    }
}
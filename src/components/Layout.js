import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { NavHeader } from './NavHeader';
import { NavFooter } from './NavFooter';
import { NavMenu } from './NavMenu';

export class Layout extends Component {
  static displayName = Layout.name;

  render () {
    return (
      <div>
        <NavHeader />
        <NavMenu/>
        <Container fluid>
          {this.props.children}
        </Container>
        <NavFooter />
      </div>
    );
  }
}

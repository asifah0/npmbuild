﻿import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';

import '../css/css/bootstrap.css';
import '../css/css/bootstrap-grid.css';
import '../css/css/bootstrap-grid.rtl.css';
import '../css/css/bootstrap.min.css';

import '../css/css/custom-styles.css';
import '../css/css/table.styles.css';
import '../css/css/media-query.css';
import '../css/font-awesome/css/fontawesome.css';
import '../css/font-awesome/css/fontawesome.min.css';
import '../css/font-awesome/css/all.css';
import '../css/font-awesome/css/all.min.css';
import '../css/font-awesome/css/regular.css';
import '../css/font-awesome/css/regular.min.css';

export class NavFooter extends Component {
    //static displayName = "Broadridge";

    render() {
        return (        
                <footer id="footer">
                    <div className="row ">

                        <div className="col-lg-6 text-left">© 2021 Broadridge Financial Solutions, Inc.  All Rights Reserved.<br /> The Broadridge logo is registered trademark of Broadridge Financial Solutions, Inc.</div>
                        <div className="col-lg-6 text-right">
                            <ul className="footer-links">
                                <li><a href="#">Accessibility Statement</a></li>
                                <li><a href="#">Privacy Statement</a></li>
                                <li><a href="#">Terms of Use and Linking Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </footer>           
        );
    }
}

﻿import React, { Component } from "react";

import { getFormattedDate, numberWithCommas, numberWithCommasAmount } from '../../Shared/Util/CommonFunctions';

export default class PrintRemediations extends Component {

    render() {
        

        return (
            <table id="clientlist" className="printContent">
                <thead>
                    <tr>
                        <th class="col-sm-2 text-left">Remed.#</th>
                        <th class="col-sm-2 text-left">Remed.Name</th>
                        <th class="col-sm-2 text-left">Remed. Status</th>
                        <th class="col-sm-2 text-left">Pymts</th>
                        <th class="col-sm-2 text-left">Pymt Amount</th>
                        <th class="col-sm-2 text-left"># Check Stubs</th>
                        <th class="col-sm-2 text-left">Mail Date</th>
                        <th class="col-sm-2 text-left">Outstanding Pymts</th>
                        <th class="col-sm-2 text-left">Voided Checks</th>
                        <th class="col-sm-2 text-left">Void Date</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.results.map(result =>
                        <tr key={result.remedSk}>
                            <td class="col-sm-2 text-left">{result.remedNumber}</td>
                            <td class="col-sm-2 text-left">{result.remedName}</td>
                            <td class="col-sm-2 text-left">{result.status}</td>
                            <td class="col-sm-2 text-left">{numberWithCommas(result.noOfPayments)}</td>

                            <td class="col-sm-2 text-left">{numberWithCommasAmount(result.paymentAmount)}</td>
                            <td class="col-sm-2 text-left">{numberWithCommas(result.noOfCheckStubs)}</td>
                            <td class="col-sm-2 text-left">{getFormattedDate(result.checkMailDt)}</td>
                            <td class="col-sm-2 text-left">{numberWithCommasAmount(result.outstandingPayments)}</td>
                            <td class="col-sm-2 text-left">{numberWithCommas(result.voidedChecks)}</td>
                            <td class="col-sm-2 text-left">{getFormattedDate(result.voidDt)}</td>

                        </tr>
                    )}
                </tbody>
            </table>


        );
    }
}

﻿import React, { Component } from "react";

import { getFormattedDate } from '../../Shared/Util/CommonFunctions';

export default class PrintSearchResults extends Component {

    render() {
        return (
            <table id="clientlist" className="printContent">
                <thead>
                    <tr>
                        <th class="col-sm-2 text-left">Remed.#</th>
                        <th class="col-sm-2 text-left">Remed.Name</th>
                        <th class="col-sm-2 text-left">Pymts</th>
                        <th class="col-sm-2 text-left">Pymt Amount</th>
                        <th class="col-sm-2 text-left"># Check Stubs</th>
                        <th class="col-sm-2 text-left">Mail Date</th>
                        <th class="col-sm-2 text-left"># Uncashed</th>
                        <th class="col-sm-2 text-left"># Undeliverables</th>
                        <th class="col-sm-2 text-left"># Reissues</th>
                        <th class="col-sm-2 text-left"># Voided</th>
                        <th class="col-sm-2 text-left">Void Date</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.results.map(result =>
                        <tr key={result.remedSk}>
                            <td class="col-sm-2 text-left">{result.remedNumber}</td>
                            <td class="col-sm-2 text-left">{result.remedName}</td>
                            <td class="col-sm-2 text-left">{result.noOfPayments}</td>

                            <td class="col-sm-2 text-left">{result.paymentAmount}</td>
                            <td class="col-sm-2 text-left">{result.noOfCheckStubs}</td>
                            <td class="col-sm-2 text-left">{getFormattedDate(result.checkMailDt)}</td>
                            <td class="col-sm-2 text-left">{result.uncashed}</td>
                            <td class="col-sm-2 text-left">{result.undeliverables}</td>
                            <td class="col-sm-2 text-left">{result.reissues}</td>
                            <td class="col-sm-2 text-left">{result.voidedChecks}</td>
                            <td class="col-sm-2 text-left">{getFormattedDate(result.voidDt)}</td>

                        </tr>
                    )}
                </tbody>
            </table>


        );
    }
}

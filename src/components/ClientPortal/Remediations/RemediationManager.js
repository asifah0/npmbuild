﻿import React, { Component} from 'react';
import { Container } from 'reactstrap';

import { ActiveRemediations } from '../../Shared/ActiveRemediations';
import { SearchRemediation } from './SearchRemediation';
import { SearchRemediationResults } from './SearchRemediationResults';

export class RemediationManager extends Component {


    constructor(props) {
        super(props);

            this.state = {
                searchInput: ''
                };
        
    }

    handleSearch = (input) => {
        this.setState({
            searchInput: input
        });
    };

    componentWillReceiveProps(nextProps) {

    }

    render() {
        return (
            <Container fluid className="content-section">

                <h1 className="h2-title bold">All Remediations</h1>

                <div class="row mt-2 mb-3">

                    <div className="col-lg-5">
                        <div className="box box-border box-height">
                            <SearchRemediation onSimpleSearch={this.handleSearch} />
                        </div>
                    </div>


                    <div className="col-lg-7">
                        <div className="box box-border box-height overflow-hidden" >
                            <ActiveRemediations source='1'/>
                        </div>
                    </div>

                </div>


                <div className="row mt-3">

                    <div className="col-lg-12">
                        <div className="box box-border consumer">
                            <SearchRemediationResults dataFromParent={this.state.searchInput} />
                        </div>
                    </div>

                </div>
            </Container>


        );
    }
}

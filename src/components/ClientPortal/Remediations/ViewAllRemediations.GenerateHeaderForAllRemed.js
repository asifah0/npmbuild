﻿import React from 'react';

export const generateHeaderForAllRemed = (requestSort, getClassNamesFor) => {
    return (
        <>           
            <th style={{ width: '10%'}}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('RemedNo')}
                    className={getClassNamesFor('RemedNo')}
                >
                    Remed. #
            </button>
            </th>

            <th style={{ width: '12%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('RemedName')}
                    className={getClassNamesFor('RemedName')}
                >
                    Remed. Name
                    </button>
            </th>

            <th class="text-left" style={{ width: '8%' }}>
                <button
                    type="button"
                    onClick={() => requestSort('RemedStatus')}
                    className={getClassNamesFor('RemedStatus')}
                >
                    Remed. Status
                    </button>
            </th>

            <th style={{ width: '8%' }}>
                <button style={{ textAlign: 'right', paddingRight: '0' }}
                    type="button"
                    onClick={() => requestSort('NoOfPayments')}
                    className={getClassNamesFor('NoOfPayments')}
                >
                   # Pymts
                    </button>
            </th>


            <th style={{ width: '12%' }}>
                <button style={{ textAlign: 'right', paddingRight: '0' }}
                    type="button"
                    onClick={() => requestSort('TotalPaymentAmt')}
                    className={getClassNamesFor('TotalPaymentAmt')}
                >
                    Pymts Amount
                    </button>
            </th>


            <th style={{ width: '15%' }}>
                <button style={{ textAlign: 'right', paddingRight: '0' }}
                    type="button"
                    onClick={() => requestSort('NoOfCheckStubs')}
                    className={getClassNamesFor('NoOfCheckStubs')}
                >
                    # Check Stubs
                    </button>
            </th>

            <th style={{ width: '8%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('MailDate')}
                    className={getClassNamesFor('MailDate')}
                >
                    Mail Date
                 </button>
            </th>

            <th class="text-left" style={{ width: '10%' }}>
                <button
                    type="button"
                    onClick={() => requestSort('OutstandingPayments')}
                    className={getClassNamesFor('OutstandingPayments')}
                >
                    Outstanding Pymts
                 </button>
            </th>

            <th class="text-left" style={{ width: '8%' }}>
                <button
                    type="button"
                    onClick={() => requestSort('NoOfVoidedChecks')}
                    className={getClassNamesFor('NoOfVoidedChecks')}
                >
                    Voided Checks
                    </button>
            </th>


            <th class="text-left" style={{ width: '14%' }}>
                <button
                    type="button"
                    onClick={() => requestSort('VoidDate')}
                    className={getClassNamesFor('VoidDate')}
                >
                     Void Date
                    </button>
            </th>

           
        </>
    );
}

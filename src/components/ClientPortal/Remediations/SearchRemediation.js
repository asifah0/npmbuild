﻿import React, { Component } from 'react';
import CurrencyInput from 'react-currency-input-field';
//import SlidingPane from "react-sliding-pane";
import SlidingPane from '../../Shared/SlidingPane';
import Dialog from 'react-bootstrap-dialog';
import axios from 'axios';
import * as ReactBootStrap from 'react-bootstrap';
import DatePicker from "react-datepicker";

import { showMsg } from '../../Shared/Util/CommonFunctions';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';

export class SearchRemediation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            clientId: '',
            txtName: '',
            txtRemStatus: '',
            fieldconditionCount: '',
            fieldconditionAmt: '',
            txtMailDate: '',
            txtMailDateTo: '',
            txtCheckStaleDate: '',
            txtCheckStaleDateTo: '',
            results: [],
            filteredResults: [],
            lstStatusLookup: [],
            loading: false,
            inprocessclicked: true,
            mailedclicked: false,
            isServerError: false,
            isAdvanceSearchPaneOpen: false
        };



    }
    clearAdvanceSearchScreen = () => {
        this.setState({ txtName1: '' })
        this.setState({ txtMailDate: '' })
        this.setState({ txtMailDateTo: '' })
        this.setState({ txtCheckStaleDate: '' })
        this.setState({ txtCheckStaleDateTo: '' })
        
        this.setState({ txtNumOfPayments: '' })
        this.setState({ txtPaymentAmount: '' })
        this.setState({ txtRemStatus: '' })
        this.setState({ fieldconditionCount: '' })
        this.setState({ fieldconditionAmt: '' })
    }
    advanceSearchOnClick = (e) => {
        this.setState({ isAdvanceSearchPaneOpen: true })
    }

    componentDidMount = () => {
        this.fetchAdvSearchLookupInfo();
    }
    fetchAdvSearchLookupInfo = () => {
        this.setState({
            loading: true,
            isServerError: false
        });

        const urlSuffix = `RemediationSearch/Lookup`;
        const Url = getClientServiceURL(urlSuffix);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                this.setState({
                    lstStatusLookup: res.data,
                    loading: false
                })
            })
            .catch((error) => {
                if (axios.isCancel(error) || error) {
                }

                this.setState({
                    loading: false,
                    isServerError: true
                });
            });
    }
    isTextNullOrEmpty = (text) => {
        if (text === null || text === undefined || text.trim() === '') {
            return (true);
        }
        else {
            return (false);
        }
    }
    isTextNull = (text) => {
        if (text == null || text === undefined || text === '') {
            return (true);
        }
        else {
            return (false);
        }
    }
    handleSearchOnClick = () => {
        if (this.isTextNullOrEmpty(this.state.txtName)) {
            showMsg(this.dialog, 'All Remediations', 'Please enter the search text.');
            return;
        }
        this.clearAdvanceSearchScreen();
        let input = { isAdvanceSearch: false, inputText: this.state.txtName };
        this.props.onSimpleSearch(input);
    }

    handleAdvSearchOnClick = () => {
        const bEmptyRemNumber = this.isTextNullOrEmpty(this.state.txtName1);
        const bEmptyMailDate = this.isTextNull(this.state.txtMailDate);
        const bEmptyCheckStaleDate = this.isTextNull(this.state.txtCheckStaleDate);
        const bEmptyNumOfPayments = this.isTextNullOrEmpty(this.state.txtNumOfPayments);
        const bEmptyPaymentAmount = this.isTextNullOrEmpty(this.state.txtPaymentAmount);
        const bEmptyRemStatus = this.isTextNullOrEmpty(this.state.txtRemStatus);

        const bEmptyMailDateTo = this.isTextNull(this.state.txtMailDateTo);
        const bEmptyCheckStaleDateTo = this.isTextNull(this.state.txtCheckStaleDateTo);

        if (bEmptyRemNumber === true && bEmptyMailDate === true && bEmptyCheckStaleDate === true && bEmptyMailDateTo === true && bEmptyCheckStaleDateTo === true && bEmptyNumOfPayments === true && bEmptyPaymentAmount === true && bEmptyRemStatus === true) {
            showMsg(this.dialog, 'All Remediations', 'Please enter the value.');
            return;
        }

        if (bEmptyNumOfPayments === false) {
            const bEmptyCountOperator = this.isTextNullOrEmpty(this.state.fieldconditionCount);
            if (bEmptyCountOperator === true) {
                showMsg(this.dialog, 'All Remediations', 'Please enter the value.');
                return;
            }
        }
        if (bEmptyPaymentAmount === false) {
            const bEmptyAmountOperator = this.isTextNullOrEmpty(this.state.fieldconditionAmt);
            if (bEmptyAmountOperator === true) {
                showMsg(this.dialog, 'All Remediations', 'Please enter the value.');
                return;
            }
        }
        this.setState({ isAdvanceSearchPaneOpen: false, txtName:'' })
        let input = { remedNumber: '', isAdvanceSearch: true, sdatefrom: '', sdateto: '', mdatefrom: '', mdateto: '', numOfPayments: '', countOperator: '', amount: '', amountOperator: '', inputText: '', status: '' };
        if (!bEmptyRemNumber) input.remedNumber = this.state.txtName1.trim();
        if (!bEmptyMailDate) input.mdatefrom = this.state.txtMailDate.toLocaleDateString("en-US");
        if (!bEmptyMailDateTo) input.mdateto = this.state.txtMailDateTo.toLocaleDateString("en-US");
        if (!bEmptyCheckStaleDate) input.sdatefrom = this.state.txtCheckStaleDate.toLocaleDateString("en-US");
        if (!bEmptyCheckStaleDateTo) input.sdateto = this.state.txtCheckStaleDateTo.toLocaleDateString("en-US");

        if (!bEmptyNumOfPayments) input.numOfPayments = this.state.txtNumOfPayments;
        if (!bEmptyPaymentAmount) input.amount = this.state.txtPaymentAmount;
        if (!bEmptyRemStatus) input.status = this.state.txtRemStatus;
        input.countOperator = this.state.fieldconditionCount;
        input.amountOperator = this.state.fieldconditionAmt;
        this.props.onSimpleSearch(input);

    }
    advanceSearchOnClick = (e) => {
        this.setState({ isAdvanceSearchPaneOpen: true })
    }
    onInputchange = (event) => {
        const value = event.target.value;
        const regex = /^[a-z\d\s]+$/i;
        if (value.match(regex) || value === "") {
            this.setState({ [event.target.name]: event.target.value });
        }
    }
    handlesStartDateChange = (date) => {
        if (date !== null) {
            this.setState({ txtMailDate: date });
            const EndDt = this.state.txtMailDateTo;
            const StartDt = date;
            if (EndDt !== '' && EndDt < StartDt) {
                this.setState({ txtMailDateTo: '' });
            }
        } else {
            this.setState({ txtMailDate: '' });
        }
    }
    handlesEndDateChange = (date) => {
        if (date !== null) {
            this.setState({ txtMailDateTo: date });
        } else {
            this.setState({ txtMailDateTo: '' });
        }
    }
    handlesStartStaleDateChange = (date) => {
        if (date !== null) {
            this.setState({ txtCheckStaleDate: date });
            const EndDt = this.state.txtCheckStaleDateTo;
            const StartDt = date;
            if (EndDt !== '' && EndDt < StartDt) {
                this.setState({ txtCheckStaleDateTo: '' });
            }
        } else {
            this.setState({ txtCheckStaleDate: '' });
        }
    }
    handlesEndStaleDateChange = (date) => {
        if (date !== null) {
            this.setState({ txtCheckStaleDateTo: date });
        } else {
            this.setState({ txtCheckStaleDateTo: '' });
        }
    }
    handlesKeyPress = (e) => {
        const re = /[0-9kmbKMB]+/g;
        if (!re.test(e.key)) {
            e.preventDefault();
        }
    }
    handlesKeyPressDecimal = (e) => {
        const re = /[0-9kmbKMB.]+/g;
        if (!re.test(e.key)) {
            e.preventDefault();
        }
    }
    handleNoOfPaymentsChange = (e) => {
        let value = e.target.value;

        if (value.length > 10)
            return;

        if (value.toLowerCase().includes('k') ||
            value.toLowerCase().includes('m') ||
            value.toLowerCase().includes('b') ||
            value.toLowerCase().includes(',')) {
            value = this.getValue(value);
        }

        let valueText = '' + value;
        if (isNaN(value)) value = '';
        
        this.setState({
            [e.target.name]: value
        });
    }
    handlePaymentAmtChange = (e) => {
        let value = e.target.value;

        if (value.length > 15)
            return;

        if (value.includes('$')) {
            value = value.replace('$', '');
        }
        if (value.toLowerCase().includes('k') ||
            value.toLowerCase().includes('m') ||
            value.toLowerCase().includes('b') ||
            value.toLowerCase().includes(',')) {
            value = this.getValue(value);
        }

        let valueText = '' + value;
        if (isNaN(value)) value = '';
        
        this.setState({
            [e.target.name]: value
        });
    }
    getValue(amount) {
        if (amount.toLowerCase() === 'k' || amount.toLowerCase() === 'm' || amount.toLowerCase() === 'b') {
            amount = '';
            return amount;
        }
        while (amount.includes(',')) {
            amount = amount.replace(',', '');
        }
        if (amount.toLowerCase().includes('k') || amount.includes('K')) {
            amount = amount.toLowerCase().replace('k', '') * 1000;
            return amount;
        }
        if (amount.toLowerCase().includes('m') || amount.includes('M')) {
            amount = amount.toLowerCase().replace('m', '') * 1000000;
            return amount;
        }
        if (amount.toLowerCase().includes('b') || amount.includes('B')) {
            amount = amount.toLowerCase().replace('b', '') * 1000000000;
            return amount;
        }
        return amount;
    }
    onRowDropdownChangEvent = (e) => {
        let select = e.target;
        let name = select.name;
        let workItemType = select.options[select.selectedIndex];
        this.setState({ [name]: workItemType.value })
    }
    render() {

        return (

            <>
                <div class="col">

                    <div>
                        <h1 class="h1-title">Search</h1>
                    </div>
                    <label class="mt-4">Search Remediation Number</label>
                    <div id="custom-search-input">
                        <div class="input-group">

                            <input name="txtName" type="text" maxLength="20" class="  search-query form-control" value={this.state.txtName} onChange={this.onInputchange} />
                            <span class="input-group-btn">
                                <button class="btn btn-secondary s-icon" type="button" onClick={this.handleSearchOnClick}>
                                    <i class="fas fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>

                    <div class="mt-4 mb-4">
                        <button type="button" class="btn btn-primary menu-button" onClick={this.advanceSearchOnClick}>Advanced Search</button>
                        <SlidingPane
                            className="rightSide-sliding-pane"
                            overlayClassName="some-custom-overlay-class"
                            isOpen={this.state.isAdvanceSearchPaneOpen}
                            title="Advanced Search"
                            subtitle=""
                            from='right'
                            width='30%'

                            onRequestClose={() => {
                                this.setState({ isAdvanceSearchPaneOpen: false });
                            }}
                        >
                            <div>
                                <div class="row justify-content-center">
                                    <div class="col-lg-12 ">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Remediation Number</label>
                                                    <input name="txtName1" type="text" maxLength="20" class="  search-query form-control" value={this.state.txtName1} onChange={this.onInputchange} />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Remediation Mail Date From:</label>
                                                    {/*<input name="txtMailDate" class="form-control" type="date" placeholder="" value={this.state.txtMailDate} onChange={this.onInputchange} />*/}

                                                    <DatePicker className="form-control"
                                                        selected={this.state.txtMailDate}
                                                        onChange={(date) => this.handlesStartDateChange(date)}
                                                        selectsStart
                                                        startDate={this.state.txtMailDate}
                                                        endDate={this.state.txtMailDateTo}
                                                        placeholderText={"mm/dd/yyyy"}
                                                    />



                                                    <label class="mt-2">To:</label>

                                                    <DatePicker className="form-control"
                                                        selected={this.state.txtMailDateTo}
                                                        onChange={(date) => this.handlesEndDateChange(date)}
                                                        selectsEnd
                                                        startDate={this.state.txtMailDate}
                                                        endDate={this.state.txtMailDateTo}
                                                        minDate={this.state.txtMailDate}
                                                        placeholderText={"mm/dd/yyyy"}
                                                    />

                                                    {/*<input name="txtMailDateTo" class="form-control" type="date" placeholder="" value={this.state.txtMailDateTo} onChange={this.onInputchange} />*/}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">


                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Check Stale Date From:</label>
                                                    {/*<input name="txtCheckStaleDate" class="form-control" type="date" placeholder="" value={this.state.txtCheckStaleDate} onChange={this.onInputchange} />*/}
                                                    <DatePicker className="form-control"
                                                        selected={this.state.txtCheckStaleDate}
                                                        onChange={(date) => this.handlesStartStaleDateChange(date)}
                                                        selectsStart
                                                        startDate={this.state.txtCheckStaleDate}
                                                        endDate={this.state.txtCheckStaleDateTo}
                                                        placeholderText={"mm/dd/yyyy"}
                                                    />

                                                    <label class="mt-2">To:</label>
                                                    <DatePicker className="form-control"
                                                        selected={this.state.txtCheckStaleDateTo}
                                                        onChange={(date) => this.handlesEndStaleDateChange(date)}
                                                        selectsEnd
                                                        startDate={this.state.txtCheckStaleDate}
                                                        endDate={this.state.txtCheckStaleDateTo}
                                                        minDate={this.state.txtCheckStaleDate}
                                                        placeholderText={"mm/dd/yyyy"}
                                                    />
                                                    {/*<input name="txtCheckStaleDateTo" class="form-control" type="date" placeholder="" value={this.state.txtCheckStaleDateTo} onChange={this.onInputchange} />*/}

                                                </div>
                                            </div>



                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Number of Payments</label>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <select class="form-control" name="fieldconditionCount" value={this.state.fieldconditionCount} onChange={this.onRowDropdownChangEvent}>
                                                                <option value=""></option>
                                                                <option value="eq">Equal To</option>
                                                                <option value="lt">Less Than</option>
                                                                <option value="gt">Greater Than</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <CurrencyInput className="form-control" allowNegativeValue={false} name="txtNumOfPayments" allowDecimals={false} value={this.state.txtNumOfPayments} onChange={this.handleNoOfPaymentsChange}
                                                                onKeyPress={(e) => this.handlesKeyPress(e)} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Amount of Payments</label>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <select class="form-control" name="fieldconditionAmt" value={this.state.fieldconditionAmt} onChange={this.onRowDropdownChangEvent}>
                                                                <option value=""></option>
                                                                <option value="eq">Equal To</option>
                                                                <option value="lt">Less Than</option>
                                                                <option value="gt">Greater Than</option>
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                            <CurrencyInput className="form-control" allowNegativeValue={false} name="txtPaymentAmount" value={this.state.txtPaymentAmount} allowDecimals={true} decimalsLimit={2} onChange={this.handlePaymentAmtChange}
                                                                onKeyPress={(e) => this.handlesKeyPressDecimal(e)} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Remediation Status</label>
                                                    <select class="form-control" name="txtRemStatus" value={this.state.txtRemStatus} onChange={this.onRowDropdownChangEvent}>
                                                        {this.state.lstStatusLookup.map((row) => <option value={row.key}>{row.text}</option>)}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-lg-3 text-left ">

                                                <button class="btn btn-border text-left" onClick={this.clearAdvanceSearchScreen}>
                                                    {/*<i class="fas fa-eraser"></i>*/}
                                                Clear
                                                </button>

                                            </div>
                                            <div class="col-lg-9 text-right">

                                                <button class="btn btn-primary ml-4" style={{ width: '160px' }} onClick={this.handleAdvSearchOnClick}>Search</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>


                        </SlidingPane>
                    </div>

                    {this.state.loading && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
                    {this.state.isServerError && <div class="label-error-red"> <span>Unable to get the data due to internal server error. Please try after sometimes.</span></div>}
                </div>

                <Dialog ref={(el) => { this.dialog = el }} />


            </>

        );
    }
}
﻿import React, { Component } from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import axios from 'axios';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import { saveAs } from 'file-saver';

import { generateHeaderForAllRemed } from './ViewAllRemediations.GenerateHeaderForAllRemed';
import PrintRemediations from './PrintRemediations';
import { RemediationDetails } from '../../Shared/RemediationDetails';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';
import { numberWithCommas, numberWithCommasAmount, getFormattedDate } from '../../Shared/Util/CommonFunctions';
import { GetErrorMessageDescription } from '../../Shared/Util/CommonFunctions';

export class ViewAllRemediations extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputText: props.dataFromParent,
            resultsToShow: [],
            next: props.fromComp === 'All' ? 5 : 0,
            total: 0,
            pageSize: 5,
            isRemedDetailsOpen: false,
            columnKey: props.fromComp === 'All' ? 'MailDate' : 'checkMailDt',
            columnSortDirection: props.fromComp === 'All' ? 'desc' : '-',
            totalCountOfRecords: 0,
            totalAmount: 0,
            totalNumberOfPayments: 0,
            loading: false,
            isServerError: false,
            remedItem: [],
            loadingExport: false,
            exportURL: ''
        };        
    }


    handleClose = () => {
        this.setState({ isRemedDetailsOpen: false })
    }

    remedNoOnClick = (item) => {
        this.setState({
            isRemedDetailsOpen: true,
            remedItem: item
        })

    }
    componentWillReceiveProps(nextProps) {
        let searchPattern = nextProps.dataFromParent;


    }
    exportToExcelClick = () => {
        const fileName = 'Remediations.xlsx';
        const excelSheetName = 'Remediations';

        this.setState({
            loadingExport: true,
            isServerError: false,
            serverErrorMessage: ''
        });

        const Url = this.state.exportURL;

        axios.get(Url,
            {
                responseType: 'arraybuffer'
            })
            .then((response) => {

                const blob = new Blob([response.data], {
                    type: 'application/octet-stream'
                })
                saveAs(blob, fileName);

                this.setState({
                    loadingExport: false,
                    isServerError: false
                });
            })
            .catch((error) => {
                this.setState({
                    loadingExport: false,
                    serverErrorMessage: GetErrorMessageDescription(error),
                    isServerError: true
                });
            });
    }

    fetchSearchResult = (input, offset, limit, sortColumn, sortDirection) => {
        this.setState({
            isServerError: false
        });

        let urlSuffix;
        let urlBasePrefix = ``;
        if (this.props.fromComp === 'All') {
            urlBasePrefix = `RemediationDashboard/GetAllRemediationsSummary`;
            urlSuffix = `?noOfRows=${limit}&sortColumn=${sortColumn}&sortDirection=${sortDirection}`;
        }
        else {
            urlBasePrefix = `RemediationManager/AllActiveRemediations`;
            urlSuffix = `?offset=${offset}&limit=${limit}&sort=${sortDirection + sortColumn}`;
        }
        let urlSuffixForExport = urlBasePrefix + `/ExportResults` + urlSuffix;
        urlSuffix = urlBasePrefix + urlSuffix;

        const Url = getClientServiceURL(urlSuffix);
        this.state.exportURL = getClientServiceURL(urlSuffixForExport);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                if (this.props.fromComp === 'All') {
                    this.setState({
                        resultsToShow: res.data.remediationsVMs,
                        totalCountOfRecords: res.data.remediationsSummaryCountVM.remedCount,
                        totalAmount: res.data.remediationsSummaryCountVM.paymentAmount,
                        totalNumberOfPayments: res.data.remediationsSummaryCountVM.noOfPayments,
                        loading: true
                    })
                }
                else {

                    const limit1 = res.data.limit;
                    const offSet1 = res.data.offSet;
                    const payload = res.data.payLoad;
                    const total = res.data.total;

                    let resultList = [];
                    let arrayLength = payload.length;
                    for (let i = 0; i < arrayLength; i++) {
                        resultList.push(payload[i]);
                    }

                    this.setState({
                        inputText: input,
                        results: resultList,
                        resultsToShow: resultList,
                        next: resultList.length,
                        total: total,
                        totalCountOfRecords: res.data.total,
                        totalAmount: res.data.totalAmount,
                        totalNumberOfPayments: res.data.noOfPayments,
                        loading: true
                    })
                }

            })
            .catch((error) => {
                if (axios.isCancel(error) || error) {
                    
                }

                this.setState({
                    loading: true,
                    isServerError: true
                });
            });
    }


    componentDidMount = () => {
        this.setState({ resultsToShow: [] });
        this.fetchSearchResult('', 0, this.state.pageSize, this.state.columnKey, this.state.columnSortDirection);
    }

    getClassNamesFor = (name) => {
        if (this.props.fromComp === 'All') {
            if (this.state.columnKey === name) {
                if (this.state.columnSortDirection === 'desc') {
                    return 'ascending';
                }
                else {
                    return 'descending';
                }
            }
            else {
                return undefined;
            }
        }
        else {
            if (this.state.columnKey === name) {
                if (this.state.columnSortDirection === '') {
                    return 'ascending';
                }
                else {
                    return 'descending';
                }
            }
            else {
                return undefined;
            }
        }
    };

    requestSort = (key) => {
        this.setState({ total: 0, resultsToShow: [] });

        if (this.props.fromComp === 'All') {

            if (this.state.columnKey === key && this.state.columnSortDirection === 'asc') {
                //this.setState({ columnSortDirection: 'desc' });
                this.state.columnSortDirection = 'desc';
            }
            else {
                //this.setState({ columnSortDirection: 'asc' });
                this.state.columnSortDirection = 'asc';
            }
            this.setState({ columnKey: key });

            this.setState({
                pageSize: 5,
                next:5
            })

            this.fetchSearchResult('', 0, this.state.pageSize, key, this.state.columnSortDirection);
        }
        else {
            if (this.state.columnKey === key && this.state.columnSortDirection === '') {
                this.state.columnSortDirection = '-';
            }
            else {
                this.state.columnSortDirection = ''
            }
            this.state.columnKey = key;
            let pageSize1 = this.state.pageSize;

            this.fetchSearchResult(this.state.inputText, 0, pageSize1, key, this.state.columnSortDirection);
        }
        

       
    };

    handleShowMorePosts = () => {

        if (this.props.fromComp === 'All') {
            this.fetchSearchResult('', '', (this.state.pageSize + this.state.next), this.state.columnKey, this.state.columnSortDirection);
            this.setState((state, props) => ({ next: state.next + state.pageSize }));
        } else {

            this.fetchSearchResult(this.state.inputText, this.state.next, this.state.pageSize, this.state.columnKey, this.state.columnSortDirection);
        }
    };



    generateTableData = () => {
        const remData = this.state.resultsToShow;

        return remData.map((item, index) => {

            return (
                <>
                    <tr key={item.remedSK}>
                        <td class="text-left">{item.remedNumber}  </td>
                        <td class="text-left">{item.remedName}</td>
                        <td class="text-left">{item.status}</td>
                        <td class="text-left">{numberWithCommas(item.noOfPayments)}</td>
                        <td class="text-left">{numberWithCommasAmount(item.paymentAmount)}</td>
                        <td class="text-left">{numberWithCommas(item.noOfCheckStubs)}</td>
                        <td class="text-left">{getFormattedDate(item.checkMailDt)}</td>
                        <td class="text-left">{numberWithCommasAmount(item.outstandingPayments)}</td>
                        <td class="text-left">{numberWithCommas(item.voidedChecks)}</td>
                        <td class="text-left">{getFormattedDate(item.voidDt)}</td>
                    </tr>
                    <ReactBootStrap.Modal
                        show={this.state.isRemedDetailsOpen}
                        onHide={this.handleClose}
                        backdrop="static"
                        keyboard={false}
                        dialogClassName="my-modal-fullscreen"
                        id="tableModal"
                    >
                        <ReactBootStrap.Modal.Header closeButton>
                            <ReactBootStrap.Modal.Title>Remediations Details</ReactBootStrap.Modal.Title>
                        </ReactBootStrap.Modal.Header>
                        <ReactBootStrap.Modal.Body>
                            <RemediationDetails RemedItem={this.state.remedItem} />
                        </ReactBootStrap.Modal.Body>
                    </ReactBootStrap.Modal>
                </>
            )
        })
    }
    getExportData = () => {
        let dataTable = [];
        const results = this.state.resultsToShow;

        if (results.length > 0) {
            if (results) {
                for (let i in results) {
                    let obj = {
                        'Remed. #': results[i].remedNumber,
                        'Remediation Name': results[i].remedName,
                        'Remediation Status': results[i].status,
                        '# Pymts': results[i].noOfPayments,
                        'Pymts Amount': results[i].paymentAmount,
                        '# of Check Stubs': results[i].noOfCheckStubs,
                        'Mail Date': getFormattedDate(results[i].checkMailDt),
                        'Outstanding Pymts': results[i].outstandingPayments,
                        'Voided Checks': results[i].voidedChecks,
                        'Void Date': getFormattedDate(results[i].voidDt),
                    }
                    dataTable.push(obj);
                }
            }
        }
        return dataTable;
    }

    generateHeader = () => {
        return (
            <>
                <th class="text-left" style={{ width: '10%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('remedNumber')}
                        className={this.getClassNamesFor('remedNumber')}
                    >
                        Remed. #
            </button>
                </th>

                <th class="text-left" style={{ width: '12%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('remedName')}
                        className={this.getClassNamesFor('remedName')}
                    >
                        Remed. Name
                    </button>
                </th>

                <th class="text-left" style={{ width: '8%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('status')}
                        className={this.getClassNamesFor('status')}
                    >
                        Remed. Status
                    </button>
                </th>

                <th class="text-left" style={{ width: '8%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('noOfPayments')}
                        className={this.getClassNamesFor('noOfPayments')}
                    >
                        # of Pymts
                    </button>
                </th>

                <th class="text-left" style={{ width: '12%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('paymentAmount')}
                        className={this.getClassNamesFor('paymentAmount')}
                    >
                        Total Pymt Amount
                    </button>
                </th>


                <th class="text-left" style={{ width: '10%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('noOfCheckStubs')}
                        className={this.getClassNamesFor('noOfCheckStubs')}
                    >
                        # Check Stubs
                    </button>
                </th>

                <th class="text-left" style={{ width: '8%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('checkMailDt')}
                        className={this.getClassNamesFor('checkMailDt')}
                    >
                        Mail Date
                    </button>
                </th>

                <th class="text-left" style={{ width: '10%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('outstandingPayments')}
                        className={this.getClassNamesFor('outstandingPayments')}
                    >
                        Outstanding Pymts
                 </button>
                </th>

                <th class="text-left" style={{ width: '8%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('voidedChecks')}
                        className={this.getClassNamesFor('voidedChecks')}
                    >
                        Voided Checks
                    </button>
                </th>


                <th class="text-left" style={{ width: '14%' }}>
                    <button
                        type="button"
                        onClick={() => this.requestSort('voidDt')}
                        className={this.getClassNamesFor('voidDt')}
                    >
                        Void Date
                    </button>
                </th>
            </>
        );
    }
    render() {
        const imagePrint = require('../../../css/images/Print.svg');
        const imageExcel = require('../../../css/images/XL.svg');
        return (

            <>
                <div className="row mt-4 mb-3">

                    <div className="col-lg-3">
                        <div className="box gray-bg p-4">
                            <p>Remediations</p>
                            <div className="dsb-numbers">{numberWithCommas(this.state.totalCountOfRecords)}</div>
                        </div>
                    </div>


                    <div className="col-lg-3">
                        <div className="box gray-bg p-4">
                            <p>Payments</p>
                            <div className="dsb-numbers">{numberWithCommas(this.state.totalNumberOfPayments)}</div>
                        </div>
                    </div>


                    <div className="col-lg-3">
                        <div className="box gray-bg p-4">
                            <p>Total Amount</p>
                            <div className="dsb-numbers">{numberWithCommasAmount(this.state.totalAmount)}</div>
                        </div>
                    </div>

                </div>

                <div className="row mt-5 mb-3">

                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col">


                                <div className="box box-border consumer">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <h1 className="h1-title"> Remediation Details</h1>
                                        </div>

                                        {
                                            this.state.resultsToShow.length > 0 ?
                                        <div class="col-lg-6 text-right">
                                            <span>
                                                        <button className='btn btn-link' onClick={() => this.exportToExcelClick()}><img src={imageExcel} alt="ExportToExcel" /></button>
                                            </span>
                                            <span className="ml-2">
                                                <ReactToPrint content={() => this.componentRef}>
                                                    <PrintContextConsumer>
                                                        {({ handlePrint }) => (

                                                            <button className='btn btn-link' onClick={handlePrint}><img src={imagePrint} alt="Print" /></button>

                                                        )}
                                                    </PrintContextConsumer>
                                                </ReactToPrint>
                                                <PrintRemediations results={this.state.resultsToShow} ref={el => (this.componentRef = el)} />
                                            </span>
                                                </div>
                                                :
                                                undefined
                                        }
                                    </div>

                                    <div className="table-scroll" style={{ height: '300px' }}>
                                        {(this.state.loading === false || this.state.loadingExport) && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
                                        {this.state.isServerError && <div class="label-error-red"> <span>Unable to display the data due to internal server error. Please try after sometimes.</span></div>}
                                        <table id="example2" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr>
                                                    {this.props.fromComp === 'All' ? generateHeaderForAllRemed(this.requestSort, this.getClassNamesFor) : this.generateHeader()}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.loading ? this.generateTableData() :undefined}
                                            </tbody>

                                        </table>
                                    </div>

                                    <div class="row mt-30">
                                        <div class="col text-center">
                                            {this.state.resultsToShow.length > 0 ?
                                                <p>Viewing results 1-{this.state.resultsToShow.length} of {
                                                    this.props.fromComp === 'All' ? this.state.totalCountOfRecords : this.state.total
                                                }</p>
                                                :
                                                undefined
                                            }

                                            {this.state.resultsToShow.length < (this.props.fromComp === 'All' ? this.state.totalCountOfRecords : this.state.total )  ?
                                                <button type="button" class="btn btn-border" onClick={this.handleShowMorePosts}>Load More</button>
                                                :
                                                undefined
                                            }
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </>

        );
    }
}
﻿import React, { Component } from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import axios from 'axios';
import { saveAs } from 'file-saver';

import PrintSearchResults from './PrintSearchResults';
import { RemediationDetails } from '../../Shared/RemediationDetails';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';
import { numberWithCommas } from '../../Shared/Util/CommonFunctions';
import { numberWithCommasAmount } from '../../Shared/Util/CommonFunctions';
import { getFormattedDate } from '../../Shared/Util/CommonFunctions';
import { GetErrorMessageDescription } from '../../Shared/Util/CommonFunctions';

export class SearchRemediationResults extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchInput: props.dataFromParent,
            resultsToShow: [],
            next: 0,
            total: 0,
            pageSize: 6,
            remedSK: 0,
            isRemedDetailsOpen: false,
            columnKey: 'remedNumber',
            columnSortDirection: '',
            loading: true,
            isServerError: false,
            loadingExport: false,
            exportURL: ''
        };



    }

    exportToExcelClick = () => {
        const fileName = 'RemediationSearchResults.xlsx';
        const excelSheetName = 'RemediationSearchResults';

        this.setState({
            loadingExport: true,
            isServerError: false,
            serverErrorMessage: ''
        });

        const Url = this.state.exportURL;

        axios.get(Url,
            {
                responseType: 'arraybuffer'
            })
            .then((response) => {

                const blob = new Blob([response.data], {
                    type: 'application/octet-stream'
                })
                saveAs(blob, fileName);

                this.setState({
                    loadingExport: false,
                    isServerError: false
                });
            })
            .catch((error) => {
                this.setState({
                    loadingExport: false,
                    serverErrorMessage: GetErrorMessageDescription(error),
                    isServerError: true
                });
            });
    }

    handleClose = () => {
        this.setState({ isRemedDetailsOpen: false })
    }

    remedNoOnClick = (item) => {
        this.setState({
            isRemedDetailsOpen: true,
            remedSK: item.remedSk
        })
       
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.dataFromParent) {
            let searchInput = nextProps.dataFromParent;


            this.state.resultsToShow = [];
            this.setState({
                loading: false,
                total:0
            });
            this.fetchSearchResult(searchInput, 0, this.state.pageSize, this.state.columnKey, this.state.columnSortDirection);
        }
    }

    fetchSearchResult = (input, offset, limit, sortColumn, sortDirection) => {
        this.setState({
            isServerError: false
        });
        let urlSuffix = ``;
        let urlBasePrefix = ``;
        if (input.isAdvanceSearch === true) {
            urlBasePrefix = `RemediationSearch/AdvanceSearch`;
            urlSuffix = `?remedNumber=${encodeURIComponent(input.remedNumber)}&mDateFrom=${input.mdatefrom}&mDateTo=${input.mdateto}&sDateFrom=${input.sdatefrom}&sDateTo=${input.sdateto}&count=${input.numOfPayments}&countOperator=${input.countOperator}&amount=${input.amount}&amountOperator=${input.amountOperator}&status=${input.status}&offset=${offset}&limit=${limit}&sort=${sortDirection + sortColumn}`;
        }
        else {
            urlBasePrefix = `RemediationSearch`;
            urlSuffix = `?searchText=${encodeURIComponent(input.inputText)}&offset=${offset}&limit=${limit}&sort=${sortDirection + sortColumn}`;
        }

        let urlSuffixForExport = urlBasePrefix + `/ExportResults` + urlSuffix;
        urlSuffix = urlBasePrefix + urlSuffix;

        const Url = getClientServiceURL(urlSuffix);
        this.state.exportURL = getClientServiceURL(urlSuffixForExport);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                const payload = res.data.payLoad;
                const total = res.data.total;

                let resultList = [];
                let arrayLength = payload.length;
                for (let i = 0; i < arrayLength; i++) {
                    resultList.push(payload[i]);
                }



                this.setState({
                    searchInput: input,
                    results: resultList,
                    resultsToShow: resultList,
                    next: resultList.length,
                    total: total,
                    loading: true
                })

            })
            .catch((error) => {
                if (axios.isCancel(error) || error) {
                   
                }
                this.setState({
                    loading: true,
                    isServerError: true
                });
            });
    }


    componentDidMount = () => {

    }

    getClassNamesFor = (name, isright) => {
        let prefix = 'text-left';
        if (isright === true) prefix = 'text-left';

        if (this.state.columnKey === name) {
            if (this.state.columnSortDirection === '') {
                return prefix + ' ' + 'ascending';
            }
            else {
                return prefix + ' ' +'descending';
            }
        }
        else {
            return prefix;
        }
    };

    requestSort = (key) => {
        
        if (this.state.columnKey === key && this.state.columnSortDirection === '') {
            this.state.columnSortDirection = '-';
        }
        else
        {
            this.state.columnSortDirection = ''
        }
        this.state.columnKey = key;
        
        let pageSize1 = this.state.pageSize;

        this.state.resultsToShow = [];
        this.fetchSearchResult(this.state.searchInput, 0, pageSize1, key, this.state.columnSortDirection);
    };

    handleShowMorePosts = () => {
        this.fetchSearchResult(this.state.searchInput, this.state.next, this.state.pageSize, this.state.columnKey, this.state.columnSortDirection);
    };



    generateTableData = () => {
        const remData = this.state.resultsToShow;
        return remData.map((item, index) => {

            return (
                <>
                    <tr key={item.remedSk} >
                        <td class="text-left"><button class="btn btn-link" onClick={() => this.remedNoOnClick(item)}> {item.remedNumber} </button>  </td>

                        <td class="text-left">{item.remedName}</td>
                        <td class="text-left">{numberWithCommas(item.noOfPayments)}</td>
                        <td class="text-left">{numberWithCommasAmount(item.paymentAmount)}</td>
                        <td class="text-left">{numberWithCommas(item.noOfCheckStubs)}</td>
                        <td class="text-left">{getFormattedDate(item.checkMailDt)}</td>
                        <td class="text-left">{item.uncashed}</td>
                        <td class="text-left">{item.undeliverables}</td>
                        <td class="text-left">{item.reissues}</td>
                        <td class="text-left">{item.voidedChecks}</td>
                        <td>{getFormattedDate(item.voidDt)}</td>
                    </tr>
                    
                </>
            )
        })
    }

    getExportData = () => {
        let dataTable = [];
        const results = this.state.resultsToShow;

        if (results.length > 0) {
            if (results) {
                for (let i in results) {
                    let obj = {
                        'Remediation #': results[i].remedNumber,
                        'Remediation Name': results[i].remedName,
                        '# of Payments': results[i].noOfPayments,
                        'Payment Amount': results[i].paymentAmount,
                        '# of Check Stubs': results[i].noOfCheckStubs,
                        'Mail Date': getFormattedDate(results[i].checkMailDt),
                        '# of Uncashed': results[i].uncashed,
                        '# of Undelivered': results[i].undeliverables,
                        '# of Reissues': results[i].reissues,
                        '# of Voids': results[i].voidedChecks,
                        'Void Date': getFormattedDate(results[i].voidDt),
                    }
                    dataTable.push(obj);
                }
            }
        }
        return dataTable;
    }

    generateHeader = () => {
        return (
            <>
                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('remedNumber')}
                        className={this.getClassNamesFor('remedNumber', false)}
                    >
                        Remed. #
            </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('remedName')}
                        className={this.getClassNamesFor('remedName', false)}
                    >
                        Remed. Name
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('noOfPayments')}
                        className={this.getClassNamesFor('noOfPayments', true)}
                    >
                        # Pymts
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('paymentAmount')}
                        className={this.getClassNamesFor('paymentAmount', true)}
                    >
                        Pymt Amts
                    </button>
                </th>


                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('noOfCheckStubs')}
                        className={this.getClassNamesFor('noOfCheckStubs', true)}
                    >
                        # Check Stubs
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('checkMailDt')}
                        className={this.getClassNamesFor('checkMailDt', false)}
                    >
                        Mail Date
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('uncashed')}
                        className={this.getClassNamesFor('uncashed', true)}
                    >
                        # Uncashed
                 </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('undeliverables')}
                        className={this.getClassNamesFor('undeliverables', true)}
                    >
                        # Undeliverables
                    </button>
                </th>


                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('reissues')}
                        className={this.getClassNamesFor('reissues', true)}
                    >
                        # Reissued
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('voidedChecks')}
                        className={this.getClassNamesFor('voidedChecks', true)}
                    >
                        # Voided
                    </button>
                </th>

                <th>
                    <button
                        type="button"
                        onClick={() => this.requestSort('voidDt')}
                        className={this.getClassNamesFor('voidDt', false)}
                    >
                        Void Date
                    </button>
                </th>
            </>
        );
    }
    render() {
        const imagePrint = require('../../../css/images/Print.svg');
        const imageExcel = require('../../../css/images/XL.svg');
        return (

            <>

                <div class="row">
                    <div class="col-lg-6">

                        <span>
                        <h1 class="h1-title mb-2"> Search Results &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        {
                            (this.state.searchInput.isAdvanceSearch === false && this.state.loading) &&
                            <span>For: {this.state.searchInput.inputText}</span>
                        }
                        </h1></span>
                    </div>
                    {
                        this.state.resultsToShow.length > 0 ?
                            <div class="col-lg-6 text-right">
                                <span>
                                    <button className='btn btn-link' onClick={() => this.exportToExcelClick()}><img src={imageExcel} alt="ExportToExcel" /></button>
                                </span>
                                <span className="ml-2">
                                    <ReactToPrint content={() => this.componentRef}>
                                        <PrintContextConsumer>
                                            {({ handlePrint }) => (

                                                <button className='btn btn-link' onClick={handlePrint}><img src={imagePrint} alt="Print" /></button>

                                            )}
                                        </PrintContextConsumer>
                                    </ReactToPrint>
                                    <PrintSearchResults results={this.state.resultsToShow} ref={el => (this.componentRef = el)} />
                                </span>
                            </div>
                            :
                            undefined
                    }
                </div>
                <div class="tbl-box-heightsearchresults">
                    {(this.state.loading === false || this.state.loadingExport) && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
                    {this.state.isServerError && <div class="label-error-red"> <span>Unable to display the data due to internal server error. Please try after sometimes.</span></div>}
                    <table id="example" class="table table-striped table-bordered dataTable no-footer fixed_headers">
                        <thead>
                            <tr>
                                {this.generateHeader()}
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.loading ? this.generateTableData() : undefined}
                        </tbody>

                    </table>
                   
                </div>

                <div class="row mt-10">
                    <div class="col text-center">
                        {this.state.resultsToShow.length > 0 ?
                            <p>Viewing results 1-{this.state.resultsToShow.length} of {this.state.total}</p>
                            :
                            undefined
                        }

                        {this.state.resultsToShow.length < this.state.total ?
                            <button type="button" class="btn btn-border" onClick={this.handleShowMorePosts}>Load More</button>
                            :
                            undefined
                        }
                    </div>
                </div>
                <ReactBootStrap.Modal
                    show={this.state.isRemedDetailsOpen}
                    onHide={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    dialogClassName="my-modal-fullscreen"
                    id="tableModal"
                >
                    <ReactBootStrap.Modal.Header closeButton>
                        <ReactBootStrap.Modal.Title>Remediations Details</ReactBootStrap.Modal.Title>
                    </ReactBootStrap.Modal.Header>
                    <ReactBootStrap.Modal.Body>
                        <RemediationDetails remedSK={this.state.remedSK} />
                    </ReactBootStrap.Modal.Body>
                </ReactBootStrap.Modal>
            </>

        );
    }
}
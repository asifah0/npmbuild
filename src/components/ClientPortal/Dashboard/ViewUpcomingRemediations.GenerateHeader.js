﻿import React from 'react';

export const generateHeader = (requestSort, getClassNamesFor) => {
    return (
        <>           
            <th style={{ width: '10%'}}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('RemedNo')}
                    className={getClassNamesFor('RemedNo')}
                >
                    Remed. #
            </button>
            </th>

            <th style={{ width: '20%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('RemedName')}
                    className={getClassNamesFor('RemedName')}
                >
                    Remed. Name
                    </button>
            </th>

            <th style={{ width: '10%' }}>
                <button style={{ textAlign: 'right', paddingRight: '0' }}
                    type="button"
                    onClick={() => requestSort('NoOfPayments')}
                    className={getClassNamesFor('NoOfPayments')}
                >
                   Approx. # Pymts
                    </button>
            </th>


            <th style={{ width: '15%' }}>
                <button style={{ textAlign: 'right', paddingRight: '0' }}
                    type="button"
                    onClick={() => requestSort('TotalPaymentAmt')}
                    className={getClassNamesFor('TotalPaymentAmt')}
                >
                    Approx. Total Pymt Amount
                    </button>
            </th>


            <th style={{ width: '15%' }}>
                <button style={{ textAlign: 'right', paddingRight: '0' }}
                    type="button"
                    onClick={() => requestSort('NoOfCheckStubs')}
                    className={getClassNamesFor('NoOfCheckStubs')}
                >
                    Approx. # Check Stubs
                    </button>
            </th>

            <th style={{ width: '15%' }}>
                <button style={{ textAlign: 'left', paddingRight: '0', paddingLeft:'0' }}
                    type="button"
                    onClick={() => requestSort('DateReceived')}
                    className={getClassNamesFor('DateReceived')}
                >
                    Approx. File Receipt Date
                    </button>
            </th>

            <th style={{ width: '15%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('MailDate')}
                    className={getClassNamesFor('MailDate')}
                >
                   Approx. Mail Date
                 </button>
            </th>
        </>
    );
}

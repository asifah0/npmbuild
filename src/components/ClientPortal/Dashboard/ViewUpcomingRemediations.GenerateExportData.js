﻿import { numberWithCommas, getFormattedDate } from '../../Shared/Util/CommonFunctions';

export const getExportData = (results) => {
    let dataTable = [];
    if (results.length > 0) {
        if (results) {
            for (let i in results) {
                let obj = {
                    'Remediation #': results[i].remedNumber,
                    'Remediation Name': results[i].remedName,
                    'Approx # Payments': numberWithCommas(results[i].noOfPayments),
                    'Approx Total Payment Amount': '$' + numberWithCommas(results[i].paymentAmount)+'.00',
                    'Approx. # Check Stubs': numberWithCommas(results[i].noOfCheckStubs),
                    'Approx. File Receipt Date': getFormattedDate(results[i].receivedDt),
                    'Approx. Mail Date': getFormattedDate(results[i].checkMailDt),
                }
                dataTable.push(obj);
            }
        }
    }
    return dataTable;
}


﻿import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { tz } from 'moment-timezone';

import { AllRemediations } from './AllRemediations';
import { CheckStatusOverview } from './CheckStatusOverview';
import { UpcomingRemediations } from './UpcomingRemediations';
import { ActiveRemediations } from '../../Shared/ActiveRemediations';
import { Alerts } from './Alerts/Alerts';

export class ClientDashboard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            dashboardDateTime: ''
        }
    }
    componentDidMount() {
        let today = new Date();
        let amOrPM = 'AM';
        let hours = today.getHours();
        let year = today.getFullYear();
        year = year.toString().substr(2, 4);
        if (hours > 12) {
            hours = hours - 12;
            amOrPM = 'PM';
        }
        hours = hours.toString().padStart(2, '0');
        let month = today.getMonth() + 1;
        month = month.toString().padStart(2, '0');
        let day = today.getDate().toString().padStart(2, '0');
        let minutes = today.getMinutes().toString().padStart(2, '0');
        const guess = tz.guess(true);
        const zone = tz.zone(guess);
        const timezoneName = zone.abbr(new Date().getTime());
        let date = month + '/' + day + '/' + year + ' ' + hours + ':' + minutes + ' ' + amOrPM + ' ' + timezoneName;

        this.setState({
            dashboardDateTime: date
        });
    }
    
    render() {
        
        //let now = new Date();
        //let currentDateUTC = now.toUTCString();

        //let currentDateLocal = new Date(currentDateUTC);

        //let offset = new Date().getTimezoneOffset();// getting offset to make time in gmt+0 zone (UTC) (for gmt+5 offset comes as -300 minutes)
        //let date = new Date();
        //date.setMinutes(date.getMinutes() + offset);// date now in UTC time

        //let easternTimeOffset = -240; //for dayLight saving, Eastern time become 4 hours behind UTC thats why its offset is -4x60 = -240 minutes. So when Day light is not active the offset will be -300
        //date.setMinutes(date.getMinutes() + easternTimeOffset);

        return (
            <Container fluid className="content-section">

                <h1 className="h2-title bold">Dashboard <span className="fs-1rem">as of {this.state.dashboardDateTime}</span></h1>

                <div className="row mt-2 mb-3">

                    <div className="col-lg-3">
                        <div className="box box-border box-height">
                            <AllRemediations/>
                        </div>
                    </div>


                        <div className="col-lg-3">
                            <div className="box box-border box-height">
                                <CheckStatusOverview/>
                            </div>
                        </div>



                            <div className="col-lg-6">
                                <div className="box box-border box-height">
                                    <UpcomingRemediations/>
                                </div>
                            </div>

                </div>


                <div className="row mt-2 mb-3">

                    <div className="col-lg-8">
                        <div className="box box-border dsb-alert-hght"> 
                            <ActiveRemediations source = '0'/>
                        </div>
                    </div>


                    <div className="col-lg-4">
                        <div className="box box-border dsb-alert-hght">
                            <Alerts/>
                        </div>
                    </div>

                </div>
                </Container>


        );
    }
}

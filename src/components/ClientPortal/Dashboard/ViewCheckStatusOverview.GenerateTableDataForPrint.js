﻿import React from 'react';

import { numberWithCommas, numberWithCommasAmount } from '../../Shared/Util/CommonFunctions';

export const generateTableDataForPrint = (items) => {
  
    return items.map((item, index) => {        
        return (
            <tr key={item.remedSK}>
                <td>{item.remedNo}</td>
                <td>{item.remedName}</td>
                <td colSpan="2">
                    <table class="row amt-head table-borderless" >
                        <tr> <td class="col-sm-6 text-left" style={{ width: '12%' }}>
                            {numberWithCommas(item.totalCheckCount)}
                            </td>

                            <td class="col-sm-6 text-left" style={{ width: '15%' }}>
                                {numberWithCommasAmount(item.totalDollarAmount)}
                            </td>
                        </tr>
                    </table>
                </td>

                <td colSpan="2">
                    <table class="row amt-head table-borderless" >
                        <tr> <td class="col-sm-6 text-left" style={{ width: '12%' }}>
                            {numberWithCommas(item.cashedCheckCount)}
                        </td>

                            <td class="col-sm-6 text-left" style={{ width: '15%' }}>
                                {numberWithCommasAmount(item.cashedDollarAmount)}
                            </td>
                        </tr>
                    </table>
                </td>

                <td colSpan="2">
                    <table class="row amt-head table-borderless" >
                        <tr> <td class="col-sm-6 text-left" style={{ width: '12%' }}>
                            {numberWithCommas(item.unCashedCheckCount)}
                        </td>

                            <td class="col-sm-6 text-left" style={{ width: '15%' }}>
                                {numberWithCommasAmount(item.unCashedDollarAmount)}
                            </td>
                        </tr>
                    </table>
                </td>


                <td colSpan="2">
                    <table class="row amt-head table-borderless" >
                        <tr> <td class="col-sm-6 text-left" style={{ width: '12%' }}>
                            {numberWithCommas(item.undeliverableCheckCount)}
                        </td>

                            <td class="col-sm-6 text-left" style={{ width: '15%' }}>
                                {numberWithCommasAmount(item.undeliverableDollarAmount)}
                            </td>
                        </tr>
                    </table>
                </td>

                <td colSpan="2">
                    <table class="row amt-head table-borderless" >
                        <tr> <td class="col-sm-6 text-left" style={{ width: '12%' }}>
                            {numberWithCommas(item.voidedCheckCount)}
                        </td>

                            <td class="col-sm-6 text-left" style={{ width: '15%' }}>
                                {numberWithCommasAmount(item.voidedDollarAmount)}
                            </td>
                        </tr>
                    </table>
                </td>
                </tr>
        )
    })
}
﻿import React, { useState } from 'react';
import * as ReactBootStrap from 'react-bootstrap';

import { ApexChart } from '../../Shared/Chart'
import { useFetch } from '../../Shared/Services/GetServiceCall';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';
import ViewCheckStatusOverview from './ViewCheckStatusOverview'

export const CheckStatusOverview = () => {

    const [isViewCheckStatusOverviewOpen, setisViewCheckStatusOverviewOpen] = useState(false);
    const urlsuffix = `RemediationDashboard/GetCheckStatusOverviewCount`;
    const url = getClientServiceURL(urlsuffix);
    const { status, results } = useFetch(url);
    const loading = (status === 'fetching');
    const isError = (status === 'error');

    const handleClose = () => setisViewCheckStatusOverviewOpen(false);

    const viewCheckStatusOverviewClick = (e) => {
        setisViewCheckStatusOverviewOpen(true);
    }
    
    const checkStatusData = {
        label: 'Remediations',
        colors: ['#E96A24', '#7B61FF', '#3FBDE0', '#005D98'],
        customScale: 1.75,
        fontSizeName: "14px",
        fontSizeValue: "18px",
        offsetYName: +20,
        offsetYValue: -20,
        legendoffsetY: 30,
        legendoffsetX: 65,
        donutOffsetX: 20,
        donutOffsetY: 55
    };
    return (
        <>               
            <h1 className="h1-title mb-3"> Check Status Overview</h1>

            <div style={{ height: '254px' }}>
                {loading && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
                {isError ? <div class="label-error-red ml-3"> <span>Unable to display the data due to internal server error. Please try after sometime.</span></div>
                    :

                    <ApexChart remedData={checkStatusData}

                        cashedCount={(results.filter((rem) => rem.checkStatus === 'Cashed')).length > 0 ? (results.filter((rem) => rem.checkStatus === 'Cashed'))[0].checkCount : 0}
                        uncashedCount={(results.filter((rem) => rem.checkStatus === 'Uncashed')).length > 0 ? (results.filter((rem) => rem.checkStatus === 'Uncashed'))[0].checkCount : 0}
                        undeliverableCount={(results.filter((rem) => rem.checkStatus === 'Undeliverable')).length > 0 ? (results.filter((rem) => rem.checkStatus === 'Undeliverable'))[0].checkCount : 0}
                        voidedCount={(results.filter((rem) => rem.checkStatus === 'Voided')).length > 0 ? (results.filter((rem) => rem.checkStatus === 'Voided'))[0].checkCount : 0}
                        remedCount={results.length > 0 ? results[0].remedCount : 0}
                        fromScreen='CheckStatusOverview'
                    />
                }
            </div>

            <div className="text-right strong mt-50"> <button class="btn btn-link chart-links" onClick={viewCheckStatusOverviewClick}>View All</button></div>
            <ReactBootStrap.Modal
                show={isViewCheckStatusOverviewOpen}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                dialogClassName="my-modal-fullscreen"
            >
                <ReactBootStrap.Modal.Header closeButton>
                    <ReactBootStrap.Modal.Title>Check Status Overview</ReactBootStrap.Modal.Title>
                </ReactBootStrap.Modal.Header>
                <ReactBootStrap.Modal.Body>
                    <ViewCheckStatusOverview remedCount={results.length > 0 ? results[0].remedCount : 0}/>
                </ReactBootStrap.Modal.Body>
            </ReactBootStrap.Modal>
        </>

    );
}


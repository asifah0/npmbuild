﻿import React from 'react';

import { numberWithCommas, getFormattedDate } from '../../Shared/Util/CommonFunctions';

export const generateTableData = (items) => {

    return items.map((item, i) => {
        return (
            <tr key={item.remedSK}>
                <td>{item.remedNumber}</td>
                <td>{item.remedName}</td>
                <td>{numberWithCommas(item.noOfPayments)}</td>
                <td>${numberWithCommas(item.paymentAmount)}.00</td>
                <td>{numberWithCommas(item.noOfCheckStubs)}</td>
                <td>{getFormattedDate(item.receivedDt)}</td>
                <td>{getFormattedDate(item.checkMailDt)}</td>
            </tr>
        )
    })
}
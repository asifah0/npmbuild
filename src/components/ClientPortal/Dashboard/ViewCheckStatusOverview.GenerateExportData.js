﻿import React from 'react';

import { numberWithCommas } from '../../Shared/Util/CommonFunctions';

export const generateTableExport = (header,data) => {

    return (
        <table>
            <thead>
                <tr>
                    {header}
                </tr>
            </thead>
            <tbody>
                {data}
            </tbody>
        </table>
    );
}

export const getExportData = (results) => {
    let dataTable = [];

    if (results.length > 0) {
        if (results) {
            for (let i in results) {
                let obj = {
                    RemedNo: results[i].RemedNo,
                    RemedName: results[i].RemedName,
                    TotalCheckCount: numberWithCommas(results[i].TotalCheckCount),
                    TotalDollarAmount: numberWithCommas(results[i].TotalDollarAmount),
                    CashedCheckCount: numberWithCommas(results[i].CashedCheckCount),
                    CashedDollarAmount: numberWithCommas(results[i].CashedDollarAmount),
                    UncashedCheckCount: numberWithCommas(results[i].UnCashedCheckCount),
                    UncashedDollarAmount: numberWithCommas(results[i].UnCashedDollarAmount),
                    UndeliverableCheckCount: numberWithCommas(results[i].UndeliverableCheckCount),
                    UndeliverableDollarAmount: numberWithCommas(results[i].UndeliverableDollarAmount),
                    VoidedCheckCount: numberWithCommas(results[i].VoidedCheckCount),
                    VoidedDollarAmount: numberWithCommas( results[i].VoidedDollarAmount),
                }
                dataTable.push(obj);
            }
        }
    }

    return dataTable;
}

export const fields = ['RemedNo', 'RemedName', 'TotalCheckCount', 'TotalDollarAmount', 'CashedCheckCount',
    'CashedDollarAmount', 'UncashedCheckCount', 'UncashedDollarAmount', 'UndeliverableCheckCount', 'UndeliverableDollarAmount', 'VoidedCheckCount', 'VoidedDollarAmount'];

export const titles = {
    RemedNo: 'Remediation #',
    RemedName: 'Remediation Name',
    TotalCheckCount: 'Total Check Count',
    TotalDollarAmount: 'Total Dollar Amount',
    CashedCheckCount: 'Cashed Check Count',
    CashedDollarAmount: 'Cashed Dollar Amount',
    UnCashedCheckCount: 'UnCashed Check Count',
    UnCashedDollarAmount: 'UnCashed Dollar Amount',
    UndeliverableCheckCount: 'Undeliverable Check Count',
    UndeliverableDollarAmount: 'Undeliverable Dollar Amount',
    VoidedCheckCount: 'Voided Check Count',
    VoidedDollarAmount: 'Voided Dollar Amount'
}




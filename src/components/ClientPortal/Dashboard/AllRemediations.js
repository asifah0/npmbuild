﻿import React, { useState } from 'react';
import * as ReactBootStrap from 'react-bootstrap';

import { ApexChart } from '../../Shared/Chart';
import { ViewAllRemediations } from '../Remediations/ViewAllRemediations';
import { useFetch } from '../../Shared/Services/GetServiceCall';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';


export const AllRemediations = () => {
   
    const [isViewRemediationOpen, setisViewRemediationOpen] = useState(false);
    const urlsuffix = `RemediationDashboard/GetAllRemediationsCount`;
    const url = getClientServiceURL(urlsuffix);
    const { status, results } = useFetch(url);
    const loading = (status === 'fetching');
    const isError = (status === 'error');

    const handleClose = () => setisViewRemediationOpen(false);

    const viewRemediationOnClick = (e) => {
        setisViewRemediationOpen(true);
    }  
   
    const remedData = {
        label: 'Remediations',
//        labels: ['In-Progress', 'Mailed', 'Closed'],
        colors: ['#003657', '#007BB4', '#6B9FBD'],
        customScale: 1.25,
        fontSizeName: "19px",
        fontSizeValue: "25px",
        offsetYName: +25,
        offsetYValue: -20,
        legendoffsetY: 40,
        legendoffsetX: 95,
        donutOffsetX: 5,
        donutOffsetY: 40
    };

    return (

            <>
                <h1 className="h1-title mb-3">All Remediations</h1>

            <div style = {{height: '230px'}}>
            {loading && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
            {isError ? <div class="label-error-red ml-3"> <span>Unable to display the data due to internal server error. Please try after sometime.</span></div>
                :
                <ApexChart remedData={remedData}
                        inProgressCount={(results.filter((rem) => rem.remedStatus === 'In Process')).length > 0 ? (results.filter((rem) => rem.remedStatus === 'In Process'))[0].remedStatusCount : 0}
                    mailedCount={(results.filter((rem) => rem.remedStatus === 'Mailed')).length > 0 ? (results.filter((rem) => rem.remedStatus === 'Mailed'))[0].remedStatusCount : 0}
                    closedCount={(results.filter((rem) => rem.remedStatus === 'Closed')).length > 0 ? (results.filter((rem) => rem.remedStatus === 'Closed'))[0].remedStatusCount : 0}
                    fromScreen='AllRemediations'
                />
                }
            </div>
                
            <div className="text-right strong mt-4">  <button class="btn btn-link chart-links" data-toggle="modal" data-target="#More-Details" onClick={viewRemediationOnClick}>View All</button></div>
                <ReactBootStrap.Modal
                    show={isViewRemediationOpen}
                    onHide={handleClose}
                    backdrop="static"
                    keyboard={false}
                    dialogClassName="my-modal-fullscreen"
                >
                    <ReactBootStrap.Modal.Header closeButton>
                        <ReactBootStrap.Modal.Title>All Remediations</ReactBootStrap.Modal.Title>
                    </ReactBootStrap.Modal.Header>
                    <ReactBootStrap.Modal.Body>
                        <ViewAllRemediations fromComp = 'All' />
                    </ReactBootStrap.Modal.Body>
                </ReactBootStrap.Modal>
            </>

        );
}


﻿
export const getExportData = (results) => {
    let dataTable = [];
    if (results.length > 0) {
        if (results) {
            for (let i in results) {
                let obj = {
                    'File Type': results[i].documentType,
                    'Remediation #': results[i].remedNo,
                    'Remediation Name': results[i].remedName,
                    '# Payments': results[i].noOfPayments,
                    'Total Payment Amt.': results[i].paymentAmount,
                    '# Check Stubs': results[i].noOfCheckStubs,
                    'File Upload Date': results[i].fileUpladDate,
                    'File Name': results[i].fileName,
                    'Upload Status': results[i].uploadStatus,
                    'Uploa User': results[i].uploadUser,
                }
                dataTable.push(obj);
            }
        }
    }
    return dataTable;
}


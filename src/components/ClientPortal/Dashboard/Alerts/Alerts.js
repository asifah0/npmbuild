﻿import React, { useState, useEffect } from 'react';
import axios from 'axios';

import { Notifications } from './Notifications';
import { Uploads } from './Uploads';
import { getClientServiceURL } from '../../../Shared/Util/GlobalUtils';

import '../../../../css/css/Alerts.Styles.css'
//css / css / Alerts.Styles.css';

export const Alerts = () => {

    
    const [isNotificationOpen, setIsNotificationOpen] = useState(true);
    const [isUploadsOpen, setIsUploadsOpen] = useState(false);
    const [noOfUploads, setNoOfUploads] = useState(0);
    const [noOfNotifications, setNoOfNotifications] = useState(0);
    const [notificationResults, setNotificationResults] = useState([]);
    const [uploadResults, setUploadResults] = useState([]);
    const [isServerError, setIsServerError] = useState(false);
    const [loaded, setLoaded] = useState(false);
    const [timeZone, setTimeZone] = useState('');

    useEffect(() => {
        const urlsuffix = `RemediationDashboard/GetNotificationDetails`;
        const url = getClientServiceURL(urlsuffix);
        fetchNotificationDetails(url);
        fetchTimeZone();
    }, []);

    function fetchTimeZone() {
        const urlsuffix = `RemediationDashboard/GetTimeZone`;
        const url = getClientServiceURL(urlsuffix);
        let strTimeZone = '';
        let abTimeZone = '';

        setTimeZone('EST');

        //axios
        //    .get(url)
        //    .then((response) => {
        //        console.log(response.data);
        //        strTimeZone = response.data;
        //        if (strTimeZone.search(/\W/) >= 0) {

        //            console.warn("Extracted timezone as:", strTimeZone);

        //            abTimeZone = strTimeZone
        //                .match(/\b\w/g) // Match first letter at each word boundary.
        //                .join("")
        //                .toUpperCase()
        //                ;

        //        }

        //        setTimeZone(abTimeZone);
        //    })
        //    .catch((error) => {
               
        //    });
    }

    function fetchNotificationDetails(url) {

        setIsServerError(false);
        let results = [];

        axios
            .get(url)
            .then((response) => {
                for (var i = 0; i < response.data.length; i++) {
                    results.push(response.data[i])
                }
                setLoaded(true);
                if (response.data.length > 0) {
                    setNotificationResults(results.filter(item => item.notificationType === 'Notification'));
                    setUploadResults(results.filter(item => item.notificationType === 'Upload'));
                    setNoOfNotifications(results.filter(item => item.notificationType === 'Notification').length);
                    setNoOfUploads(results.filter(item => item.notificationType === 'Upload').length);
                }
            })
            .catch((error) => {
                setLoaded(true);
                setIsServerError(true);
            });        
    }


    const notificationsClick = () => {
        setIsNotificationOpen(true);
        setIsUploadsOpen(false);
    }

    const uploadsClick = () => {
        setIsNotificationOpen(false);
        setIsUploadsOpen(true);
    }

    let isNotificationClass = isNotificationOpen ? "buttonClicked" : "buttonNotClicked";
    let isUploadsClass = isUploadsOpen ? "buttonClicked" : "buttonNotClicked";

    return (       

    <>
            <div><h1 class="h1-title">Alerts</h1></div>

            <div class="alert-mesg">
                <ul class="nav nav-pills float-right pb-15" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <button class={isNotificationClass} onClick={notificationsClick}> Notifications<span class="Button-badge-dsb">{noOfNotifications}</span></button>
                    </li>
                    <li class="nav-item">
                        <button class={isUploadsClass} onClick={uploadsClick}> Uploads<span class="Button-badge-dsb">{noOfUploads}</span></button>
                    </li>
                </ul>

            <div class="tab-content clearfix" id="pills-tabContent" style={{clear:"both"}}>
                <div class="tab-pane fade show active" id="mesgs" role="tabpanel" aria-labelledby="pills-home-tab">

                        {isNotificationOpen ? <Notifications notificationResults={notificationResults} /> : <Uploads uploadResults={uploadResults} timeZone={timeZone} />}

                </div>
            </div>

        </div>

    </>
    );

}
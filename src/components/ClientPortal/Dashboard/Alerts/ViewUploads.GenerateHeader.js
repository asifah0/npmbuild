﻿import React from 'react';


export const generateHeader = (requestSort, getClassNamesFor) => {
    return (
        <>
            <th class="text-left" style={{ width:'10%'}}>
                <button style={{ textAlign: 'left', paddingLeft:'0' }}
                    type="button"
                    onClick={() => requestSort('DocumentType')}
                    className={getClassNamesFor('DocumentType')}
                >
                    File Type
            </button>
            </th>

            <th class="text-left" style={{ width: '8%' }}>
                <button style={{ textAlign: 'left', paddingLeft:'0' }}
                    type="button"
                    onClick={() => requestSort('RemedNo')}
                    className={getClassNamesFor('RemedNo')}
                >
                    Remed. #
            </button>
            </th>

            <th class="text-left" style={{ width: '10%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('RemedName')}
                    className={getClassNamesFor('RemedName')}
                >
                    Remed. Name
                    </button>
            </th>

            <th class="text-left" style={{ width: '8%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('NoOfPayments')}
                    className={getClassNamesFor('NoOfPayments')}
                >
                    # Pymts
                    </button>
            </th>


            <th class="text-left" style={{ width: '10%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('PaymentAmount')}
                    className={getClassNamesFor('PaymentAmount')}
                >
                    Total Pymt Amt.
                    </button>
            </th>


            <th class="text-left" style={{ width: '8%' }}>
                <button style={{ textAlign: 'right', paddingLeft:'0' }}
                    type="button"
                    onClick={() => requestSort('NoOfCheckStubs')}
                    className={getClassNamesFor('NoOfCheckStubs')}
                >
                    # Check Stubs
                    </button>
            </th>

            <th class="text-left" style={{ width: '10%' }}>
                <button style={{ textAlign: 'center', paddingLeft: '0', paddingRight:'0' }}
                    type="button"
                    onClick={() => requestSort('FileUploadDate')}
                    className={getClassNamesFor('FileUploadDate')}
                >
                    File Upload Date
                    </button>
            </th>

            <th class="text-left" style={{ width: '14%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('FileName')}
                    className={getClassNamesFor('FileName')}
                >
                    File Name
                 </button>
            </th>

            <th class="text-left" style={{ width: '8%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('UploadStatus')}
                    className={getClassNamesFor('UploadStatus')}
                >
                    Upload Status
                 </button>
            </th>

            <th class="text-left" style={{ width: '14%' }}>
                <button style={{ textAlign: 'left', paddingLeft: '0' }}
                    type="button"
                    onClick={() => requestSort('UploadUser')}
                    className={getClassNamesFor('UploadUser')}
                >
                    Upload User
                 </button>
            </th>
        </>
    );
}

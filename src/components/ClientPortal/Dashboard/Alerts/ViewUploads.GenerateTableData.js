﻿import React from 'react';

import { numberWithCommas, numberWithCommasAmount, getFormattedDate } from '../../../Shared/Util/CommonFunctions';

export const generateTableData = (items) => {
    return items.map((item, index) => {       
        return (            
            <tr key={item.remedSK}>
                <td class="text-left" style={{ width: '10%' }}>{item.documentType}</td>
                <td class="text-left" style={{ width: '8%' }}>{item.remedNo}</td>
                <td class="text-left" style={{ width: '10%' }}>{item.remedName}</td>
                <td class="text-left" style={{ width: '8%' }}>{numberWithCommas(item.noOfPayments)}</td>
                <td class="text-left" style={{ width: '10%' }}>{numberWithCommasAmount(item.paymentAmount)}</td>
                <td class="text-left" style={{ width: '8%' }}>{numberWithCommas(item.noOfCheckStubs)}</td>
                <td class="text-left" style={{ width: '10%' }}>{getFormattedDate(item.fileUploadDate)}</td>
                <td class="text-left" style={{ width: '14%' }}>{item.fileName}</td>
                <td class="text-left" style={{ width: '8%' }}>{item.uploadStatus}</td>
                <td class="text-left" style={{ width: '14%' }}>{item.uploadUser}</td>
            </tr>
        )
    })
}

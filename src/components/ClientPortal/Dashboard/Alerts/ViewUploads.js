﻿import React, { useEffect, useState, useRef } from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import axios from 'axios';
import { saveAs } from 'file-saver';

import { generateHeader } from './ViewUploads.GenerateHeader';
import { generateTableData } from './ViewUploads.GenerateTableData';
import { getExportData } from './ViewUploads.GenerateExportData';
import { getClientServiceURL } from '../../../Shared/Util/GlobalUtils';
import { numberWithCommas } from '../../../Shared/Util/CommonFunctions';
import { ExportCSV } from '../../../Shared/ExportToExcel';
import Print from '../../../Shared/Print';


export const ViewUploads = (props) => {

    const [resultsToShow, setResultsToShow] = useState([]);
    const [uploadsCount, setUploadsCount] = useState(0);
    const [pageSize, setPageSize] = useState(5);
    const [loaded, setLoaded] = useState(false);
    const [isError, setIsError] = useState(false);
    const [next, setNext] = useState(5);
    const [columnKey, setColumnKey] = useState('FileUploadDate');
    const [columnSortDirection, setColumnSortDirection] = useState('desc');

    const imageExcel = require('../../../../css/images/XL.svg');

    const handleShowMorePosts = () => {

        const urlsuffix = `RemediationDashboard/GetUploadsSummary?noOfRows=${next + pageSize}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const url = getClientServiceURL(urlsuffix);
        setNext(next => parseInt(next + pageSize));

        fetchUploadsSummary(url);
    };

    const fetchUploadsSummary = (url) => {

        setIsError(false);

        axios
            .get(url)
            .then((response) => {
                setResultsToShow(response.data.uploadsVMs);
                setUploadsCount(parseInt(response.data.totalUploads));
                setLoaded(true);
            })
            .catch((error) => {
                setLoaded(true);
                setIsError(true);
            });
    }



    React.useEffect(() => {

        const urlsuffix = `RemediationDashboard/GetUploadsSummary?noOfRows=${pageSize}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const url = getClientServiceURL(urlsuffix);
        fetchUploadsSummary(url);

    }, []);


    const getClassNamesFor = (name) => {
        if (columnKey === name) {
            if (columnSortDirection === 'asc') {
                return 'ascending';
            }
            else {
                return 'descending';
            }
        }
        else {
            return undefined;
        }
    };

    const requestSort = (key) => {

        let direction;

        if (columnKey === key && columnSortDirection === 'asc') {
            setColumnSortDirection('desc');
            direction = 'desc';

        }
        else {
            setColumnSortDirection('asc');
            direction = 'asc';
        }
        setColumnKey(key);
        setResultsToShow([]);
        setPageSize(5);
        const urlsuffix = `RemediationDashboard/GetUploadsSummary?noOfRows=${pageSize}&sortColumn=${key}&sortDirection=${direction}`;
        const url = getClientServiceURL(urlsuffix);
        setNext(5);

        fetchUploadsSummary(url);
    };

    const generateHeaderForPrint = () => {        
        return (
            <>                
                    <th class="col-sm-2 text-left">File Type</th>
                    <th class="col-sm-2 text-left">Remed. #</th>
                    <th class="col-sm-2 text-left">Remed. Name</th>
                    <th class="col-sm-2 text-left"># Pymts</th>
                    <th class="col-sm-2 text-left">Total Pymt Amount</th>
                    <th class="col-sm-2 text-left"># Check Stubs</th>
                    <th class="col-sm-2 text-left">File Upload Date</th>
                    <th class="col-sm-2 text-left">File Name</th>
                    <th class="col-sm-2 text-left">Upload Status</th>
                    <th class="col-sm-2 text-left">Upload User</th>                
            </>
        )
    }

    const exportToExcelClick = () => {
        const fileName = 'Uploads.xlsx';
        const urlSuffix = `RemediationDashboard/GetUploadsSummary/ExportResults?noOfRows=${0}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const Url = getClientServiceURL(urlSuffix);

        axios.get(Url,
            {
                responseType: 'arraybuffer'
            })
            .then((response) => {

                const blob = new Blob([response.data], {
                    type: 'application/octet-stream'
                })
                saveAs(blob, fileName);
            })
            .catch((error) => {

            });
    }

    return (

        <>
            <div className="row mt-4 mb-3">
                <div className="col-lg-3">
                    <div className="box gray-bg p-4">
                        <p>Total Uploads</p>
                        <div className="dsb-numbers">{numberWithCommas(uploadsCount)}</div>
                    </div>
                </div>
            </div>

            <div className="row mt-5 mb-3">

                <div className="col-lg-12">
                    <div className="row">
                        <div className="col">


                            <div className="box box-border Upcomig-Remed">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <h1 className="h1-title"> Upload Details</h1>
                                    </div>


                                    <div className="col-lg-6 text-right">
                                        <span>
                                            <button className='btn btn-link' onClick={() => exportToExcelClick()}><img src={imageExcel} alt="ExportToExcel" /></button>
                                        </span>
                                        <span className="ml-2">
                                            <Print header={generateHeaderForPrint()} data={generateTableData(resultsToShow)} />
                                        </span>
                                    </div>
                                </div>

                                <div className="table-scroll" style={{ height: '300px' }}>
                                    <table id="uploads" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr>
                                                {generateHeader(requestSort, getClassNamesFor)}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {loaded ? generateTableData(resultsToShow) : <ReactBootStrap.Spinner animation="border" />}
                                        </tbody>
                                    </table>
                                    {isError && <div class="label-error-red ml-3"> <span>Unable to display the data due to internal server error. Please try after sometime.</span></div>}
                                </div>

                                <div className="row mt-30">
                                    <div className="col text-center">                                       
                                        {resultsToShow.length > 0 ?
                                            <p>Viewing results 1-{resultsToShow.length} of {numberWithCommas(uploadsCount)}</p>
                                            :
                                            undefined
                                        }

                                        {resultsToShow.length < uploadsCount ?
                                            <button type="button" class="btn btn-border" onClick={handleShowMorePosts}>Load More</button>
                                            :
                                            undefined
                                        }

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>


        );
}
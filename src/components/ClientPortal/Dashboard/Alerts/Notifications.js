﻿import React from 'react';
import { getFormattedDate } from '../../../Shared/Util/CommonFunctions';
import '../../../../css/css/Alerts.Styles.css'

export const Notifications = (props) => {

    return (        
        <div class="row uploads-mesg mt-3">
            <div class="uploads-mesg">
                <div class="col">

                    {!props.notificationResults.length > 0 ? <h2>No data is available to display</h2> :
                        props.notificationResults.map((item, index) =>
                        <div class="upl-section">
                                <p><span className="alert-message " >{item.messageTitle}</span> - <span class="i-txt">{getFormattedDate(item.receivedDate)}</span></p>
                            <p>{item.messageDescription}</p>
                        </div>
                    )}                  

                </div>
            </div>
        </div>
        );
}
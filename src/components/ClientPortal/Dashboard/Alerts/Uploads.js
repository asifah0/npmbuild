﻿import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { ViewUploads } from './ViewUploads'
import { getFormattedDateWithTime } from '../../../Shared/Util/CommonFunctions';

import '../../../../css/css/Alerts.Styles.css'

export const Uploads = (props) => {
    
    const [isUploadsOpen, setIsUploadsOpen] = useState(false);

    const uploadsOnClick = () => {
        setIsUploadsOpen(true);
    }

    const handleClose = () => {
        setIsUploadsOpen(false);
    }

    return (
        <>
        <div class="row uploads-mesg mt-3">
            <div class="uploads-mesg">
                <div class="col">

                        {!props.uploadResults.length > 0 ? <h2>No data is available to display</h2> : props.uploadResults.map((item, index) =>
                        <div class="upl-section">
                                <p><span className="alert-message " > { item.remedName} - Remed. # {item.remedNo}</span></p>
                                <p>{item.messageTitle} {getFormattedDateWithTime(item.receivedDate)} { props.timeZone}</p>
                                <p>{item.messageDescription }</p>
                        </div>
                    )}

                </div>
            </div>            
        </div>

        <div className="text-right strong mt-3">  <button class="btn btn-link" onClick={uploadsOnClick}>View Uploads</button></div>

            <Modal
                show={isUploadsOpen}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                dialogClassName="my-modal-fullscreen"
                id="tableModal"
            >
                <Modal.Header closeButton>
                    <Modal.Title>Uploads</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <ViewUploads/>
                </Modal.Body>
            </Modal>

        </>
        );
}
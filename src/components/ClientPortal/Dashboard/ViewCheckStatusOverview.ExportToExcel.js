﻿import React from 'react'
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

export const ExportCSV = ({ xlsxData, fileName }) => {

    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';
    const imageExcel = require('../../../css/images/XL.svg');

    const exportToCSV = (xlsxData, fileName) => {

        xlsxData.unshift({
            'RemedNo': '', 'RemedName': '', 'TotalCheckCount': '', 'TotalDollarAmount': '', 'CashedCheckCount': '', 'CashedDollarAmount': ''
            , 'UncashedCheckCount': '', 'UncashedDollarAmount': '', 'UndeliverableCheckCount': '', 'UndeliverableDollarAmount': '', 'VoidedCheckCount': '', 'VoidedDollarAmount': ''
        },
            {
                'RemedNo': '', 'RemedName': '', 'TotalCheckCount': '', 'TotalDollarAmount': '', 'CashedCheckCount': '', 'CashedDollarAmount': ''
                , 'UncashedCheckCount': '', 'UncashedDollarAmount': '', 'UndeliverableCheckCount': '', 'UndeliverableDollarAmount': '', 'VoidedCheckCount': '', 'VoidedDollarAmount': ''
            })

        const ws = XLSX.utils.json_to_sheet(xlsxData, { skipHeader: true });

        ws.A1 = {
            t: 's', v: 'Remediation #'}

        ws.B1 = { t: 's', v: 'Remediation Name'}

        ws.C1 = { t: 's', v: 'Total' }
        ws.C2 = { t: 's', v: 'Check Count'}
        ws.D2 = { t: 's', v: 'Dollar Amount' }

        ws.E1 = { t: 's', v: 'Cashed' }
        ws.E2 = { t: 's', v: 'Check Count' }
        ws.F2 = { t: 's', v: 'Dollar Amount' }

        ws.G1 = { t: 's', v: 'Uncashed' }
        ws.G2 = { t: 's', v: 'Check Count' }
        ws.H2 = { t: 's', v: 'Dollar Amount' }

        ws.I1 = { t: 's', v: 'Undeliverable' }
        ws.I2 = { t: 's', v: 'Check Count' }
        ws.J2 = { t: 's', v: 'Dollar Amount' }

        ws.K1 = { t: 's', v: 'Voided Checks' }
        ws.K2 = { t: 's', v: 'Check Count' }
        ws.L2 = { t: 's', v: 'Dollar Amount' }

        // s - start, e - end, r - row, c - col (0 based)
        const merge = [{ s: { r: 0, c: 0 }, e: { r: 1, c: 0 },  }, // RemedNo
            { s: { r: 0, c: 1 }, e: { r: 1, c: 1 } }, // RemedName
            { s: { r: 0, c: 2 }, e: { r: 0, c: 3 } },// Total
            { s: { r: 0, c: 4 }, e: { r: 0, c: 5 } },//Cashed
            { s: { r: 0, c: 6 }, e: { r: 0, c: 7 } },//Uncashed
            { s: { r: 0, c: 8 }, e: { r: 0, c: 9 } },//Undeliverable
            { s: { r: 0, c: 10 }, e: { r: 0, c: 11 } },//Voided Checks

        ];
                      
        ws['!merges'] = merge;

        ws["C2"].s = {     
            font: {bold: true},
        };

        //ws.A1 = {
        //    s: {
        //        alignment: { textRotation: 90 },
        //        font: { sz: 14, bold: true }
        //    }
        //}


        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };

        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
        const data = new Blob([excelBuffer], { type: fileType });
        FileSaver.saveAs(data, fileName + fileExtension);
    }

    return (
        <button className='btn btn-link' onClick={(e) => exportToCSV(xlsxData, fileName)}><img src={imageExcel} alt="ExportToExcel" /></button>
    )
}

﻿import React from 'react';
import '../../../css/css/ViewCheckStatusOverview.Styles.css';

export const generateHeader = (requestSort, getClassNamesFor) => {
    return (
        <>
            <th style={{width:'12%'}}>
                <button
                    type="button"
                    onClick={() => requestSort('RemedNo')}
                    className={getClassNamesFor('RemedNo')}
                >
                    Remed. #
                    </button>
            </th>

            <th style={{ width: '13%' }}>
                <button
                    type="button"
                    onClick={() => requestSort('RemedName')}
                    className={getClassNamesFor('RemedName')}
                >
                    Remed. Name
                    </button>
            </th>

            <th colspan="2" class="text-left" style={{ width: '15%' }}>
                <div class="row amt-head">
                    <div class="ttl-line">Total</div>
                    <div class="col-sm-6" style={{align: 'left'}}>
                        <button
                            type="button"
                            onClick={() => requestSort('TotalCheckCount')}
                            className={getClassNamesFor('TotalCheckCount')}
                        >
                            Check Count
                    </button>
                    </div>

                    <div class="col-sm-6" style={{ align: 'left' }}>
                        <button
                            type="button"
                            onClick={() => requestSort('TotalDollarAmount')}
                            className={getClassNamesFor('TotalDollarAmount')}
                        >
                            Dollar Amount
                    </button>
                    </div>
                </div>
            </th>


            <th colspan="2" class="text-left" style={{ width: '15%' }}>
                <div class="row amt-head">
                    <div class="ttl-line">Cashed</div>
                    <div class="col-sm-6">
                        <button
                            type="button"
                            onClick={() => requestSort('CashedCheckCount')}
                            className={getClassNamesFor('CashedCheckCount')}
                        >
                            Check Count
                    </button>
                    </div>

                    <div class="col-sm-6">
                        <button
                            type="button"
                            onClick={() => requestSort('CashedDollarAmount')}
                            className={getClassNamesFor('CashedDollarAmount')}
                        >
                            Dollar Amount
                    </button>
                    </div>
                </div>
            </th>

            <th colspan="2" class="text-left" style={{ width: '15%' }}>
                <div class="row amt-head">
                    <div class="ttl-line">Uncashed</div>
                    <div class="col-sm-6">
                        <button
                            type="button"
                            onClick={() => requestSort('UnCashedCheckCount')}
                            className={getClassNamesFor('UnCashedCheckCount')}
                        >
                            Check Count
                 </button>
                    </div>

                    <div class="col-sm-6">
                        <button
                            type="button"
                            onClick={() => requestSort('UnCashedDollarAmount')}
                            className={getClassNamesFor('UnCashedDollarAmount')}
                        >
                            Dollar Amount
                    </button>
                    </div>
                </div>
            </th>


            <th colspan="2" class="text-left" style={{ width: '15%' }}>
                <div class="row amt-head">
                    <div class="ttl-line">Undeliverable</div>
                    <div class="col-sm-6">
                        <button
                            type="button"
                            onClick={() => requestSort('UndeliverableCheckCount')}
                            className={getClassNamesFor('UndeliverableCheckCount')}
                        >
                            Check Count
                    </button>
                    </div>

                    <div class="col-sm-6">
                        <button
                            type="button"
                            onClick={() => requestSort('UndeliverableDollarAmount')}
                            className={getClassNamesFor('UndeliverableDollarAmount')}
                        >
                            Dollar Amount
                    </button>
                    </div>
                </div>
            </th>

            <th colspan="2" class="text-left" style={{ width: '15%' }}>
                <div class="row amt-head">
                    <div class="ttl-line">Voided</div>
                    <div class="col-sm-6">
                        <button
                            type="button"
                            onClick={() => requestSort('VoidedCheckCount')}
                            className={getClassNamesFor('VoidedCheckCount')}
                        >
                            Check Count
                    </button>
                    </div>

                    <div class="col-sm-6">
                        <button
                            type="button"
                            onClick={() => requestSort('VoidedDollarAmount')}
                            className={getClassNamesFor('VoidedDollarAmount')}
                        >
                            Dollar Amount
                    </button>
                    </div>
                </div>
            </th>
        </>
    );
}

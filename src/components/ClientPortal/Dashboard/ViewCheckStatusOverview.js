﻿import React, {useState, useRef, useEffect } from 'react';

import * as ReactBootStrap from 'react-bootstrap';
import axios from 'axios';
import { saveAs } from 'file-saver';

import { generateHeader } from './ViewCheckStatusOverview.GenerateHeader';
import { generateTableData } from './ViewCheckStatusOverview.GenerateTableData';
import { generateHeaderForPrint } from './ViewCheckStatusOverview.GenerateHeaderForPrint';
import { generateTableDataForPrint } from './ViewCheckStatusOverview.GenerateTableDataForPrint';
import Print from '../../Shared/Print';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';
import { numberWithCommas } from '../../Shared/Util/CommonFunctions';

const ViewCheckStatusOverview = (props) => {

    const [resultsToShow, setResultsToShow] = useState([]);
    const [pageSize, setPageSize] = useState(5);
    const [loaded, setLoaded] = useState(false);
    const [isError, setIsError] = useState(false);
    const [next, setNext] = useState(5);
    const [filename, setFilename] = useState('CheckStatusOverview');
    const [columnKey, setColumnKey] = useState('RemedNo');
    const [columnSortDirection, setColumnSortDirection] = useState('asc');

    const imageExcel = require('../../../css/images/XL.svg');

    const handleShowMorePosts = () => {

        const urlsuffix = `RemediationDashboard/GetCheckStatusOverviewSummary?noOfRows=${next + pageSize}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const url = getClientServiceURL(urlsuffix);
        setNext(next => parseInt(next + pageSize));

        fetchCheckStatusOverviewSummary(url);
    };

    const fetchCheckStatusOverviewSummary = (url) => {

        setIsError(false);

        axios
            .get(url)
            .then((response) => {
                setResultsToShow(response.data);
                setLoaded(true);
            })
            .catch((error) => {
                setLoaded(true);
                setIsError(true);
            });
    }



    React.useEffect(() => {

        const urlsuffix = `RemediationDashboard/GetCheckStatusOverviewSummary?noOfRows=${pageSize}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const url = getClientServiceURL(urlsuffix);
        fetchCheckStatusOverviewSummary(url);

    }, []);


    const getClassNamesFor = (name) => {
        if (columnKey === name) {
            if (columnSortDirection === 'asc') {
                return 'ascending';
            }
            else {
                return 'descending';
            }
        }
        else {
            return undefined;
        }
    };

    const requestSort = (key) => {

        let direction;

        if (columnKey === key && columnSortDirection === 'asc') {
            setColumnSortDirection('desc');
            direction = 'desc';

        }
        else {
            setColumnSortDirection('asc');
            direction = 'asc';
        }
        setColumnKey(key);
        setResultsToShow([]);
        const urlsuffix = `RemediationDashboard/GetCheckStatusOverviewSummary?noOfRows=${pageSize}&sortColumn=${key}&sortDirection=${direction}`;
        const url = getClientServiceURL(urlsuffix);
        setNext(5);

        fetchCheckStatusOverviewSummary(url);
    };

    const exportToExcelClick = () => {
        const fileName = 'CheckStatusOverview.xlsx';
        const urlSuffix = `RemediationDashboard/GetCheckStatusOverviewSummary/ExportResults?noOfRows=${0}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const Url = getClientServiceURL(urlSuffix);

        axios.get(Url,
            { 
                responseType: 'arraybuffer'
            })
            .then((response) => {

                const blob = new Blob([response.data], {
                    type: 'application/octet-stream'
                })
                saveAs(blob, fileName);
            })
            .catch((error) => {

            });
    }
    return (
        <>
            <div class="row mt-2 mb-3">
                <div class="col-lg-3">
                    <div class="box gray-bg p-4">
                        <p>Remediations</p>
                        <div class="dsb-numbers">{props.remedCount}</div>
                    </div>
                </div>
            </div>

            <div className="row mt-5 mb-3">              

                <div className="col-lg-12">
                    <div className="row">
                        <div className="col">


                            <div className="box box-border rem-check">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <h1 className="h1-title"> Remediation Details</h1>
                                    </div>


                                    <div className="col-lg-6 text-right">                                       
                                        <span>
                                            <button className='btn btn-link' onClick={() => exportToExcelClick()}><img src={imageExcel} alt="ExportToExcel" /></button>
                                        </span>
                                        <span className="ml-2">
                                            <Print header={generateHeaderForPrint()} data={generateTableDataForPrint(resultsToShow)} />
                                        </span>
                                    </div>
                                </div>

                                

                                <div className="table-scroll" style={{ height: '350px' }}>
                                    <table id="example2" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr>
                                                {generateHeader(requestSort, getClassNamesFor)}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {loaded ? generateTableData(resultsToShow) : <ReactBootStrap.Spinner animation="border" />}
                                        </tbody>
                                    </table>
                                    {isError && <div class="label-error-red ml-3"> <span>Unable to display the data due to internal server error. Please try after sometime.</span></div>}
                                </div>

                                <div className="table-scroll" style={{ height: '350px', display:'none' }}>
                                    <table id="remed-check-status-export" className="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                {generateHeaderForPrint()}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {generateTableData(resultsToShow)}
                                        </tbody>
                                    </table>                                                                       
                                </div>

                                <div className="row mt-30">
                                    <div className="col text-center">
                                      {resultsToShow.length > 0 ?
                                            <p>Viewing results 1-{resultsToShow.length} of {numberWithCommas(props.remedCount)}</p>
                                            :
                                            undefined
                                        }

                                        {resultsToShow.length < props.remedCount ?
                                            <button type="button" class="btn btn-border" onClick={handleShowMorePosts}>Load More</button>
                                            :
                                            undefined
                                        }
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>
        );
}

export default ViewCheckStatusOverview;
﻿import React, { useState } from 'react';
import * as ReactBootStrap from 'react-bootstrap';

import { RemediationDetails } from '../../Shared/RemediationDetails';
import ViewUpcomingRemediations from './ViewUpcomingRemediations';
import { numberWithCommas, getFormattedDate } from '../../Shared/Util/CommonFunctions';
import { useFetch } from '../../Shared/Services/GetServiceCall';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';

export const UpcomingRemediations = () => {
    
    const [isViewUpcomingRemedOpen, setisViewUpcomingRemedOpen] = useState(false);
    const [isRemedDetailsOpen, setIsRemedDetailsOpen] = useState(false);
    const [remedSK, setRemedSK] = useState(0);

    const urlsuffix = `RemediationDashboard/GetUpcomingRemediations?offset=${0}&limit=${0}&sort=${''}`;
    const url = getClientServiceURL(urlsuffix);
    const { status, results } = useFetch(url);
    const loading = (status === 'fetching');
    const isError = (status === 'error');

    const handleClose = () => setisViewUpcomingRemedOpen(false);

    const handleCloseRemedDetails = () => setIsRemedDetailsOpen(false);

    const viewUpcomingRemedClick = (e) => {
        setisViewUpcomingRemedOpen(true);
    }

    const generateHeader = () => {
        return (
            <>
                <th style={{ width: '22%' }}>Remed. #</th>
                <th style={{ width: '28%' }}>Remed. Name</th>
                <th style={{ width: '15%' }}>Approx. Pymts</th>
                <th style={{ width: '20%' }}>Approx. File Rcpt. Date</th>
                <th style={{ width: '15%' }}>Approx. Mail Date</th>
            </>
        )
    }

    const remedNoOnClick = (item) => {
        setIsRemedDetailsOpen(true);
        setRemedSK(item.remedSk);
    }

    const generateTableData = () => {
        return results.map((item, index) => {            
            return (
                <>                    
                    <tr key={item.remedSk}>
                        <td><button class="btn btn-link" onClick={() => remedNoOnClick(item)}>{item.remedNumber.trim()}</button>  </td>
                        <td>{item.remedName}</td>
                        <td>{numberWithCommas(item.noOfPayments)}</td>
                        <td>{getFormattedDate(item.receivedDt)}</td>
                        <td>{getFormattedDate(item.checkMailDt)}</td>
                    </tr>                   
                </>
            )
        })
    }

   

    return (
            <>
                <div className="row">
                    <div className="col-lg-8"><h1 className="h1-title">Upcoming Remediations</h1></div>
                <div className="col-lg-4 text-right ">
                    <span className="amt-orng">{numberWithCommas(results.reduce((a, v) => a = a + v.noOfPayments, 0)) }</span>
                            <div>Approx Pymts</div>
                    </div>
                </div>
                
                <div className="row">

                <div className="tbl-height" style={{height: '265px'}}>
                                      
                    <table id="upcmg-table" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">
                            <thead>
                                    {generateHeader()}
                            </thead>
                            <tbody>
                            {loading ? <ReactBootStrap.Spinner animation="border" /> : generateTableData() }
                            </tbody>
                    </table>                    
                    {isError && <div class="label-error-red ml-3"> <span>Unable to display the data due to internal server error. Please try after sometime.</span></div>}
                    </div>
                </div>

            <div className="text-right mt-4 mb-2 strong">  <button class="btn btn-link" onClick={viewUpcomingRemedClick}>More Details</button></div>

            <ReactBootStrap.Modal
                show={isViewUpcomingRemedOpen}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                dialogClassName="my-modal-fullscreen"
            >
                <ReactBootStrap.Modal.Header closeButton>
                    <ReactBootStrap.Modal.Title>Upcoming Remediations</ReactBootStrap.Modal.Title>
                </ReactBootStrap.Modal.Header>
                <ReactBootStrap.Modal.Body>
                    <ViewUpcomingRemediations/>
                </ReactBootStrap.Modal.Body>
            </ReactBootStrap.Modal>
                     

            <ReactBootStrap.Modal
                show={isRemedDetailsOpen}
                onHide={handleCloseRemedDetails}
                backdrop="static"
                keyboard={false}
                dialogClassName="my-modal-fullscreen"
                id="tableModal"
            >
                <ReactBootStrap.Modal.Header closeButton>
                    <ReactBootStrap.Modal.Title>Remediations Details</ReactBootStrap.Modal.Title>
                </ReactBootStrap.Modal.Header>
                <ReactBootStrap.Modal.Body>
                    <RemediationDetails remedSK={remedSK} />
                </ReactBootStrap.Modal.Body>
            </ReactBootStrap.Modal>


            </>

        );
    }

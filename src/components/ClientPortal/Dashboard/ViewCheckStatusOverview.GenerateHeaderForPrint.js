﻿import React from 'react';

export const generateHeaderForPrint = () => {
    return (
        <>
            <th style={{ width: '16%' }}>
                Remed. #                
            </th>

            <th style={{ width: '18%' }}>
                Remed. Name
            </th>

            <th colspan="2">
                <table class="row amt-head">
                    
                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Total</th></tr>
                    <tr> <th class="col-sm-6 strong" style={{ width: '12%'}}>
                            Check Count
                    </th>

                        <th class="col-sm-6 strong" style={{ width: '15%' }}>
                            Dollar Amount
                    </th>
                        </tr>
                </table>
            </th>
          


            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Cashed</th></tr>
                    <tr> <th class="col-sm-6 strong" style={{ width: '12%' }}>
                        Check Count
                    </th>

                        <th class="col-sm-6 strong" style={{ width: '15%' }}>
                            Dollar Amount
                    </th>
                    </tr>
                </table>
            </th>

            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Uncashed</th></tr>
                    <tr> <th class="col-sm-6 strong" style={{ width: '12%' }}>
                        Check Count
                    </th>

                        <th class="col-sm-6 strong" style={{ width: '15%' }}>
                            Dollar Amount
                    </th>
                    </tr>
                </table>
            </th>


            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Undeliverable</th></tr>
                    <tr> <th class="col-sm-6 strong" style={{ width: '12%' }}>
                        Check Count
                    </th>

                        <th class="col-sm-6 strong" style={{ width: '15%' }}>
                            Dollar Amount
                    </th>
                    </tr>
                </table>
            </th>

            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Voided</th></tr>
                    <tr> <th class="col-sm-6 strong" style={{ width: '12%' }}>
                        Check Count
                    </th>

                        <th class="col-sm-6 strong" style={{ width: '15%' }}>
                            Dollar Amount
                    </th>
                    </tr>
                </table>
            </th>
        </>
    );
}

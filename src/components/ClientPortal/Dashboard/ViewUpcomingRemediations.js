﻿import React, { useState } from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import axios from 'axios';
import { saveAs } from 'file-saver';

import { RemediationDetails } from '../../Shared/RemediationDetails';
import { generateHeader } from './ViewUpcomingRemediations.GenerateHeader';
import Print from '../../Shared/Print';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';
import { numberWithCommas, getFormattedDate, numberWithCommasAmount } from '../../Shared/Util/CommonFunctions';

const ViewUpcomingRemediations = () => {

    const [resultsToShow, setResultsToShow] = useState([]);
    const [paymentAmount, setPaymentAmount] = useState(0);
    const [noOfPayments, setNoOfPayments] = useState(0);
    const [remedCount, setRemedCount] = useState(0);
    const [pageSize, setPageSize] = useState(5);    
    const [loaded, setLoaded] = useState(false);
    const [isError, setIsError] = useState(false);
    const [next, setNext] = useState(5);
    const [columnKey, setColumnKey] = useState('MailDate');
    const [columnSortDirection, setColumnSortDirection] = useState('desc');
    const [isRemedDetailsOpen, setIsRemedDetailsOpen] = useState(false);
    const [remedSK, setRemedSK] = useState(0);

    const imageExcel = require('../../../css/images/XL.svg');

    const remedNoOnClick = (item) => {
        setIsRemedDetailsOpen(true);
        setRemedSK(item.remedSk);
    }

    const handleCloseRemedDetails = () => setIsRemedDetailsOpen(false);
    
    const handleShowMorePosts = () => {
        
        const urlsuffix = `RemediationDashboard/GetUpcomingRemediationsSummary?noOfRows=${next + pageSize}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const url = getClientServiceURL(urlsuffix);
        setNext(next => parseInt(next + pageSize));

        fetchUpcomingRemediationSummary(url);
    };

    const fetchUpcomingRemediationSummary = (url) => {

        setIsError(false);

        axios
            .get(url)
            .then((response) => {
                setResultsToShow(response.data.remediationsVMs);
                setPaymentAmount(response.data.remediationsSummaryCountVM.paymentAmount);
                setNoOfPayments(response.data.remediationsSummaryCountVM.noOfPayments);
                setRemedCount(response.data.remediationsSummaryCountVM.remedCount);
                setLoaded(true);
            })
            .catch((error) => {
                setLoaded(true);
                setIsError(true);
            });
    }



    React.useEffect(() => {

        const urlsuffix = `RemediationDashboard/GetUpcomingRemediationsSummary?noOfRows=${pageSize}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const url = getClientServiceURL(urlsuffix);
        fetchUpcomingRemediationSummary(url);

    }, []);

    
    const getClassNamesFor = (name) => {
        if (columnKey === name) {
            if (columnSortDirection === 'asc') {
                return 'ascending';
            }
            else {
                return 'descending';
            }
        }
        else {
            return undefined;
        }
    };

    const requestSort = (key) => {

        let direction;

        if (columnKey === key && columnSortDirection === 'asc') {
            setColumnSortDirection('desc');
            direction = 'desc';
            
        }
        else {
            setColumnSortDirection('asc');
            direction = 'asc';
        }
        setColumnKey(key);        
        setResultsToShow([]);
        setPageSize(5);
        const urlsuffix = `RemediationDashboard/GetUpcomingRemediationsSummary?noOfRows=${pageSize}&sortColumn=${key}&sortDirection=${direction}`;
        const url = getClientServiceURL(urlsuffix);
        setNext(5);

        fetchUpcomingRemediationSummary(url);
    };

    const generateHeaderForPrint = () => {
        return (
            <>
                <th class="text-left" style={{width: '10%'}}>Remed. #</th>
                <th class="text-left" style={{ width: '10%' }}>Remed. Name</th>
                <th class="text-centre" style={{ width: '10%' }}>Approx. # Pymts</th>
                <th class="text-centre" style={{ width: '10%' }}>Approx. Total Pymnt Amount</th>
                <th class="text-centre" style={{ width: '10%' }}>Approx. # Check Stubs</th>
                <th class="text-centre" style={{ width: '10%' }}>Approx. File Receipt Date</th>
                <th class="text-centre" style={{ width: '10%' }}>Approx. Mail Date</th>
            </>
        )
    }

    const generateTableDataForPrint = (items) => {
        return items.map((item, index) => {           
            return (
                <tr key={item.remedSK}>
                    <td class="text-left" style={{ width: '10%' }}>{item.remedNumber}</td>
                    <td class="text-left" style={{ width: '10%' }}>{item.remedName}</td>
                    <td class="text-left" style={{ width: '10%' }}>{numberWithCommas(item.noOfPayments)}</td>
                    <td class="text-left" style={{ width: '10%' }}>{numberWithCommasAmount(item.paymentAmount)}</td>
                    <td class="text-left" style={{ width: '15%' }}>{numberWithCommas(item.noOfCheckStubs)}</td>
                    <td class="text-left" style={{ width: '10%' }}>{getFormattedDate(item.receivedDt)}</td>
                    <td class="text-left" style={{ width: '10%' }}>{getFormattedDate(item.checkMailDt)}</td>
                </tr>
            )
        })
    }

    const generateTableData = (items) => {

        return items.map((item, i) => {
            return (
                <>
                <tr key={item.remedSK}>
                    <td><button class="btn btn-link" onClick={() => remedNoOnClick(item)}> {item.remedNumber} </button>  </td>
                    <td>{item.remedName}</td>
                    <td>{numberWithCommas(item.noOfPayments)}</td>
                    <td>{numberWithCommasAmount(item.paymentAmount)}</td>
                    <td>{numberWithCommas(item.noOfCheckStubs)}</td>
                    <td>{getFormattedDate(item.receivedDt)}</td>
                    <td>{getFormattedDate(item.checkMailDt)}</td>
                </tr>

                 <ReactBootStrap.Modal
                        show={isRemedDetailsOpen}
                        onHide={handleCloseRemedDetails}
                        backdrop="static"
                        keyboard={false}
                        dialogClassName="my-modal-fullscreen"
                        id="tableModal"
                    >
                        <ReactBootStrap.Modal.Header closeButton>
                            <ReactBootStrap.Modal.Title>Remediations Details</ReactBootStrap.Modal.Title>
                        </ReactBootStrap.Modal.Header>
                        <ReactBootStrap.Modal.Body>
                            <RemediationDetails remedSK={remedSK} />
                        </ReactBootStrap.Modal.Body>
                    </ReactBootStrap.Modal>
                </>
            )
        })
    }

    const exportToExcelClick = () => {
        const fileName = 'UpcomingRemediations.xlsx';
        const urlSuffix = `RemediationDashboard/GetUpcomingRemediationsSummary/ExportResults?noOfRows=${0}&sortColumn=${columnKey}&sortDirection=${columnSortDirection}`;
        const Url = getClientServiceURL(urlSuffix);

        axios.get(Url,
            {
                responseType: 'arraybuffer'
            })
            .then((response) => {

                const blob = new Blob([response.data], {
                    type: 'application/octet-stream'
                })
                saveAs(blob, fileName);
            })
            .catch((error) => {

            });
    }

        return (
            <>
                <div className="row mt-4 mb-3">
                    <div className="col-lg-3">
                        <div className="box gray-bg p-4">
                            <p>Total Payments</p>
                            <div className="dsb-numbers">{numberWithCommas(noOfPayments)}</div>
                        </div>
                    </div>
                </div>



                <div className="row mt-5 mb-3">

                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col">


                                <div className="box box-border Upcomig-Remed">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <h1 className="h1-title"> Remediation Details</h1>
                                        </div>


                                        <div className="col-lg-6 text-right">
                                            <span>
                                                <button className='btn btn-link' onClick={() => exportToExcelClick()}><img src={imageExcel} alt="ExportToExcel" /></button>
                                            </span>
                                            <span className="ml-2">
                                                <Print header={generateHeaderForPrint()} data={generateTableDataForPrint(resultsToShow)} />
                                            </span>
                                        </div>
                                    </div>

                                    <div className="table-scroll" style={{ height: '300px' }}>

                                        <table id="clientlist" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr>
                                                    {generateHeader(requestSort, getClassNamesFor)}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {loaded ? generateTableData(resultsToShow) : <ReactBootStrap.Spinner animation="border" />}
                                            </tbody>
                                        </table>
                                        {isError && <div class="label-error-red ml-3"> <span>Unable to display the data due to internal server error. Please try after sometime.</span></div>}
                                    </div>

                                    <div className="row mt-30">
                                        <div className="col text-center">
                                            {resultsToShow.length > 0 ?
                                                <p>Viewing results 1-{resultsToShow.length} of {numberWithCommas(remedCount)}</p>
                                                :
                                                undefined
                                            }

                                            {resultsToShow.length < remedCount ?
                                                <button type="button" class="btn btn-border" onClick={handleShowMorePosts}>Load More</button>
                                                :
                                                undefined
                                            }
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </>

        )

}

export default ViewUpcomingRemediations;

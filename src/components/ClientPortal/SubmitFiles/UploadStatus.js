﻿import React from 'react';

import { numberWithCommas, numberWithCommasAmount } from '../../Shared/Util/CommonFunctions';

export const UploadStatus = (props) => {

    const successImg = require('../../../css/images/sucess.svg');
    const defaultImg = require('../../../css/images/defaultUploadIcon.svg');
    const infoImg = require('../../../css/images/Info.png');
    const errorImg = require('../../../css/images/Error.png');

    return (

        <div class="row ">
            <div class="col ">
                
                <div class="box submitBox p-30">

                    {props.status === '' ?

                        <div class="text-center mt-5">
                            <p>Upload the file using the boxes on the left</p>
                            <img src={defaultImg} alt='default' />
                        </div>
                        :
                        props.status === 'Success' ?
                        <div class="text-center">
                                <p class="mt-5"><img src={successImg} alt='Success' /></p>

                                {props.fileTypeSelected === 'Consumer Data' || props.isRemedExists === false ?
                                    <>
                                        <p class="succs-text">Success! Your Consumer data file upload is in progress.</p>
                                        <p class="succs-mesg">You will be notified once the upload is done or for any validation errors.</p>
                                    </>
                                    :
                                    <>
                                        <p class="succs-text">Success! Your file has been uploaded.</p>
                                        <p class="succs-mesg">You may upload another file using the boxes on the left.</p>
                                    </>
                                    }

                                <div class="tbl-sucess">

                                    {props.fileTypeSelected === 'Consumer Data' || props.isRemedExists === false ?
                                        <table id="upload-consumer" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr>
                                            <th class="text-left">File Name</th>
                                            <th class="text-left">Remed. #</th>
                                            <th class="text-left"># of Pymts</th>
                                            <th class="text-left">Pymt Amount</th>
                                            <th class="text-left"># of Check Stubs</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left">{props.selectedFile.name}</td>
                                            <td class="text-left">{props.remedNo}</td>
                                            <td class="text-left">{numberWithCommas(props.noOfPayments)}</td>
                                            <td class="text-left">{numberWithCommasAmount(props.paymentAmt)}</td>
                                            <td class="text-left">{numberWithCommas(props.noOfCheckStubs)}</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        :
                                        <table id="upload-document" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr>
                                                    <th class="text-left" style={{ width: '10%' }}>File Name</th>
                                                    <th class="text-left" style={{ width: '10%' }}>Remediation #</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-left">{props.selectedFile.name} </td>
                                                    <td class="text-left">{props.remedNo}</td>
                                                </tr>
                                            </tbody>
                                        </table>
}
                            </div>

                            </div>
                            :

                            (props.msg === 'Consumer data upload is in progress') ?
                                <div class="text-center">
                                    <p class="mt-5">
                                        <img src={infoImg} alt='InProcess' />
                                      {/*  <i class="fas fa-exclamation-circle fa-5x"></i>*/}
                                    </p>
                                    <p class="failure-mesg" style={{ color: 'black', fontWeight: 'bold' }}>Info! {props.msg}</p>
                                </div>
                            :
                                <div class="text-center">
                                    <p class="mt-5">
                                        <img src={errorImg} alt='Error' />
{/*                                        <i class="fas fa-exclamation-circle fa-5x text-danger"></i>*/}
                                    </p>
                                    <p className="label-error-red">Error! While upload the files.</p>
                                    <p className="label-error-red">You may upload another file using the boxes on the left.</p>
                                    <p className="label-error-red">{props.msg}</p>
                                </div>
                    }
         </div>
         </div>
      </div>

    );
}
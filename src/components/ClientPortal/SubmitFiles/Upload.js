﻿import React, { Component } from 'react';
import CurrencyInput from 'react-currency-input-field';
import DatePicker from "react-datepicker";
import Dialog from 'react-bootstrap-dialog';
import * as ReactBootStrap from 'react-bootstrap';
import axios from 'axios';
import { saveAs } from 'file-saver';

import { showMsg, getAmountValue, getFormattedDateAssign, formatDate } from '../../Shared/Util/CommonFunctions';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';

const noOfValidColumns = 20;
//const noOfPayments = 3;

export class Upload extends Component {

    constructor(props) {
        super(props);
        this.state = {
            remedNoInput: '',
            isRemedExists: false,
            isConsumerDataInProcess: false,
            isConsumerDataUploaded: false,
            remedSK: 0,
            remedDetailsloaded: false,
            remedDetailsError: false,
            remedName: '',
            noOfPayments: 0,
            paymentAmt: 0,
            noOfCheckStubs: 0,
            mailDate: undefined,
            isRemedSelected: false,
            fileTypeSelected: '',
            enableConsumerUpload: false,
            enableDocumentUpload: false,
            selectedFile: [],
            uploadStatus: '',
            isError: false,
            loading: false,
            noOfFileRows: 0,
            noOfFileColumns: 0,
            s3Loaded: true,
            s3LoadedProgress: 0,
            s3Url: ''
        }
        this.remedNoOnChange = this.remedNoOnChange.bind(this);
        this.remedNoOnClick = this.remedNoOnClick.bind(this);
        //this.handleMailDateChange = this.handleMailDateChange.bind(this);
    }

    handlePaymentAmtChange = (e) => {
        let value = e.target.value;
        if (value.length > 15)
            return;

        if (value.includes('$')) {
            value = value.replace('$', '');
        }
        if (value.toLowerCase().includes('k') ||
            value.toLowerCase().includes('m') ||
            value.toLowerCase().includes('b') ||
            value.toLowerCase().includes(',')) {
            value =  getAmountValue(value);
        }

        if (isNaN(value)) value = '';
        this.setState({ paymentAmt: value });
    }

    handleNoOfChecksChange = (e) => {
        let value = e.target.value;

        if (value.length > 5)
            return;

            if (value.toLowerCase().includes('k') ||
                value.toLowerCase().includes('m') ||
                value.toLowerCase().includes('b') ||
                value.toLowerCase().includes(',')) {
                value = getAmountValue(value);
        }

        if (isNaN(value)) value = '';
        this.setState({ noOfCheckStubs: value });
    }

    handleNoOfPaymentsChange = (e) => {
        let value = e.target.value;

        if (value.length > 10)
            return;

        if (value.toLowerCase().includes('k') ||
            value.toLowerCase().includes('m') ||
            value.toLowerCase().includes('b') ||
            value.toLowerCase().includes(',')) {
            value = getAmountValue(value);
        }

        if (isNaN(value)) value = '';
        this.setState({
            noOfPayments: value,
            selectedFile: ''
        });
    }

    remedNoOnChange = (e) => {
        const value = e.target.value;
        //const regex = /^[a-z\d\-_\s]+$/i;
        const regex = /^[a-z\d\s]+$/i;
        if (value.match(regex) || value === "") {
            this.setState({ remedNoInput: e.target.value });
        }
    }

    fetchRemedStatus = () => {
        this.setState({
            isError: false
        });

        let resultRemedStatusSK = '';
        let resultRemed = [];

        const urlSuffix = `SubmitFiles/GetRemedStatusByRemedSK?RemedNo=${this.state.remedNoInput.trim()}`;
        const Url = getClientServiceURL(urlSuffix);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {

                console.log(res.data);

                resultRemedStatusSK = res.data;

                if (resultRemedStatusSK === 'RemedNo Not Found') {
                    this.setState({
                        isRemedExists: false,
                        isConsumerDataInProcess: false,
                        isConsumerDataUploaded: false,
                        isRemedSelected: true,
                        enableConsumerUpload: true,
                        enableDocumentUpload: false,
                        remedSK: 0
                    })
                }
                else {
                    resultRemed = resultRemedStatusSK.split('|');

                    if (resultRemed[0].toUpperCase() === 'UPCOMING') {

                        if (resultRemed[1].toUpperCase() === 'INPROCESS' || resultRemed[1].toUpperCase() === 'TOBEPROCESSED')
                            this.setState({ isConsumerDataInProcess: true });
                        else
                            this.setState({ isConsumerDataInProcess: false });

                        this.setState({
                            isRemedExists: true,
                            isConsumerDataUploaded: false,
                            isRemedSelected: true,
                            enableConsumerUpload: true,
                            enableDocumentUpload: false,
                            remedSK: parseInt(resultRemed[2])
                        })

                    }
                    else {

                        this.setState({
                            isRemedExists: true,
                            isConsumerDataInProcess: false,
                            isConsumerDataUploaded: true,
                            isRemedSelected: true,
                            enableConsumerUpload: false,
                            enableDocumentUpload: true,
                            remedSK: 0
                        })
                    }

                }
            })
            .catch((error) => {
                if (axios.isCancel(error) || error) {

                }
                this.setState({
                    loading: true,
                    isError: true
                });
            });
    }

    remedNoOnClick = () => {
        
        if (this.state.remedNoInput.trim() !== '') {
            this.fetchRemedStatus();
        }
        else {
            showMsg(this.dialog, 'Submit Files', 'Please enter Remediation Number.');
            this.setState({ remedNoInput: '' });
        }
        
    }

    fetchRemedDetails = () => {
        this.setState({
            isError: false,
            enableConsumerUpload: false
        });

        let resultRemedDetails = [];

        const urlSuffix = `RemediationDashboard/GetRemediationDetailsByRemedSK?RemedSK=${this.state.remedSK}`;
        const Url = getClientServiceURL(urlSuffix);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                resultRemedDetails = res.data.remediationDetails;
                if (resultRemedDetails.length > 0) {
                    this.setState({
                        remedName: resultRemedDetails[0].remedName.trim(),
                        noOfPayments: resultRemedDetails[0].noOfPayments,
                        paymentAmt: resultRemedDetails[0].paymentAmount,
                        noOfCheckStubs: resultRemedDetails[0].noOfCheckStubs,
                        mailDate: getFormattedDateAssign(resultRemedDetails[0].checkMailDt),
                        remedDetailsloaded: true,
                        enableConsumerUpload: true
                    })                  
                }
            })
            .catch((error) => {
                if (axios.isCancel(error) || error) {

                }
                this.setState({
                    remedDetailsloaded: true,
                    remedDetailsError: true,
                    enableConsumerUpload: false
                });
            });
    }

    handleFileTypeChange = (e) => {

        this.setState({                     
            fileTypeSelected: '',
            selectedFile: [],
            isFilePicked: false,
            errorMsg: ''
        })

        this.props.uploadCallBack('');       

        if (e.target.value === 'Consumer Data') {
            this.fetchRemedDetails();
            if (this.state.remedDetailsError)
                this.setState({
                    fileTypeSelected: '',
                })
            else {
                this.setState({
                    fileTypeSelected: e.target.value,
                })

                if (this.state.isConsumerDataInProcess) {
                    this.props.uploadCallBack('Failed', 'Consumer data upload is in progress', this.state.remedNoInput, this.state.remedName,
                        this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                        this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);

                    this.setState({
                        uploadStatus: 'Failed',
                        s3Loaded: true,
                        isFilePicked: false,
                        selectedFile: '',
                    });
                }

            }
        }
        else {
            this.setState({
                fileTypeSelected: e.target.value,                
            })
        }
    }

    handleremedNameChange = (e) => {
        const value = e.target.value;
        //const regex = /^[a-z\d\-_\s]+$/i;
        const regex = /^[a-z\d\s]+$/i;
        if (value.match(regex) || value === "") {
            this.setState({ remedName: value });
        }
    }

    handleMailDateChange = (date) => {
        if (date !== null) 
            this.setState({ mailDate: date });
        else 
            this.setState({ mailDate: undefined });                
    }

    handlesKeyPress = (e, allowDecimals) => {
       // let re = /[0-9kmbKMB.]+/g;
        let re = /[0-9.]+/g;
        if (!allowDecimals) {
            re = /[0-9]+/g;
        }
        if (!re.test(e.key)) {
            e.preventDefault();
        }
    }

    handleCancelOnclick = (e) => {

        e.preventDefault();

        this.setState({
            remedNoInput: '',
            isRemedExists: false,
            isConsumerDataInProcess: false,
            isConsumerDataUploaded: false,
            remedName: '',
            noOfPayments: 0,
            paymentAmt: 0,
            noOfCheckStubs: 0,
            mailDate: undefined,
            isRemedSelected: false,
            enableConsumerUpload: false,
            enableDocumentUpload: false,
            fileTypeSelected: '',
            selectedFile: [],
            isFilePicked: false,
            errorMsg: '',
            remedDetailsError: false,
            remedDetailsloaded: false,
            isError: false,
            loading: false,
            noOfFileRows: 0,
            noOfFileColumns: 0,
            s3Loaded: true,
            s3LoadedProgress: 0,
            s3Url: ''
        })

        this.props.uploadCallBack('');
    }

    insertRemediationDetails = (url,docType, fileName) => {

        const urlSuffix = `SubmitFiles/SubmitFilesUpload`;
        const Url = getClientServiceURL(urlSuffix);
        
        let dtMailDate = docType !== 'Documents' ? formatDate(this.state.mailDate) : null;

       
        const remedDetails = {
            RemedNo: this.state.remedNoInput,
            RemedName: this.state.remedName,
            NoOfPayments: parseInt(this.state.noOfPayments),
            Paymentamount: parseFloat(this.state.paymentAmt),
            NoOfCheckStubs: parseInt(this.state.noOfCheckStubs),
            MailDate: dtMailDate,
            DocType: docType,
            S3Url: url,
            FileName: fileName,
            FileDisplayName: this.state.selectedFile.name
        }

        //axios.post(Url, {
        //    RemedNo: this.state.remedNoInput,
        //    RemedName: this.state.remedName,
        //    NoOfPayments: parseInt(this.state.noOfPayments),
        //    Paymentamount: parseFloat(this.state.paymentAmt),
        //    NoOfCheckStubs: parseInt(this.state.noOfCheckStubs),
        //    MailDate: dtMailDate,
        //    DocType: docType,
        //    S3Url: url,
        //    FileName: this.state.selectedFile.name
        //    })
        axios({
            method: 'post',
            url: Url,
            data: JSON.stringify(remedDetails),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => {

                console.log(res.data);

                this.props.uploadCallBack('Success', '', this.state.remedNoInput, this.state.remedName,
                    this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                    this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);

                this.setState({
                    uploadStatus: 'Success',
                    enableConsumerUpload: false,
                    s3Loaded: true,
                    isFilePicked: false,
                    selectedFile: '',
                    s3Url: ''
                });
                
            })
            .catch((error) => {

                this.props.uploadCallBack('Failed', 'Error while saving remediation', this.state.remedNoInput, this.state.remedName,
                    this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                    this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);

                this.setState({
                    uploadStatus: 'Failed',
                    s3Loaded: true,
                    isFilePicked: false,
                    selectedFile: '',
                });

            });
    }

    getFileNameToUpload = () => {
        let fileName = this.state.selectedFile.name;
        let fileExt = fileName.substr(fileName.lastIndexOf('.') + 1);

        let fileNameCustom = fileName.substr(0, fileName.lastIndexOf('.'));
        let today = new Date();
        let hours = today.getHours();
        let year = today.getFullYear();
        hours = hours.toString().padStart(2, '0');
        let month = today.getMonth() + 1;
        month = month.toString().padStart(2, '0');
        let day = today.getDate().toString().padStart(2, '0');
        let minutes = today.getMinutes().toString().padStart(2, '0');
        let seconds = today.getSeconds().toString().padStart(2, '0');
        let date = month + day + year + hours + minutes + seconds;


        fileNameCustom = fileNameCustom + '_' + this.state.remedNoInput + '_' + date + '.' + fileExt;

        return fileNameCustom;
    }

    uploadFileToS3 = (docType, fileNameWithS3Folder, fileName) => {       

        this.setState({ s3Loaded: false });

        //const urlSuffix = `AWSS3File/UploadFile`;
        //const urlSuffix = `AWSS3File/UploadConsumerFile`;

        const urlSuffix = `AWSS3File/UploadFileWithDiffFileName?fileName=${fileNameWithS3Folder}`;
        const Url = getClientServiceURL(urlSuffix);

        const formData = new FormData();
        formData.append('File', this.state.selectedFile);

        axios.post(Url, formData, {
            headers: {
                /*'Content-Type': 'multipart/form-data'*/
                'Content-Type': this.state.selectedFile.type
            },
            //onUploadProgress: ProgressEvent => {
            //    this.setState({
            //        s3LoadedProgress: Math.round(
            //            (ProgressEvent.loaded * 100) / ProgressEvent.total
            //        )
            //    });
            //}
            })
            .then((res) => {
                if (res.data === '') {

                    this.props.uploadCallBack('Failed', 'Error while uploading file to S3', this.state.remedNoInput, this.state.remedName,
                        this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                        this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);

                    this.setState({
                        uploadStatus: 'Failed',
                        s3Loaded: true,
                        isFilePicked: false,
                        selectedFile: '',
                    });
                    return 'error'
                }
                else {

                    this.setState({ s3Url: res.data });
                    this.insertRemediationDetails(res.data, docType,fileName);
                    return 'success';
                }
            })
            .catch((error) => {

                this.props.uploadCallBack('Failed', 'Error while uploading file to S3', this.state.remedNoInput, this.state.remedName,
                    this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                    this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);

                this.setState({
                    uploadStatus: 'Failed',
                    s3Loaded: true,
                    isFilePicked: false,
                    selectedFile: '',
                });

                return 'error';
            });
    };

    handleUploadOnclick = (e) => {
        e.preventDefault();        

        let fileName = this.getFileNameToUpload();

        let validationMsg = '';

        if (this.state.fileTypeSelected === 'Consumer Data' || !this.state.isRemedExists) {
            if (this.state.remedName.trim() === '') {
                validationMsg = validationMsg + 'Please enter Remediation Name. \n';
            }
            if (this.state.noOfPayments === 0 || this.state.noOfPayments === '') {
                validationMsg = validationMsg + 'Please enter Number of Payments. \n';
            }
            if (this.state.paymentAmt === 0 || this.state.paymentAmt === '') {
                validationMsg = validationMsg + 'Please enter Payment Amount \n';
            }
            if (this.state.noOfCheckStubs === 0 || this.state.noOfCheckStubs === '') {
                validationMsg = validationMsg + 'Please enter Number of Check Stubs \n';
            }
            if (this.state.mailDate === '') {
                validationMsg = validationMsg + 'Please select Mail Date \n';
            }
        }
        if (!this.state.isFilePicked) {
            validationMsg = validationMsg + 'Please choose the file';
        }

        if (validationMsg !== '') {
            showMsg(this.dialog, 'Submit Files', validationMsg);
            return;
        }

        let isError = false;
        let errorMessage = '';
        errorMessage = this.state.errorMsg;

        if (this.state.fileTypeSelected === 'Consumer Data' || this.state.isRemedExists === false) {

            if (this.state.noOfFileColumns !== noOfValidColumns) {
                isError = true;
                errorMessage = 'No of columns in the file not matching with defined value. File not uploaded.';
            }
            if (this.state.noOfFileRows !== parseInt(this.state.noOfPayments)) {
                isError = true;
                errorMessage = 'Check the number of payments assigned for this remediation. File not uploaded.';
            }

            if (isError) {
                this.props.uploadCallBack('Failed', errorMessage, this.state.remedNoInput, this.state.remedName,
                    this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                    this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);
                this.setState({
                    uploadStatus: 'Failed',
                });
            }
            else {
                if (this.state.isFilePicked === true) {

                    let s3Status;
                    let fileNameWithS3Folder = 'Consumer/' + fileName;
                    this.uploadFileToS3('Consumer', fileNameWithS3Folder, fileName);

                    //if (s3Status === 'Success') {
                    //    this.props.uploadCallBack('Success', '', this.state.remedNoInput, this.state.remedName,
                    //        this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                    //        this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);

                    //    this.setState({
                    //        uploadStatus: 'Success',
                    //        enableConsumerUpload: false
                    //    });
                    //}
                    //else {
                    //    this.props.uploadCallBack('Failed', 'Error while uploading file to S3', this.state.remedNoInput, this.state.remedName,
                    //        this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                    //        this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);
                    //    this.setState({
                    //        uploadStatus: 'Failed',
                    //    });
                    //}

                }
            }

        }

        else {
            if (this.state.isFilePicked === true) {

                let fileNameWithS3Folder = 'Documents/' + fileName;
                this.uploadFileToS3('Documents', fileNameWithS3Folder, fileName);

                //this.props.uploadCallBack('Success', '', this.state.remedNoInput, this.state.remedName,
                //    this.state.noOfPayments, this.state.paymentAmt, this.state.noOfCheckStubs, this.state.mailDate,
                //    this.state.fileTypeSelected, this.state.selectedFile, this.state.isRemedExists);
                //this.setState({
                //    uploadStatus: 'Success',
                //    selectedFile: '',
                //    isFilePicked: false,
                //});
            }
        }

    }

    handleInputClick = (event) => {
        event.target.value = ''
    }

    handleUploadConsumer = (e) => {        

        if (e.target.files.length === 0) {
            this.setState({
                isFilePicked: false,
                selectedFile: [],
                noOfFileRows: 0,
                noOfFileColumns: 0,
            })
        }
        else {
            this.setState({
                isFilePicked: true,
                selectedFile: e.target.files[0],
                errorMsg: ''
            })

            if (this.state.fileTypeSelected === 'Consumer Data' || this.state.isRemedExists === false) {

                let fileName = e.target.files[0].name;
                let fileExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                if (fileExt === 'csv') {
                    this.setState({ isFilePicked: true })
                } else {
                    this.setState({
                        errorMsg: 'Please select a CSV file',
                        isFilePicked: false,
                        selectedFile: '',
                    })
                    showMsg(this.dialog, 'Submit Files', 'Please select CSV file.');
                    return;
                }
                const input = e.target;
                const reader = new FileReader();
                reader.onload = () => {
                    const lines = reader.result.split(/\r\n|\n/);
                    let linesIgnoreEmpty = lines.length -1;
                    const headerRow = lines[0];
                    const fileColumns = headerRow.split('|');
                    //for excel
                    //const headers = dataStringLines[0].split(/,(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)/);
                                        
                    //let isValidRemedNo = true;
                    for (let i = 1; i < lines.length; i++) {
                       
                        const item = lines[i];

                        if (item === "\n" || item.trim().length === 0) {
                            linesIgnoreEmpty = linesIgnoreEmpty - 1;
                        }

                        //const row = lines[i].split('|');
                        //if (fileColumns && row.length === fileColumns.length) {
                        //    for (let j = 0; j < fileColumns.length; j++) {
                        //        let item = row[j];
                        //        if (item === "\n" || item.trim().length === 0) {
                                
                        //        }
                        //        else {
                        //            if (item[0] !== this.state.remedNoInput)
                        //                isValidRemedNo = false;                                    
                        //        }
                        //    }
                        //}
                    }

                    this.setState({
                        noOfFileRows: linesIgnoreEmpty,
                        noOfFileColumns: fileColumns.length,
                    })
                };

                reader.readAsText(input.files[0]);
            }
            else {
                this.setState({
                    selectedFile: e.target.files[0],
                    errorMsg: '',
                    isFilePicked: true,
                    enableDocumentUpload: true
                })
            }
        }        
    };

    remedDetailsControls() {
        return (
            <>
                <div class="form-group">
                    <label class="strong required" for="Remediation" >Remed. Name</label>
                    <input class="form-control" type="text" value={this.state.remedName} name="remedName" maxLength="100"
                        onChange={this.handleremedNameChange} placeholder="Remediation Name" disabled={this.state.isConsumerDataInProcess} />
                </div>

                <div class="form-group">
                    <label class="strong required" for="Payments"># of Pymts</label>
                    <CurrencyInput className="form-control" allowNegativeValue={false} name="noOfPayments" value={this.state.noOfPayments}
                        onChange={this.handleNoOfPaymentsChange}
                        onKeyPress={(e) => this.handlesKeyPress(e, false)} disabled={this.state.isConsumerDataInProcess}/>
                </div>

                <div class="form-group">
                    <label class="strong required" for="Payment Amount">Total Pymt Amount</label>
                    <CurrencyInput className="form-control" allowNegativeValue={false} name="paymentAmt" value={this.state.paymentAmt} decimalsLimit={2}
                        onChange={this.handlePaymentAmtChange} prefix="$"
                        onKeyPress={(e) => this.handlesKeyPress(e, true)} disabled={this.state.isConsumerDataInProcess}/>
                </div>

                <div class="form-group">
                    <label class="strong required" for="Check Stubs"># of Check Stubs</label>
                    <CurrencyInput className="form-control" allowNegativeValue={false} name="noOfCheckStubs" value={this.state.noOfCheckStubs}
                        onChange={this.handleNoOfChecksChange} maxLength="5"
                        onKeyPress={(e) => this.handlesKeyPress(e, false)} disabled={this.state.isConsumerDataInProcess}/>
                </div>

                <div class="form-group">
                    <label class="strong required" for="Mail Date">Approx. Mail Date</label>
                    <DatePicker className="form-control sm-date"
                        selected={(this.state.mailDate) ? Date.parse(this.state.mailDate) : this.state.mailDate}
                        onChange={(date) => this.handleMailDateChange(date)}
                        disabled={this.state.isConsumerDataInProcess}
                        placeholderText={"mm/dd/yyyy"}
                        name='mailDate'
                        dateFormat="MM/dd/yyyy"
                        maxDate={new Date(9999, 12, 31)}
                        />
                    </div>

                {/*<div class="form-group">
                    <label class="strong required" for="Mail Date">Approx. Mail Date</label>
                    <input id="mailDate" type="date" class="form-control " placeholder="Approx. Mail Date" value={this.state.mailDate} name="mailDate"
                        onChange={this.handleChange} disabled={this.state.isConsumerDataInProcess}/>
                </div>*/}
            </>
        );
    }

    uploadFileControls(fileUploadClass) {

        return (
            <div className={fileUploadClass}>
                <div class="form-group mt-5">


                    <label for="File"><b>File&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> {this.state.isFilePicked > 0 ? this.state.selectedFile.name : ''}
                    </label>

                    <div class="file file--upload file-active mt-2">
                        <label for="input-file" class="required">
                            <i class="fas fa-cloud-upload-alt"></i> Choose File
                                </label>                       
                        <input id="input-file" type="file" accept=
                            {(this.state.fileTypeSelected === 'Consumer Data' || this.state.isRemedExists === false) ? ".csv" : "*"}
                            onChange={this.handleUploadConsumer} onClick={this.handleInputClick}
                            disabled={this.state.fileTypeSelected === 'Documents' ? !this.state.enableDocumentUpload :  this.state.isConsumerDataInProcess }
                        />

                    </div>
                </div>

                <div class="form-group">
                    <div class="row text-center mt-4">
                        <div class="col-lg-12">
                            <div>
                            {this.state.fileTypeSelected === 'Documents' ?
                                <button class="btn btn-primary col-sm-6" onClick={this.handleUploadOnclick} disabled={!this.state.enableDocumentUpload}>Upload File</button>
                                :
                                    <button class="btn btn-primary col-sm-6" onClick={this.handleUploadOnclick} disabled={!this.state.enableConsumerUpload || this.state.isConsumerDataInProcess }>Upload File</button>
                                }
                            </div>
                            <div>{this.state.s3Loaded ? '' : <ReactBootStrap.Spinner animation="border" />}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    cancelControl(cancelClass) {
        return (
            <div class="form-group" className={cancelClass}>
                <div class="row text-center mt-4">
                    <div class="col-lg-12 mt-2">
                        <button class="btn btn-primary col-sm-6" onClick={this.handleCancelOnclick} disabled={!this.state.s3Loaded}>
                            {this.state.enableDocumentUpload ? 'Cancel' : this.state.enableConsumerUpload ? 'Cancel' : 'Complete'}                            
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    render() {

        let remedDetailsClass = (this.state.isRemedSelected && this.state.isRemedExists === false) ? 'remedDisplay' : this.state.fileTypeSelected === 'Consumer Data' ? 'remedDisplay' : 'remedNone';
        let fileTypeClass = this.state.isRemedExists === false ? 'remedNone' : this.state.isRemedSelected && (this.state.fileTypeSelected === '' || this.state.fileTypeSelected === 'Documents') ? 'remedDisplay' : 'remedNone';
        let cancelClass = this.state.isRemedSelected ? 'remedDisplay' : 'remedNone';
        let fileUploadClass = (this.state.isRemedSelected && this.state.isRemedExists === false) ? 'remedDisplay' : (this.state.isRemedSelected && this.state.fileTypeSelected !== '') ? 'remedDisplay' : 'remedNone';        
                
        return (
            <div class="row ">
                <div class="col box submit-leftpanel p-50">
                    <form>                        
                            <div class="strong mb-3">Enter the Remediation # to upload Consumer Data:</div>

                            <div class="form-group">
                            <label class="strong required" for="RemediationNo">Remed. #</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="txtRemedNo" placeholder="Remediation #" value={this.state.remedNoInput}
                                    disabled={this.state.isRemedSelected} onChange={this.remedNoOnChange} maxLength="20"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default submit-search form-control" type="button" onClick={this.remedNoOnClick}><i class="fas fa-caret-right"></i></button>
                                    </span>
                             </div>
                        </div>
                        <div style={{ height: '340px' }}>
                            {
                                this.state.isRemedExists === true ?

                                    this.state.isConsumerDataUploaded ?

                                        <div class="form-group" className={fileTypeClass}>
                                            <label class="strong" for="FileType">File Type</label>
                                            <div class="form-group">
                                                <select className="form-control" placeholder="File Type" defaultValue={this.state.fileTypeSelected} onChange={this.handleFileTypeChange}>
                                                    <option ket='0' value='' selected={this.state.fileTypeSelected === '' ? "selected" : ''}></option>
                                                    <option key='2' value='Documents' selected={this.state.fileTypeSelected === 'Documents' ? "selected" : ''}>Documents</option>
                                                </select>
                                            </div>
                                        </div>

                                        :

                                        <>
                                            <div class="form-group" className={fileTypeClass}>
                                                <label class="strong" for="FileType">File Type</label>
                                                <div class="form-group">
                                                    <select className="form-control" placeholder="File Type" onChange={this.handleFileTypeChange}>
                                                        <option ket='0' value='' selected={this.state.fileTypeSelected === '' ? "selected" : ''}></option>
                                                        <option key='1' value='Consumer Data' selected={this.state.fileTypeSelected === 'Consumer Data' ? "selected" : ''}>Consumer Data</option>
                                                        <option key='2' value='Documents' selected={this.state.fileTypeSelected === 'Documents' ? "selected" : ''}>Documents</option>
                                                    </select>
                                                </div>

                                            </div>
                                            
                                            <div className={remedDetailsClass} >
                                                {this.remedDetailsControls()}
                                            </div>
                                        </>
                                    : this.state.isRemedExists === false && this.state.isRemedSelected ?
                                    <div className={remedDetailsClass} >
                                        {this.remedDetailsControls()}
                                    </div>
                                        :''
                            }

                        </div>
                        {this.uploadFileControls(fileUploadClass)}
                        {this.cancelControl(cancelClass)}
                                                                    
                        </form>
                </div>

                <Dialog ref={(el) => { this.dialog = el }} />

                </div>
 
        );
    }

    
}
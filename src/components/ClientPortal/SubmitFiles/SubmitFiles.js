﻿import React, { Component } from 'react';
import { Container } from 'reactstrap';

import { Upload } from './Upload';
import { UploadStatus } from './UploadStatus';
import '../../../css/css/SubmitFiles.Styles.css'

export class SubmitFiles extends Component {

    constructor(props) {
        super(props);
        this.state = {
            uploadStatus: '',
            uploadMsg: '',
            remedNo: '',
            remedName: '',
            noOfPayments: 0,
            paymentAmt: 0,
            noOfCheckStubs: 0,
            mailDate: '',
            fileTypeSelected: '',
            selectedFile: '',
            isRemedExists: false
        }

        this.childUpoad = React.createRef();
    }

    uploadCallBackFunction = (status, msg, remedNo, remedName, noOfPayments, paymentAmt, noOfCheckStubs, mailDate, fileTypeSelected, selectedFile, isRemedExists) => {
        this.setState({
            uploadStatus: status,
            uploadMsg: msg,
            remedNo: remedNo,
            remedName: remedName,
            noOfPayments: noOfPayments,
            paymentAmt: paymentAmt,
            noOfCheckStubs: noOfCheckStubs,
            mailDate: mailDate,
            fileTypeSelected: fileTypeSelected,
            selectedFile: selectedFile,
            isRemedExists: isRemedExists
        })
        
    }

    render() {
        return (
            <Container fluid className="content-section">

                <h1 className="h2-title bold">Submit Files</h1>

                <div className="row mt-2 mb-3">

                    <div className="col-lg-3">
                        <Upload ref={this.childUpoad} uploadCallBack={this.uploadCallBackFunction}/>
                    </div>


                    <div class="col-lg-9">
                        <UploadStatus status={this.state.uploadStatus} msg={this.state.uploadMsg} remedNo={this.state.remedNo}
                            remedDetails={this.state.remedDetails} fileTypeSelected={this.state.fileTypeSelected} selectedFile={this.state.selectedFile}
                            isRemedExists={this.state.isRemedExists} noOfPayments={this.state.noOfPayments} paymentAmt={this.state.paymentAmt}
                            noOfCheckStubs={this.state.noOfCheckStubs} />
                    </div>

                </div>
            </Container>


        );
    }
}

﻿import React, { Component } from 'react';
import { Container } from 'reactstrap';
import Dialog from 'react-bootstrap-dialog';
import DatePicker from "react-datepicker";

import { ReportingResults } from './ReportingResults';
import { showMsg } from '../../Shared/Util/CommonFunctions';

import '../../../css/css/Reporting.Styles.css'
import 'react-datepicker/dist/react-datepicker.css';

export class Reporting extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedReport: '',
            remedNoOrName: '',
            startDate: '',
            endDate: ''
        }
        this.reportingResults = React.createRef();
    }

    handleReportOnChange = (e) => {

        this.setState({
            selectedReport: e.target.value,
            remedNoOrName: '',
            startDate: '',
            endDate: '',
            runReportClicked: false
        });
        this.reportingResults.current.reportOnChange();
    }

    handleRemedNoOrNameChange = (e) => {
        const value = e.target.value;
        const regex = /^[a-z\d\-_\s]+$/i;
        if (value.match(regex) || value === "") {
            this.setState({ remedNoOrName: e.target.value });
        }        
    }

    handlesStartDateChange = (date) => {
        if (date !== null) {
            this.setState({ startDate: date });
            const EndDt = this.state.endDate;
            const StartDt = date;
            if (EndDt !== '' && EndDt < StartDt) {
                this.setState({ endDate: '' });
            }
        } else {
            this.setState({ startDate: '' });
        }
    }

    handlesEndDateChange = (date) => {
        if (date !== null) {
            this.setState({ endDate: date });
        } else {
            this.setState({ endDate: '' });
        }
    }

    checksMailedOrReissueControls() {
        return (
            <>
                <div class="col-lg-3">
                    <label class="">Enter Remediation Name Or Number</label>
                    <div class="form-group">
                        <input type="text" name="remedNoOrName" class="form-control" value={this.state.remedNoOrName} onChange={this.handleRemedNoOrNameChange} />
                    </div>
                </div>


                <div class="col-lg-2">
                    <label class="">Check Date From:</label>
                    <div class="form-group">              
                        <DatePicker className="form-control date-input"
                            selected={this.state.startDate}
                            onChange={(date) => this.handlesStartDateChange(date)}
                            selectsStart
                            startDate={this.state.startDate}
                            endDate={this.state.endDate}
                            placeholderText={"mm/dd/yyyy"}
                        />
                    </div>
                </div>

                <div class="col-lg-2">
                    <label class="">  To </label>
                    <div class="form-group">                        
                        <DatePicker className="form-control date-input"
                            selected={this.state.endDate}
                            onChange={(date) => this.handlesEndDateChange(date)}
                            selectsEnd
                            startDate={this.state.startDate}
                            endDate={this.state.endDate}
                            minDate={this.state.startDate}
                            placeholderText={"mm/dd/yyyy"}
                        />
                    </div>
                </div>

            </>
        );
    }

    summaryOrDataFilesControls() {
        return (
            <>
                <div class="col-lg-3">
                    <label class="">Enter Remediation Name Or Number</label>
                    <div class="form-group fome-inline ">
                        <input type="text" name="remedNoOrName" class="form-control" value={this.state.remedNoOrName} onChange={this.handleRemedNoOrNameChange} />              
                    </div>
                </div>
            </>
        )
    }

    handleRunReportClick = (e) => {
        if (this.state.selectedReport === '')
            showMsg(this.dialog, 'Reporting', 'Please select an report');
        else {

            if (this.state.remedNoOrName.trim() === '')
                this.setState({ remedNoOrName: '' });

            if (this.state.selectedReport === 'Data Files Report') {
                this.setState({ runReportClicked: true });               
                this.reportingResults.current.runReport(this.state.selectedReport, this.state.remedNoOrName.trim(), this.state.startDate, this.state.endDate);
            }
            else {
                if (this.state.remedNoOrName.trim() !== '') {
                    this.setState({ runReportClicked: true });
                    this.reportingResults.current.runReport(this.state.selectedReport, this.state.remedNoOrName.trim(), this.state.startDate, this.state.endDate);
                }
                else {
                    showMsg(this.dialog, 'Reporting', 'Please enter Remediation Name or Number.');
                }
            }
        }
    }

    render() {

        return (

            <Container fluid className="content-section">

            <h1 class="h2-title bold">Reporting</h1>

            <div class="row mt-2 mb-3">

                <div class="col-lg-12">
                    <div class="row mt-2 mb-3">
                        <div class="col">
                            <div class="box box-border reports">
                                <div><h1 class="h1-title">All Reports</h1></div>

                            <div class="row">
                              <div class="col-lg-3">
                                <label class="">Select a report</label>
                                <div class="form-group fome-inline">
                                    <select onChange={ this.handleReportOnChange} class="form-control" id="exampleFormControlSelect1">
                                      <option value = "">Select</option>
                                            <option value="High Level Summary Report">High Level Summary Report</option>
                                            <option value="Checks Mailed Report">Checks Mailed Report</option>
                                            <option value="Data Files Report">Data Files Report</option>
                                            <option value="Reissue Report">Reissue Report</option>
                                    </select>
                                </div>
                             </div>

                                        {(this.state.selectedReport === ('High Level Summary Report')) || (this.state.selectedReport === ('Data Files Report')) ?
                                            
                                            this.summaryOrDataFilesControls() :
                                            (this.state.selectedReport === ('Checks Mailed Report')) || (this.state.selectedReport === ('Reissue Report')) ?
                                                this.checksMailedOrReissueControls():''
                                        }

                                        {this.state.selectedReport !== ''?
                                            <div class="col-lg-2 mt-4 pt-1"><button class="btn btn-primary" onClick={this.handleRunReportClick}>Run Report</button></div>
                                            : ''
                                        }
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

                <ReportingResults ref={this.reportingResults} selectedReport={this.state.selectedReport} isReportClicked={this.state.runReportClicked} />

                <Dialog ref={(el) => { this.dialog = el }}/>

            </Container>
            )
    }
}
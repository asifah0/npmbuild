﻿import React from 'react';
import '../../../css/css/Reporting.Styles.css';

export const generateHeader = () => {
    return (
        <>

            <th class="text-left" style={{width:'10%'}}>
                    Remed.#
            </th>

            <th class="text-left" style={{ width: '10%' }}>
                   File Type
            </th>

            <th class="text-left" style={{ width: '15%' }}>
                    Filename
            </th>


            <th class="text-left" style={{ width: '10%' }}>
                    Date Received
            </th>


            <th class="text-left" style={{ width: '15%' }}>
                    Upload User
            </th>

            <th class="text-left" style={{ width: '10%' }}>
                    # of Pymts
            </th>

            <th class="text-left" style={{ width: '10%' }}>
                Pymt Amount
            </th>

            <th class="text-left" style={{ width: '30%' }}>
                File Status
            </th>

        </>
    )
}
﻿import React from 'react';

import { numberWithCommas, numberWithCommasAmount, getFormattedDate } from '../../Shared/Util/CommonFunctions';

export const generateTableData = (items) => {

    return items.map((item, index) => {
        return (
            <tr key={index}>
                <td class="text-left">{index === 0 ? item.remedNo : items[index - 1].remedSK !== item.remedSK ? item.remedNo : ''}</td>
                <td class="text-left">{item.fileType}</td>
                <td class="text-left">{item.docFileName}</td>
                <td class="text-left">{getFormattedDate(item.dateReceived)}</td>
                <td class="text-left">{item.uploadUser}</td>
                <td class="text-left">{item.fileType.toUpperCase() === 'CONSUMER' ? numberWithCommas(item.noOfPayments) : '' }</td>
                <td class="text-left">{item.fileType.toUpperCase() === 'CONSUMER' ? numberWithCommasAmount(item.paymentAmount) : ''}</td>
                <td class="text-left">{item.fileUploadStatus}</td>
            </tr>
        )
    })
}
﻿import React from 'react';

export const GenerateHeaderForPrint = () => {
    return (
        <>

            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Total</th></tr>
                    <tr> <th class="col-sm-6 text-left" style={{ width: '12%'}}>
                        Check Count
                    </th>

                        <th class="col-sm-6 text-left" style={{ width: '15%'}}>
                            Amount
                    </th>
                    </tr>
                </table>
            </th>



            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Cashed</th></tr>
                    <tr> <th class="col-sm-6 text-left" style={{ width: '12%'}}>
                        Check Count
                    </th>

                        <th class="col-sm-6 text-left" style={{ width: '15%'}}>
                            Amount
                    </th>
                    </tr>
                </table>
            </th>

            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Uncashed</th></tr>
                    <tr> <th class="col-sm-6 text-left" style={{ width: '12%'}}>
                        Check Count
                    </th>

                        <th class="col-sm-6 text-left" style={{ width: '15%'}}>
                            Amount
                    </th>
                    </tr>
                </table>
            </th>


            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Undeliverable</th></tr>
                    <tr> <th class="col-sm-6 text-left" style={{ width: '12%'}}>
                        Check Count
                    </th>

                        <th class="col-sm-6 text-left" style={{ width: '15%'}}>
                            Amount
                    </th>
                    </tr>
                </table>
            </th>

            <th colspan="2">
                <table class="row amt-head">

                    <tr> <th colspan="2" class="col-sm-12 ttl-line strong text-center">Voided</th></tr>
                    <tr> <th class="col-sm-6 text-left" style={{ width: '12%'}}>
                        Check Count
                    </th>

                        <th class="col-sm-6 text-left" style={{ width: '15%'}}>
                            Amount
                    </th>
                    </tr>
                </table>
            </th>
        </>
    );
}

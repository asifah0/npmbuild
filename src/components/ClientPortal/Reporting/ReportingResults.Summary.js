﻿import React, { useState } from 'react';

import * as ReactBootStrap from 'react-bootstrap';
import axios from 'axios';
import { saveAs } from 'file-saver';

import { generateHeader } from './ReportingResults.Summary.GenerateHeader';
import { generateTableData } from './ReportingResults.Summary.GenerateTableData';
import { GenerateHeaderForPrint } from './ReportingResults.Summary.GenerateHeaderForPrint';
import { generateTableDataForPrint } from './ReportingResults.Summary.GenerateTableDataForPrint';
import Print from '../../Shared/Print';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';

const HighLevelSummaryReport = (props) => {

    const [results, setResults] = useState([]);

    const imageExcel = require('../../../css/images/XL.svg');

    React.useEffect(() => {
        setResults(props.results);
    }, [props.results])

  
    const exportToExcelClick = () => {
        const fileName = 'HighLevelSummary.xlsx';
        const urlSuffix = props.urlSuffixExport;
        const Url = getClientServiceURL(urlSuffix);

        axios.get(Url,
            {
                responseType: 'arraybuffer'
            })
            .then((response) => {

                const blob = new Blob([response.data], {
                    type: 'application/octet-stream'
                })
                saveAs(blob, fileName);
            })
            .catch((error) => {

            });
    }
    return (

        <>
            <div className="row">
                <div className="col-lg-6">
                    <h1 className="h1-title"> High Level Summary Report</h1>
                </div>
                <div className="col-lg-6 text-right">
                    <span>
                        <button className='btn btn-link' onClick={() => exportToExcelClick()}><img src={imageExcel} alt="ExportToExcel" /></button>
                    </span>
                    <span className="ml-2">
                        <Print header={GenerateHeaderForPrint()} data={generateTableDataForPrint(results)} />
                    </span>
                </div>
            </div>



            <div className="table-scroll" style={{ height: '450px' }}>
                {props.isServerError && <div class="label-error-red ml-3"> <span>Unable to get the data due to internal server error. Please try after sometimes.</span></div>}
                <table id="remed-summary" class="table table-striped table-bordered dataTable no-footer fixed_headers" style={{ width: "100%" }} role="grid" aria-describedby="example2_info">
                    <thead>
                        <tr>
                            {generateHeader()}
                        </tr>
                    </thead>
                    <tbody>
                        {props.loaded ? generateTableData(results) : <ReactBootStrap.Spinner animation="border" />}
                    </tbody>
                </table>
            </div>
        </>
    );
};

export default HighLevelSummaryReport;
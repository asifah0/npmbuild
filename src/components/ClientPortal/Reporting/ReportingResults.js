﻿import React, { Component } from 'react';
import axios from 'axios';
import Dialog from 'react-bootstrap-dialog';

import HighLevelSummaryReport from './ReportingResults.Summary';
import ChecksMailedReport from './ReportingResults.ChecksMailed';
import ReissueReport from './ReportingResults.Reissue';
import DataFilesReport from './ReportingResults.DataFiles';
import { showMsg } from '../../Shared/Util/CommonFunctions';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';

import '../../../css/css/Reporting.Styles.css';

const defaultImg = require('../../../css/images/defaultUploadIcon.svg');

export class ReportingResults extends Component {

    constructor(props) {
        super(props);
        this.state = {
            remedNoOrName: '',
            runReportCalled: false,
            selectedReport: '',
            results: [],
            loaded: false,
            isServerError: false,
            urlSuffixExport: ''
        }
        this.setState({ runReportCalled: this.props.isReportClicked });
    }

    reportOnChange = () => {
        this.setState({
            runReportCalled: false,
            results: []
        });
    }

    fetchReport = (selectedReport, remedNoOrName, startDate, endDate) => {

        this.setState({
            loaded: false,
            isServerError: false,
            urlSuffixExport: '',
            results: []
        });
        let urlSuffix = "";

        if (selectedReport === 'High Level Summary Report') {
            if (remedNoOrName !== '') {
                urlSuffix = `Reporting/GetHighLevelSummaryReport?remedNoOrName=${remedNoOrName}`;
                this.setState({ urlSuffixExport: `Reporting/GetHighLevelSummaryReport/ExportResults?remedNoOrName=${remedNoOrName}` });
            }
        }
        else if (selectedReport === 'Checks Mailed Report') {
            if (remedNoOrName !== '') {
                if (startDate !== '' && endDate !== '') {
                    urlSuffix = `Reporting/GetChecksMailedReport?remedNoOrName=${remedNoOrName}&FromDt=${startDate.toLocaleDateString("en-US")}&ToDt=${endDate.toLocaleDateString("en-US")}`;
                    this.setState({
                        urlSuffixExport: `Reporting/GetChecksMailedReport/ExportResults?remedNoOrName=${remedNoOrName}&FromDt=${startDate.toLocaleDateString("en-US")}&ToDt=${endDate.toLocaleDateString("en-US")}`
                    });
                }
                else {
                    urlSuffix = `Reporting/GetChecksMailedReport?remedNoOrName=${remedNoOrName}`;
                    this.setState({
                        urlSuffixExport: `Reporting/GetChecksMailedReport/ExportResults?remedNoOrName=${remedNoOrName}`
                    });
                }
            }
        }
        else if (selectedReport === 'Reissue Report') {
            if (remedNoOrName !== '') {
                if (startDate !== '' && endDate !== '') {
                    urlSuffix = `Reporting/GetReissueReport?remedNoOrName=${remedNoOrName}&FromDt=${startDate.toLocaleDateString("en-US")}&ToDt=${endDate.toLocaleDateString("en-US")}`;
                    this.setState({
                        urlSuffixExport: `Reporting/GetReissueReport/ExportResults?remedNoOrName=${remedNoOrName}&FromDt=${startDate.toLocaleDateString("en-US")}&ToDt=${endDate.toLocaleDateString("en-US")}`
                    });
                }
                else {
                    urlSuffix = `Reporting/GetReissueReport?remedNoOrName=${remedNoOrName}`;
                    this.setState({
                        urlSuffixExport: `Reporting/GetReissueReport/ExportResults?remedNoOrName=${remedNoOrName}`
                    });
                }
            }
        }
        else if (selectedReport === 'Data Files Report') {
            if (remedNoOrName !== '') {
                urlSuffix = `Reporting/GetDataFilesReport?remedNoOrName=${remedNoOrName}`;
                this.setState({
                    urlSuffixExport: `Reporting/GetDataFilesReport/ExportResults?remedNoOrName=${remedNoOrName}`
                });
            }
            else {
                urlSuffix = `Reporting/GetDataFilesReport`;
                this.setState({
                    urlSuffixExport: `Reporting/GetDataFilesReport/ExportResults`
                });
            }
        }
        
        const Url = getClientServiceURL(urlSuffix);

        let dataFilesResults = [];
        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {

                console.log(res.data);
                for (var i = 0; i < res.data.length; i++) {
                    dataFilesResults.push(res.data[i])
                }

                this.setState({
                    results: dataFilesResults,
                    loaded: true
                });
                if (dataFilesResults.length === 0)
                    showMsg(this.dialog, 'Reporting Results', 'No results Found');

            })
            .catch((error) => {
                if (axios.isCancel(error) || error) {

                }
                this.setState({
                    loaded: true,
                    isServerError: true
                });
            });
    }

    runReport = (selectedReport, remedNoOrName, startDate, endDate) => {
        this.setState({
            remedNoOrName: remedNoOrName,
            runReportCalled: true,
            selectedReport: selectedReport
        });

        this.fetchReport(selectedReport, remedNoOrName, startDate, endDate);       
    }

    render() {
        return (
            <div class="row mt-3">
                <div class="col-lg-12">

                    <div class="box box-border reportingBox">

                        {(this.state.selectedReport === '' || this.state.runReportCalled === false) ?
                            <div class="text-center">
                                <p>Run your report using the boxes at the top</p>
                                <img src={defaultImg} alt='default' />

                            </div>
                            : this.props.selectedReport === 'High Level Summary Report' ?
                                <HighLevelSummaryReport results={this.state.results} loaded={this.state.loaded} isServerError={this.state.isServerError} urlSuffixExport={this.state.urlSuffixExport} />
                                : this.props.selectedReport === 'Checks Mailed Report' ?
                                    <ChecksMailedReport results={this.state.results} loaded={this.state.loaded} isServerError={this.state.isServerError} urlSuffixExport={this.state.urlSuffixExport}/>
                                    : this.props.selectedReport === 'Reissue Report' ?
                                        <ReissueReport results={this.state.results} loaded={this.state.loaded} isServerError={this.state.isServerError} urlSuffixExport={this.state.urlSuffixExport}/>
                                        : <DataFilesReport results={this.state.results} loaded={this.state.loaded} isServerError={this.state.isServerError} urlSuffixExport={this.state.urlSuffixExport}/>
                        }
                    </div>
                </div>

                <Dialog ref={(el) => { this.dialog = el }} />

            </div>

            )
    }
}
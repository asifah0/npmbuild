﻿import React from 'react';

import { numberWithCommasAmount, getFormattedDate } from '../../Shared/Util/CommonFunctions';

export const generateTableData = (items) => {

    return items.map((item, index) => {
        return (
            <>
                <tr key={item.ConsumerSK} >
                    <td class="text-left">{item.remedNo}</td>
                    <td class="text-left">{item.accountNo}</td>
                    <td class="text-left">{item.broadridgeNo}</td>
                    <td class="text-left">{item.consumerName}</td>

                    <td class="text-left">{item.address1}</td>
                    <td class="text-left">{item.address2}</td>
                    <td class="text-left">{item.city}</td>
                    <td class="text-left">{item.state}</td>
                    <td class="text-left">{item.zip}</td>
                    <td class="text-left">{item.countryCode}</td>

                    <td class="text-left">{item.reissueReason}</td>
                    <td class="text-left">{getFormattedDate(item.reissueRqstdDate)}</td>

                    <td class="text-left">{item.checkNo}</td>
                    <td class="text-left">{getFormattedDate(item.checkDate)}</td>
                    <td class="text-left">{numberWithCommasAmount(item.checkAmount)}</td>
                    
                </tr>
            </>
        )
    })
}
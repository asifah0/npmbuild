﻿import React from 'react';

import { numberWithCommas, numberWithCommasAmount } from '../../Shared/Util/CommonFunctions';

export const generateTableData = (items) => {

    return items.map((item, index) => {
        return (
            <tr key={item.remedSK}>
                <td colSpan="2">
                    <div class="row amt-head">
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommas(item.totalCheckCount)}</div>
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommasAmount(item.totalDollarAmount)}</div>
                    </div>
                </td>

                <td colSpan="2">
                    <div class="row amt-head"><div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommas(item.cashedCheckCount)}</div>
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommasAmount(item.cashedDollarAmount)}</div>
                    </div>
                </td>

                <td colSpan="2">
                    <div class="row amt-head">
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommas(item.uncashedCheckCount)}</div>
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommasAmount(item.uncashedDollarAmount)}</div>
                    </div>
                </td>

                <td colSpan="2">
                    <div class="row amt-head">
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommas(item.undeliverableCheckCount)}</div>
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommasAmount(item.undeliverableDollarAmount)}</div>
                    </div>
                </td>

                <td colSpan="2">
                    <div class="row amt-head">
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommas(item.voidedCheckCount)}</div>
                        <div class="col-sm-6" style={{ align: 'left' }}>{numberWithCommasAmount(item.voidedDollarAmount)}</div>
                    </div>
                </td>

            </tr>
        )
    })
}
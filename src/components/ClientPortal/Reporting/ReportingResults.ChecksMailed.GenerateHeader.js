﻿import React from 'react';
import '../../../css/css/Reporting.Styles.css';

export const generateHeader = () => {
    return (
        <>

            <th class="text-left">
                    Remed. #
            </th>

            <th class="text-left">
                    Account #
            </th>

            <th class="text-left">
                    Broadridge #
            </th>


            <th class="text-left">
                    Consumer Name
            </th>


            <th class="text-left">
                    Address 1
            </th>

            <th class="text-left">
                    Address 2
            </th>

            <th class="text-left">
                    City
            </th>

            <th class="text-left">
                    State
            </th>


            <th class="text-left">
                    Zip
            </th>

            <th class="text-left">
                    Country Code
            </th>

            <th class="text-left">
                    Check #
            </th>

            <th class="text-left">
                    Check Date
            </th>

            <th class="text-left">
                    Check Status
            </th>

            <th class="text-left">
                    Check Amount
            </th>
          

            <th class="text-left">
                    Status Date
            </th>
        </>
        )
}
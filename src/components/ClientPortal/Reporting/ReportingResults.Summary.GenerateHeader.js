﻿import React from 'react';
import '../../../css/css/Reporting.Styles.css';

export const generateHeader = () => {
    return (
        <>           
            <th colspan="2" class="text-left">
                <div class="row amt-head">
                    <div class="ttl-line">Total</div>
                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Check Count
                    </div>

                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Amount
                    </div>
                </div>
            </th>


            <th colspan="2" class="text-left">
                <div class="row amt-head">
                    <div class="ttl-line">Cashed</div>
                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Check Count
                    </div>

                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Amount
                    </div>
                </div>
            </th>

            <th colspan="2" class="text-left">
                <div class="row amt-head">
                    <div class="ttl-line">Uncashed</div>
                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Check Count
                    </div>

                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Amount
                    </div>
                </div>
            </th>


            <th colspan="2" class="text-left">
                <div class="row amt-head">
                    <div class="ttl-line">Undeliverable</div>
                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Check Count
                    </div>

                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                           Amount
                    </div>
                </div>
            </th>

            <th colspan="2" class="text-left">
                <div class="row amt-head">
                    <div class="ttl-line">Voided</div>
                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Check Count
                    </div>

                    <div class="col-sm-6 text-left" style={{ align: 'left' }}>
                            Amount
                    </div>
                </div>
            </th>
        </>
    );
}

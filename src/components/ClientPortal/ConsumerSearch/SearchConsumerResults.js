﻿
import React, { Component } from 'react';
import * as ReactBootStrap from 'react-bootstrap';
//import SlidingPane from "react-sliding-pane";
import SlidingPane from '../../Shared/SlidingPane';
import axios from 'axios';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import BootstrapTable from 'react-bootstrap-table-next';
import { saveAs } from 'file-saver';
import Dialog from 'react-bootstrap-dialog';

import { ExportCSV } from '../../Shared/ExportToExcel';
import PrintConsumerSearchResults from './PrintConsumerSearchResults';
import ConsumerSearchRecordDetails from './ConsumerSearchRecordDetails';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';
import { numberWithCommas } from '../../Shared/Util/CommonFunctions';
import { getFormattedDate } from '../../Shared/Util/CommonFunctions';
import { getFormattedCurrentDate } from '../../Shared/Util/CommonFunctions';
import { GetErrorMessageDescription } from '../../Shared/Util/CommonFunctions';
import { isTextNullOrEmpty } from '../../Shared/Util/CommonFunctions';

export class SearchConsumerResults extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchInput: props.dataFromParent,
            countryList: props.countryList,
            reissueReasonList: props.reissueReasonList,
            resultsToShow: [],
            resultsToHideExpansion: [],
            next: 0,
            total: 0,
            pageSize: 20,
            isConsumerRecordPopupOpened: false,
            isReissuePanelPopupOpened: false,
            isSuccessPanelPopupOpened: false,
            columnKey: 'consumerName',
            columnSortDirection: '',
            readOnly: false,
            loading: false,
            loadingExport: false,
            loadingReissueInfoRead: false,
            loadingReissueSubmit: false,

            isServerError: false,
            serverErrorMessage: 'Unknown Error',
            isServerErrorReIssueInfo: false,
            isServerErrorReIssueSubmit: false,
            serverErrorMessageReIssueInfo: 'Unknown Error',

            selectedRecord: [],
            selectedCheckRecord: {},
            reissueConsumerNameSaved: '',
            exportURL: ''
        };
    }

    onRowDropdownChangEvent = (e) => {
        let select = e.target;
        let name = select.name;
        let workItemType = select.options[select.selectedIndex];
        this.setState({ [name]: workItemType.value })
    }

    exportToExcelClick = () => {
        const fileName = 'ConsumerSearchResults.xlsx';
        const excelSheetName = 'ConsumerSearchResults';

        this.setState({
            loadingExport: true,
            isServerError: false,
            serverErrorMessage: ''
        });

        const Url = this.state.exportURL;

        axios.get(Url,
            {
                responseType: 'arraybuffer'
            })
            .then((response) => {

                const blob = new Blob([response.data], {
                    type: 'application/octet-stream'
                })
                saveAs(blob, fileName);

                this.setState({
                    loadingExport: false,
                    isServerError: false
                });
            })
            .catch((error) => {
                this.setState({
                    loadingExport: false,
                    serverErrorMessage: GetErrorMessageDescription(error),
                    isServerError: true
                });
            });
    }

    doSendReissueRequest = () => {
        let validationMsg = '';

        if (this.state.readOnly === true) {
            if (isTextNullOrEmpty(this.state.rfirstName)) {
                validationMsg = validationMsg + 'Please enter the First Name. ' + '\n';
            }
            if (isTextNullOrEmpty(this.state.rlastName)) {
                validationMsg = validationMsg + 'Please enter the Last Name. ' + '\n';
            }
            if (isTextNullOrEmpty(this.state.raddress1)) {
                validationMsg = validationMsg + 'Please enter the Address. ' + '\n';
            }
            if (isTextNullOrEmpty(this.state.rcity)) {
                validationMsg = validationMsg + 'Please enter the City. ' + '\n';
            }
            if (isTextNullOrEmpty(this.state.rstate)) {
                validationMsg = validationMsg + 'Please enter the State. ' + '\n';
            }
            if (isTextNullOrEmpty(this.state.rzip) || this.state.rzip.length < 5) {
                validationMsg = validationMsg + 'Please enter the valid Zip. ' + '\n';
            }
            if (isTextNullOrEmpty(this.state.rcountry)) {
                validationMsg = validationMsg + 'Please select the Country. ' + '\n';
            }
        }

        if (validationMsg !== '') {
            this.dialog.show({ title: 'Reissue Request', body: validationMsg, actions: [Dialog.OKAction()], bsSize: 'large', onHide: (dialog) => { dialog.hide() } });
            return;
        }

        let input = {
            "consumerSK": this.state.selectedCheckRecord.consumerSk,
            "checkSK": this.state.selectedRecord.checkSK,
            "isModified": this.state.readOnly,
            "businessName": this.state.rbname,
            "firstName": this.state.rfirstName,
            "lastName": this.state.rlastName,
            "address1": this.state.raddress1,
            "address2": this.state.raddress2,
            "city": this.state.rcity,
            "state": this.state.rstate,
            "zipCode": this.state.rzip,
            "country": this.state.rcountry,
            "deceasedFlag": this.state.rDeceased
        }
        this.state.reissueConsumerNameSaved = this.state.rfirstName.trim() + ' ' + this.state.rlastName.trim();
        if (this.state.reissueConsumerNameSaved.trim() === '') {
            this.state.reissueConsumerNameSaved = this.state.rbname;
        }
        this.sendReissueRequest(this.state.selectedCheckRecord.consumerSk, input);
    }
    handleSuccessVoid = () => {
        this.setState({ isSuccessPanelPopupOpened: false })
    }
    handleRessiue = () => {
        this.setState({ isReissuePanelPopupOpened: false })
    }
    consumerReissueRecordOnClick = (item) => {

        this.setState({
            rfirstName: '',
            rlastName: '',
            rbname: '',
            rDeceased: false,
            raddress1: '',
            raddress2: '',
            rcity: '',
            rstate: '',
            rzip: '',
            rcountry: '',
            readOnly:false
        })

        this.fetchConsumerRecordDetails(item.consumerSk, item.checkSK);
        this.setState({
            isReissuePanelPopupOpened: true,
            selectedCheckRecord: {},
            selectedRecord: item
        })

    }
    handleClose = () => {
        this.setState({ isConsumerRecordPopupOpened: false })
    }
    consumerRecordOnClick = (item) => {
        this.setState({
            isConsumerRecordPopupOpened: true,
            selectedRecord: item
        })
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.dataFromParent) {
            let searchInput = nextProps.dataFromParent;
            let countryList = nextProps.countryList;
            let reissueReasonList = nextProps.reissueReasonList;
            if (countryList) {
                this.setState({
                    countryList: countryList,
                    reissueReasonList: reissueReasonList
                });
            }

            this.state.resultsToShow = [];
            this.state.resultsToHideExpansion = [];
            this.setState({
                loading: true,
                total:0
            });
            this.fetchSearchResult(searchInput, 0, this.state.pageSize, this.state.columnKey, this.state.columnSortDirection);
        }
    }
    fetchSearchResult = (input, offset, limit, sortColumn, sortDirection) => {
        this.setState({
            isServerError: false,
            serverErrorMessage: ''
        });

        let urlSuffix = ``;
        let urlBasePrefix = ``;
        if (input.isAdvanceSearch === true) {
            urlBasePrefix = `ConsumerSearch/AdvanceSearch`;
            urlSuffix = `?field1=${input.field1}&condition1=${input.condition1}&value1=${encodeURIComponent(input.value1)}&suffix1=${input.suffix1}`;
            urlSuffix = urlSuffix + `&field2=${input.field2}&condition2=${input.condition2}&value2=${encodeURIComponent(input.value2)}&suffix2=${input.suffix2}`;
            urlSuffix = urlSuffix + `&field3=${input.field3}&condition3=${input.condition3}&value3=${encodeURIComponent(input.value3)}&suffix3=${input.suffix3}`;
            urlSuffix = urlSuffix + `&offset=${offset}&limit=${limit}&sort=${sortDirection + sortColumn}`;
        }
        else {
            urlBasePrefix = `ConsumerSearch`;
            urlSuffix = `?searchText=${encodeURIComponent(input.inputText)}&offset=${offset}&limit=${limit}&sort=${sortDirection + sortColumn}`;
        }
        let urlSuffixForExport = urlBasePrefix + `/ExportResults` + urlSuffix;
        urlSuffix = urlBasePrefix + urlSuffix;

        const Url = getClientServiceURL(urlSuffix);
        this.state.exportURL = getClientServiceURL(urlSuffixForExport);
        document.getElementById('divConsmGrd1').scrollIntoView();

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                const payload = res.data.payLoad;
                const total = res.data.total;

                let resultListHide = this.state.resultsToHideExpansion;
                let resultList = [];
                let arrayLength = payload.length;
                for (let i = 0; i < arrayLength; i++) {
                    resultList.push(payload[i]);
                    if (payload[i].expandable === false) {
                        const counter = resultList.length - 1;
                        resultListHide.push(payload[i].consumerSk);
                    }
                }
                this.setState({
                    searchInput: input,
                    results: resultList,
                    resultsToShow: resultList,
                    next: resultList.length,
                    resultsToHideExpansion: resultListHide,
                    total: total,
                    loading: false
                })

                if (total <= 0) {
                    let message = 'No results were found based on your search criteria.';
                    this.dialog.show({ title: 'Consumer Search', body: message, actions: [Dialog.OKAction()], bsSize: 'small', onHide: (dialog) => { dialog.hide() } });
                    return;
                }
                
            })
            .catch((error) => {

                this.setState({
                    loading: false,
                    serverErrorMessage: GetErrorMessageDescription(error),
                    isServerError: true
                });
            });
    }

    fetchConsumerRecordDetails = (consumerSK, checkSK) => {
        this.setState({
            isServerErrorReIssueInfo: false,
            loadingReissueInfoRead: true,
            isServerErrorReIssueSubmit: false,
            loadingReissueSubmit: false,
            serverErrorMessage: ''
        });

        const urlsuffix = `/Consumers/${consumerSK}/DetailsForReissueRequest?checkSK=${checkSK}`;
        const url = getClientServiceURL(urlsuffix);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                    this.setState({
                        selectedCheckRecord: res.data,
                        rfirstName: res.data.firstName,
                        rlastName: res.data.lastName,
                        rbname: res.data.businessName,
                        rDeceased: (res.data.deceasedFlag ===true),
                        raddress1: res.data.address1,
                        raddress2: res.data.address2,
                        rcity: res.data.city,
                        rstate: res.data.state,
                        rzip: res.data.zipCode,
                        rcountry: res.data.country,
                    loadingReissueInfoRead: false
                })

            })
            .catch((error) => {

                this.setState({
                    loadingReissueInfoRead: false,
                    serverErrorMessageReIssueInfo: GetErrorMessageDescription(error),
                    isServerErrorReIssueInfo: true
                });
            });
    }

    sendReissueRequest = (consumerSK, input) => {
        this.setState({
            isServerErrorReIssueSubmit: false,
            loadingReissueSubmit: true,
            serverErrorMessageReIssueInfo:''
        });

        const urlsuffix = `/Consumers/ReIssueRequest`;
        const url = getClientServiceURL(urlsuffix);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .post(url, input, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                this.state.selectedRecord.checkStatus = res.data.checkStatus;
                this.state.selectedRecord.checkStatusCode = res.data.checkStatusCode;
                this.state.selectedRecord.checkStatusDate = res.data.checkStatusDate;
                this.state.selectedRecord.reissueAllowed = res.data.reissueAllowed;
                const tempResults = this.state.resultsToShow;
                this.setState({ resultsToShow: [] })
                this.setState({ isReissuePanelPopupOpened: false, loadingReissueSubmit: false, resultsToShow: tempResults })
                this.setState({ isSuccessPanelPopupOpened: true })
            })
            .catch((error) => {

                this.setState({
                    loadingReissueSubmit: false,
                    serverErrorMessageReIssueInfo: GetErrorMessageDescription(error),
                    isServerErrorReIssueSubmit: true
                });
            });
    }
    componentDidMount = () => {

    }
    getClassNamesFor = (name, isright) => {
        let prefix = 'text-left';
        if (isright === true) prefix = 'text-left';

        if (this.state.columnKey === name) {
            if (this.state.columnSortDirection === '') {
                return prefix + ' ' + 'ascending';
            }
            else {
                return prefix + ' ' + 'descending';
            }
        }
        else {
            return prefix;
        }
    };
    getClassNamesForSort = (name) => {

        if (this.state.columnKey === name) {
            if (this.state.columnSortDirection === '') {
                return 'ascendingmode';
            }
            else {
                return 'descendingmode';
            }
        }
        else {
            return undefined;
        }
    };
    requestSort = (key) => {
        if (this.state.columnKey === key && this.state.columnSortDirection === '') {
            this.state.columnSortDirection = '-';
        }
        else {
            this.state.columnSortDirection = ''
        }
        this.state.columnKey = key;
        let pageSize1 = this.state.pageSize;
        this.state.resultsToShow = [];
        this.state.resultsToHideExpansion = [];
        this.fetchSearchResult(this.state.searchInput, 0, pageSize1, key, this.state.columnSortDirection);
    };
    handleShowMorePosts = () => {
        this.fetchSearchResult(this.state.searchInput, this.state.next, this.state.pageSize, this.state.columnKey, this.state.columnSortDirection);
    };
    onInputCheckedChange = (event) => {
        this.setState({
            [event.target.name]: event.target.checked
        });
    }
    onInputchange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    getExportData = () => {
        let dataTable = [];
        const results = this.state.resultsToShow;
        if (results.length > 0) {
            if (results) {
                for (let i in results) {
                    let obj = {
                        'Consumer Name': results[i].consumerName,
                        'Account#': results[i].accountNum,
                        'Broadridge#': results[i].broadridgeNum,
                        'Remed.#': results[i].remedNumber,
                        'Address1': results[i].address1,
                        'Address2': results[i].address2,
                        'City': results[i].city,
                        'State': results[i].state,
                        'Zipcode': results[i].zipCode,
                        'Check Number': results[i].checkNumber,
                        'Check Date': getFormattedDate(results[i].checkDate),
                        'Check Amount': results[i].checkAmount,
                        'Check Status': results[i].checkStatus,
                        'Check Status Date': getFormattedDate(results[i].checkStatusDate),
                    }
                    dataTable.push(obj);
                }
            }
        }
        return dataTable;
    }
    generateHeader = () => {


        return (
            <>
                <th>
                    <button
                        type="button"
                        onClick={() => this.requestSort('consumerName')}
                        className={this.getClassNamesFor('consumerName', false)}
                    >
                        Consumer Name
            </button>
                </th>

                <th>
                    <button
                        type="button"
                        onClick={() => this.requestSort('accountNum')}
                        className={this.getClassNamesFor('accountNum', false)}
                    >
                        Account#
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('broadridgeNum')}
                        className={this.getClassNamesFor('broadridgeNum', false)}
                    >
                        Broadridge#
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('remedNumber')}
                        className={this.getClassNamesFor('remedNumber', false)}
                    >
                        Remed.#
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('address1')}
                        className={this.getClassNamesFor('address1', false)}
                    >
                        Address 1
                    </button>
                </th>

                <th>
                    <button
                        type="button"
                        onClick={() => this.requestSort('address2')}
                        className={this.getClassNamesFor('address2', false)}
                    >
                        Address 2
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('city')}
                        className={this.getClassNamesFor('city', true)}
                    >
                        City
                 </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('state')}
                        className={this.getClassNamesFor('state', false)}
                    >
                        # State
                    </button>
                </th>


                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('zipCode')}
                        className={this.getClassNamesFor('zipCode', false)}
                    >
                        Zipcode
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('checkNumber')}
                        className={this.getClassNamesFor('checkNumber', false)}
                    >
                        Check Number
                    </button>
                </th>

                <th>
                    <button
                        type="button"
                        onClick={() => this.requestSort('checkDate')}
                        className={this.getClassNamesFor('checkDate', false)}
                    >
                        Check Date
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('checkAmount')}
                        className={this.getClassNamesFor('checkAmount', true)}
                    >
                        Check Amount
                    </button>
                </th>

                <th class="text-left">
                    <button
                        type="button"
                        onClick={() => this.requestSort('checkStatus')}
                        className={this.getClassNamesFor('checkStatus', false)}
                    >
                        Check Status
                    </button>
                </th>

                <th>
                    <button
                        type="button"
                        onClick={() => this.requestSort('checkStatusDate')}
                        className={this.getClassNamesFor('checkStatusDate', false)}
                    >
                        Check Status Date
                    </button>
                </th>
            </>
        );
    }
    dateFormatter = (cell, row) => {
        return (
            <span> { getFormattedDate(cell)}</span>
        );
    }
    currencyFormatter = (cell, row) => {
        return (
            <span> ${ numberWithCommas(cell)}.00</span>
        );
    }
    accNumFormatter = (cell, row) => {
        return (
            <span> <button class="btn btn-link text-left" onClick={() => this.consumerRecordOnClick(row)}> {cell} </button></span>
        );
    }
    checkNumFormatter = (cell, row) => {
        return (
            row.reissueAllowed === true ?
                <span> <button class="btn btn-link text-left" onClick={() => this.consumerReissueRecordOnClick(row)}> {cell} </button></span>
                :
                <span>  {cell} </span>
        );
    }
    leftAlighFormatter = (cell, row) => {
        return (
            <span class="text-left">  {cell} </span>
        );
    }
    generateChildTableData = (row) => {
        if (row.oldCheckList && row.oldCheckList.length > 0) {
            return row.oldCheckList.map((item, index) => {

                return (
                    <>
                        <table className="">
                            <tr key={item.consumerSk} >
                                <td style={{ width: '2%' }}>&nbsp;</td>
                                <td style={{ width:'5%' }}>&nbsp;</td>
                                <td style={{ width:'7%' }}>&nbsp;</td>
                                <td style={{ width:'7%' }}>&nbsp;</td>
                                <td style={{ width: '7%' }}>&nbsp;</td>
                                <td style={{ width: '7%' }}>&nbsp;</td>
                                <td style={{ width: '7%' }}class="text-left" >{item.address1}</td>
                                <td style={{ width: '7%' }}class="text-left">{item.address2}</td>
                                <td style={{ width: '6%' }}class="text-left">{item.city}</td>
                                <td style={{ width: '5%' }}class="text-left">{item.state}</td>
                                <td style={{ width: '5%' }}class="text-left">{item.zipCode}</td>
                                <td style={{ width: '7%' }}class="text-left"> {item.checkNumber}  </td>
                                <td style={{ width: '6%' }}class="text-left">{getFormattedDate(item.checkDate)}</td>
                                <td style={{ width: '7%' }}class="text-left">${numberWithCommas(item.checkAmount)}.00</td>
                                <td style={{ width: '6%' }}class="text-left">{item.checkStatus}</td>
                                <td style={{ width: '9%' }}class="text-left">{getFormattedDate(item.checkStatusDate)}</td>
                            </tr>
                        </table>

                    </>
                )
            })
        }
        else {
            return (null);
        }
    }
    render() {
        const expandRow = {
            renderer: row => (

                this.generateChildTableData(row)
            ),
            nonExpandable: this.state.resultsToHideExpansion,
            showExpandColumn: true,
            expandByColumnOnly: true,

            expandHeaderColumnRenderer: ({ isAnyExpands }) => {
                if (isAnyExpands) {
                    return <span><i class="far fa-minus-square" title="Collapse All"></i></span>;
                }
                return <span><i class="far fa-plus-square" title="Expand All"></i></span>;
            },
            expandColumnRenderer: ({ expanded, rowKey, expandable }) => {
                if (expandable) {
                    if (expanded) {
                        return (
                            <span><i class="far fa-minus-square"></i></span>

                        );
                    }
                    return (
                        <span><i class="far fa-plus-square"></i></span>
                    );
                }
                else {
                    return (undefined);
                }
            },
            onExpandAll: (isExpandAll, rows, e) => {
            }

        };
        const headerSortingStyle = { backgroundColor: '#c8e6c9' };
        const columns = [
            {
                dataField: 'consumerName', text: 'Consumer Name', align: 'left',  headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('consumerName')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('consumerName');
                },
                style: {
                    width: '12%'
                }
            },
            {
                dataField: 'accountNum', text: 'Account#', align: 'left', headerAlign: 'left', formatter: this.accNumFormatter,
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('accountNum')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('accountNum');
                },
                style: {
                    width: '7%'
                }
            },
            {
                dataField: 'broadridgeNum',text: 'Broadridge#', align: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('broadridgeNum')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('broadridgeNum');
                },
                style: {
                    width: '7%'
                }
            },
            {
                dataField: 'remedNumber', text: 'Remed.#', align: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('remedNumber')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('remedNumber');
                },
                style: {
                    width: '7%'
                }
            },
            {
                dataField: 'address1',text: 'Address 1',align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('address1')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('address1');
                },
                style: {
                    width: '7%'
                }
            },
            {
                dataField: 'address2',text: 'Address 2',align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('address2')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('address2');
                },
                style: {
                    width: '7%'
                }
            },
            {
                dataField: 'city',text: 'City',align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('city')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('city');
                },
                style: {
                    width: '6%'
                }
            },
            {
                dataField: 'state',text: 'State',align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('state')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('state');
                },
                style: {
                    width: '5%'
                }
            },
            {
                dataField: 'zipCode',text: 'Zipcode',align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('zipCode')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('zipCode');
                },
                style: {
                    width: '5%'
                }
            },
            {
                dataField: 'checkNumber',text: 'Check Number',formatter: this.checkNumFormatter,align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('checkNumber')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('checkNumber');
                },
                style: {
                    width: '7%'
                }
            },
            {
                dataField: 'checkDate',text: 'Check Date',formatter: this.dateFormatter,align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('checkDate')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('checkDate');
                },
                style: {
                    width: '6%'
                }
            },
            {
                dataField: 'checkAmount',text: 'Check Amount',formatter: this.currencyFormatter,align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('checkAmount')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('checkAmount');
                },
                style: {
                    width: '7%'
                }
            },
            {
                dataField: 'checkStatus',text: 'Check Status',align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('checkStatus')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('checkStatus');
                },
                style: {
                    width: '6%'
                }
            },
            {
                dataField: 'checkStatusDate',text: 'Check Status Date',formatter: this.dateFormatter,align: 'left',headerAlign: 'left',
                headerEvents: {
                    onClick: (e, column, columnIndex) => this.requestSort('checkStatusDate')
                },
                headerClasses: (column, colIndex) => {
                    return this.getClassNamesForSort('checkStatusDate');
                },
                style: {
                    width: '9%'
                }
            }
        ];
        const imagePrint = require('../../../css/images/Print.svg');
        const successImg = require('../../../css/images/sucess.svg');
        const imageExcel = require('../../../css/images/XL.svg');
        return (
            <>
                <div class="row">
                    
                    <div class="col-lg-6">
                        <span>
                            <h1 class="h1-title mb-2"> Search Results &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        {
                            (this.state.searchInput.isAdvanceSearch === false && this.state.loading === false) &&
                            <span>For: {this.state.searchInput.inputText}</span>
                            }
                       </h1></span>
                    </div>
                    {
                        this.state.resultsToShow.length > 0 ?
                            <div class="col-lg-6 text-right">
                        
                                <span>
                                    <button className='btn btn-link' onClick={() => this.exportToExcelClick()}><img src={imageExcel} alt="ExportToExcel" /></button>
                                </span>
                                <span className="ml-2">
                                    <ReactToPrint content={() => this.componentRef}>
                                        <PrintContextConsumer>
                                            {({ handlePrint }) => (
                                                <button className='btn btn-link' onClick={handlePrint}><img src={imagePrint} alt="Print" /></button>
                                            )}
                                        </PrintContextConsumer>
                                    </ReactToPrint>
                                    <PrintConsumerSearchResults results={this.state.resultsToShow} ref={el => (this.componentRef = el)} />
                                </span>
                           
                                </div>
                        :
                            undefined
                    }
                </div>
                <div id="divConsmGrd1" class="tbl-box-heightconsumersearchresults">
                    {(this.state.loading || this.state.loadingExport) && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
                    {this.state.isServerError && <div class="label-error-red"> <span>{this.state.serverErrorMessage}</span></div>}
                    <BootstrapTable class="table table-striped table-bordered dataTable no-footer fixed_headers" keyField='consumerSk' data={this.state.resultsToShow} columns={columns} expandRow={expandRow} class="fixed_headers" />
                </div>
                <div class="row mt-30">
                    <div class="col text-center">
                        {this.state.resultsToShow.length > 0 ?
                            <p>Viewing results 1-{this.state.resultsToShow.length} of {this.state.total}</p>
                            :
                            undefined
                        }

                        {this.state.resultsToShow.length < this.state.total ?
                            <button type="button" class="btn btn-border" onClick={this.handleShowMorePosts}>Load More</button>
                            :
                            undefined
                        }
                    </div>
                </div>
                <ReactBootStrap.Modal
                    show={this.state.isConsumerRecordPopupOpened}
                    onHide={this.handleClose}
                    backdrop="static"
                    keyboard={false}
                    dialogClassName="my-modal-fullscreen"
                    id="tableModal"
                >
                    <ReactBootStrap.Modal.Header closeButton>
                        <ReactBootStrap.Modal.Title>Consumer Search</ReactBootStrap.Modal.Title>
                    </ReactBootStrap.Modal.Header>
                    <ReactBootStrap.Modal.Body>
                        <ConsumerSearchRecordDetails selectedRecord={this.state.selectedRecord} />
                    </ReactBootStrap.Modal.Body>
                </ReactBootStrap.Modal>
                <SlidingPane
                    className="rightSide-sliding-pane"
                    overlayClassName="some-custom-overlay-class"
                    isOpen={this.state.isReissuePanelPopupOpened}
                    title={'Reissue Check #' + this.state.selectedRecord.checkNumber}
                    subtitle=""
                    from='right'
                    width='35%'
                    onRequestClose={() => {
                        this.setState({ isReissuePanelPopupOpened: false });
                    }}
                >
                    <div>
                        {this.state.loadingReissueInfoRead && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
                        {this.state.isServerErrorReIssueInfo && <div class="label-error-red"> <span>{this.state.serverErrorMessageReIssueInfo}</span></div>}
                        <p>Review the consumer information below before reissuing check. If changes required click “Yes” and modify appropriate information.</p>
                        <div class="row justify-content-center">
                            <div class="col-lg-12 ">

                                <div class="form-group row">
                                    <label class="col-sm-10">
                                        Do you need to modify Consumer Details?
			</label>
                                    <div class="col-sm-2">
                                        <div class="form-check">
                                            <input name="readOnly" class="form-check-input" type="checkbox" id="inlineCheckbox1" value={this.state.readOnly} onChange={this.onInputCheckedChange} />
                                            <label class="form-check-label mt-1" for="inlineCheckbox1">Yes</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">First Name</label>
                                    <div class="col-sm-9">
                                        <input disabled={!this.state.readOnly} class="form-control" type="text" maxLength="50" name="rfirstName" value={this.state.rfirstName} onChange={this.onInputchange} />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Last Name</label>
                                    <div class="col-sm-9">
                                        <input disabled={!this.state.readOnly} class="form-control" type="text" maxLength="50" name="rlastName" value={this.state.rlastName} onChange={this.onInputchange} />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Business Name</label>
                                    <div class="col-sm-9">
                                        <input disabled={!this.state.readOnly} class="form-control" type="text" maxLength="50" name="rbname" value={this.state.rbname} onChange={this.onInputchange} />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Deceased</label>

                                    <div class="col-sm-6">
                                        <div class="form-check">



                                                {(this.state.selectedCheckRecord && this.state.selectedCheckRecord.deceasedFlag === true) ?
                                                <input disabled={!this.state.readOnly} class="form-check-input" type="checkbox" id="inlineCheckbox1" name="rDeceased" value={this.state.rDeceased} onChange={this.onInputCheckedChange}  checked/>

                                                    :
                                                <input disabled={!this.state.readOnly} class="form-check-input" type="checkbox" id="inlineCheckbox1" name="rDeceased" value={this.state.rDeceased} onChange={this.onInputCheckedChange} />
                                                }

                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Address 1</label>
                                    <div class="col-sm-9">
                                        <input disabled={!this.state.readOnly} class="form-control" type="text" maxLength="50" name="raddress1" value={this.state.raddress1} onChange={this.onInputchange} />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Address 2</label>
                                    <div class="col-sm-9">
                                        <input disabled={!this.state.readOnly} class="form-control" type="text" maxLength="50" name="raddress2" value={this.state.raddress2} onChange={this.onInputchange} />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">City</label>
                                    <div class="col-sm-9">
                                        <input disabled={!this.state.readOnly} class="form-control" type="text" maxLength="50" name="rcity" value={this.state.rcity} onChange={this.onInputchange} />
                                    </div>
                                </div>
                                <div class="form-group row consumer-removepadding">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label class="col-sm-6">State</label>
                                            <div class="col-sm-6">
                                                <input disabled={!this.state.readOnly} class="form-control" type="text" maxLength="50" name="rstate" value={this.state.rstate} onChange={this.onInputchange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label class="col-sm-6 text-right">Zip</label>
                                            <div class="col-sm-6">
                                                <input disabled={!this.state.readOnly} class="form-control" type="text" maxLength="15" name="rzip" value={this.state.rzip} onChange={this.onInputchange} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Country</label>
                                    <div class="col-sm-9">
                                        <select disabled={!this.state.readOnly} name="rcountry" class="form-control" id="exampleFormControlSelectCountry" value={this.state.rcountry} onChange={this.onRowDropdownChangEvent}>
                                            {this.state.countryList && this.state.countryList.map((row) => <option value={row.key}>{row.text}</option>)}
                                        </select>
                                    </div>
                                </div>
                                {this.state.loadingReissueSubmit && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
                                {this.state.isServerErrorReIssueSubmit && <div class="label-error-red"> <span>{this.state.serverErrorMessageReIssueInfo}</span></div>}


                                <div class="row text-center mt-5 ml-5">
                                    <div class="col-lg-12">
                                        <button class="btn btn-primary col-sm-6" style={{ width: '180px' }} onClick={this.doSendReissueRequest}>Request Reissue</button>
                                        <label class="ml-2" style={{ width: '150px' }} >&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12 mt-2">
                                        <button class="btn btn-border col-sm-6" style={{ width: '180px' }} onClick={this.handleRessiue}>Cancel</button>
                                        <label class="ml-2" style={{ width: '150px' }} >&nbsp;</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </SlidingPane>
                <SlidingPane
                    className="rightSide-sliding-pane"
                    overlayClassName="some-custom-overlay-class"
                    isOpen={this.state.isSuccessPanelPopupOpened}
                    title={'Success'}
                    subtitle=""
                    from='right'
                    width='20%'
                    onRequestClose={() => {
                        this.setState({ isSuccessPanelPopupOpened: false });
                    }}
                >
                    <div>

                        <div class="row justify-content-center">
                            <div class="col-lg-12 ">
                                <p class="mt-1 text-center"><img style={{ width: '50px' }} src={successImg} alt='Success' /></p>
                                <p class="succs-text text-center">Your check reissue request was received.</p>

                                <div class="form-group column">
                                    <label class="col-sm-12 mt-3">Remediation Number</label>
                                    <div class="col-sm-12">
                                        <div class="ml-2">{this.state.selectedRecord.remedNumber}</div>
                                    </div>
                                </div>
                                <div class="form-group column">
                                    <label class="col-sm-12">Consumer Name</label>
                                    <div class="col-sm-12">
                                        <div class="ml-2">{this.state.reissueConsumerNameSaved}</div>
                                    </div>
                                </div>
                                <div class="form-group column">
                                    <label class="col-sm-12">Address Where Check Should be Mailed</label>
                                    <div class="col-sm-12">
                                        <div class="ml-2">{this.state.raddress1 + ', ' + this.state.raddress2 + ', ' + this.state.rcity + ', ' + this.state.rstate}</div>
                                    </div>
                                </div>
                                <div class="form-group column">
                                    <label class="col-sm-12">Check Number</label>
                                    <div class="col-sm-12">
                                        <div class="ml-2">{this.state.selectedRecord.checkNumber}</div>
                                    </div>
                                </div>

                                <div class="form-group column">
                                    <label class="col-sm-12">Check Amount</label>
                                    <div class="col-sm-12">
                                        <div class="ml-2">${this.state.selectedRecord.checkAmount}.00</div>
                                    </div>
                                </div>

                                <div class="form-group column">
                                    <label class="col-sm-12">Date of Submission</label>
                                    <div class="col-sm-12">
                                        <div class="ml-2">{getFormattedCurrentDate()}</div>
                                    </div>
                                </div>
                                <div class="row text-center mt-5">
                                    <div class="col-lg-12 mt-2">
                                        <button class="btn btn-border col-sm-6" style={{ width: '180px' }} onClick={this.handleSuccessVoid}>Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </SlidingPane>
                <Dialog ref={(component) => { this.dialog = component }} />
            </>
        );
    }
}
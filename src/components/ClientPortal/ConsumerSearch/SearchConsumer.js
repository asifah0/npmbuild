﻿import axios from 'axios';
import React, { Component } from 'react';
import Dialog from 'react-bootstrap-dialog';
import * as ReactBootStrap from 'react-bootstrap';

import { showMsg } from '../../Shared/Util/CommonFunctions';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';

export class SearchConsumer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            clientId: '',
            txtName: '',
            results: [],
            filteredResults: [],
            loading: false,
            isServerError: false,
            inprocessclicked: true,
            mailedclicked: false,
            isAdvanceSearchPaneOpen: false,

            lookupFieldNameOptions: [],

            fieldname1conditions: [],
            fieldname2conditions: [],
            fieldname3conditions: [],

            fieldname1lookup: [],
            fieldname2lookup: [],
            fieldname3lookup: [],

            fieldname1: '',
            fieldcondition1: '',
            fieldvalue1: '',
            fieldsuffix1: '',

            fieldname2: '',
            fieldcondition2: '',
            fieldvalue2: '',
            fieldsuffix2: '',

            fieldname3: '',
            fieldcondition3: '',
            fieldvalue3: '',
        };
    }
    advanceSearchOnClick = (e) => {
        this.setState({ isAdvanceSearchPaneOpen: true })
    }
    isTextNullOrEmpty = (text) => {
        if (text === null || text === undefined || text.trim() === '') {
            return (true);
        }
        else {
            return (false);
        }
    }
    hideAdvanceSearch = (e) => {
        this.setState({ isAdvanceSearchPaneOpen: false })
    }
    componentDidMount = () => {
        this.setState({

            loading: true
        });

        this.fetchAdvSearchLookupInfo();

    }
    fetchAdvSearchLookupInfo = () => {
        this.setState({
            loading: true,
            isServerError: false
        });
        
        const urlSuffix = `ConsumerSearch/Lookup`;
        const Url = getClientServiceURL(urlSuffix);

        if (this.cancel) {
            this.cancel.cancel();
        }
        this.cancel = axios.CancelToken.source();
        axios
            .get(Url, {
                cancelToken: this.cancel.token,
            })
            .then((res) => {
                this.setState({
                    lookupFieldNameOptions: res.data.lookupList,
                    loading: false
                })
                this.props.onSetLookupParams(res.data.countryList, res.data.reissueReasonList);
            })
            .catch((error) => {
                if (axios.isCancel(error) || error) {
                }

                this.setState({
                    loading: false,
                    isServerError: true
                });
            });
    }
    handleSearchOnClick = () => {
        if (this.isTextNullOrEmpty(this.state.txtSearchInputText)) {
            showMsg(this.dialog, 'Consumer Search', 'Please enter the search text.');
            return;
        }
        this.clearRow1OnClickEvent();
        this.clearRow2OnClickEvent();
        this.clearRow3OnClickEvent();
        this.setState({ isAdvanceSearchPaneOpen: false })
        let input = { isAdvanceSearch: false, inputText: this.state.txtSearchInputText };
        this.props.onSimpleSearch(input);
    }
    handleAdvSearchOnClickEvent = () => {
        let bRow1Empty = this.isTextNullOrEmpty(this.state.fieldname1);
        let bRow2Empty = this.isTextNullOrEmpty(this.state.fieldname2);
        let bRow3Empty = this.isTextNullOrEmpty(this.state.fieldname3);

        if (bRow1Empty === true && bRow2Empty === true && bRow3Empty === true) {
            showMsg(this.dialog, 'Consumer Search', 'Please enter the search crierita.');
            return;
        }
        let counter = 0;
        let verifiedRows = 0;
        if (bRow1Empty === false) counter++;
        if (bRow2Empty === false) counter++;
        if (bRow3Empty === false) counter++;

        let input = { isAdvanceSearch: true, row1: false, row2: false, row3: false, field1: '', value1: '', condition1: '', suffix1: '', field2: '', value2: '', condition2: '', suffix2: '', field3: '', value3: '', condition3: '', suffix3: '' };
        

        if (bRow1Empty === false) {
            let bRowConditionEmpty = this.isTextNullOrEmpty(this.state.fieldcondition1);
            let bRowValueEmpty = this.isTextNullOrEmpty(this.state.fieldvalue1);
            let bRo1SuffixEmpty = this.isTextNullOrEmpty(this.state.fieldsuffix1);

            if (bRowConditionEmpty === true) {
                showMsg(this.dialog, 'Consumer Search', 'Please enter the Filter.');
                return;
            }
            if (bRowValueEmpty === true) {
                showMsg(this.dialog, 'Consumer Search', 'Please enter the Value.');
                return;
            }
            if (counter > 1) {
                if (bRo1SuffixEmpty === true) {
                    showMsg(this.dialog, 'Consumer Search', 'Please enter the field (And/Or).');
                    return;
                }
            }
            verifiedRows++;

            input.row1 = true;
            input.field1 = this.state.fieldname1;
            input.value1 = this.state.fieldvalue1;
            input.condition1 = this.state.fieldcondition1;
            input.suffix1 = this.state.fieldsuffix1;
        }
        if (bRow2Empty === false) {
            let bRowConditionEmpty = this.isTextNullOrEmpty(this.state.fieldcondition2);
            let bRowValueEmpty = this.isTextNullOrEmpty(this.state.fieldvalue2);
            let bRo1SuffixEmpty = this.isTextNullOrEmpty(this.state.fieldsuffix2);

            if (bRowConditionEmpty === true) {
                showMsg(this.dialog, 'Consumer Search', 'Please enter the Filter.');
                return;
            }
            if (bRowValueEmpty === true) {
                showMsg(this.dialog, 'Consumer Search', 'Please enter the Value.');
                return;
            }
            const remaining = counter - verifiedRows;
            if (counter > 1 && remaining > 1) {
                if (bRo1SuffixEmpty === true) {
                    showMsg(this.dialog, 'Consumer Search', 'Please enter the field (And/Or).');
                    return;
                }
            }

            input.row2 = true;
            input.field2 = this.state.fieldname2;
            input.value2 = this.state.fieldvalue2;
            input.condition2 = this.state.fieldcondition2;
            input.suffix2 = this.state.fieldsuffix2;
        }
        if (bRow3Empty === false) {
            let bRowConditionEmpty = this.isTextNullOrEmpty(this.state.fieldcondition3);
            let bRowValueEmpty = this.isTextNullOrEmpty(this.state.fieldvalue3);

            if (bRowConditionEmpty === true) {
                showMsg(this.dialog, 'Consumer Search', 'Please enter the Filter.');
                return;
            }
            if (bRowValueEmpty === true) {
                showMsg(this.dialog, 'Consumer Search', 'Please enter the Value.');
                return;
            }

            input.row3 = true;
            input.field3 = this.state.fieldname3;
            input.value3 = this.state.fieldvalue3;
            input.condition3 = this.state.fieldcondition3;
            input.suffix3 = 'na';
        }
        this.setState({ txtSearchInputText: '' })
        this.props.onSimpleSearch(input);
    }
    clearRow1OnClickEvent = () => {
        this.setState({ fieldname1: '' })
        this.setState({ fieldcondition1: '' })
        this.setState({ fieldvalue1: '' })
        this.setState({ fieldsuffix1: '' })

        this.setState({
            fieldname1conditions: [],
            fieldname1lookup: []
        })
    }
    clearRow2OnClickEvent = () => {
        this.setState({ fieldname2: '' })
        this.setState({ fieldcondition2: '' })
        this.setState({ fieldvalue2: '' })
        this.setState({ fieldsuffix2: '' })

        this.setState({
            fieldname2conditions: [],
            fieldname2lookup: []
        })
    }
    clearRow3OnClickEvent = () => {
        this.setState({ fieldname3: '' })
        this.setState({ fieldcondition3: '' })
        this.setState({ fieldvalue3: '' })

        this.setState({
            fieldname3conditions: [],
            fieldname3lookup: []
        })
    }
    onRowDropdownChangEvent = (e) => {
        let select = e.target;
        let name = select.name;
        let workItemType = select.options[select.selectedIndex];
        this.setState({ [name]: workItemType.value })

        if (name === 'fieldname1' || name === 'fieldname2' || name === 'fieldname3') {
            const details = this.getFilterConditionList(workItemType.value);
            if (details.length === 0) {
                showMsg('empty for ' + workItemType.value);
            }
            const propertyName = name + 'conditions';
            const propertyNameLookup = name + 'lookup';
            let lookupList = [];
            if (details.lookupValues) {
                lookupList = details.lookupValues;
            }
            this.setState({
                [propertyName]: details.fieldConditions,
                [propertyNameLookup]: lookupList
            });
        }
    }
    getFilterConditionList = (value) => {
        let result = [];
        const list = this.state.lookupFieldNameOptions;
        for (let index = 0; index < list.length; index++) {
            if (list[index].fieldName === value) {
                result = list[index];
                break;
            }
        }
        return (result);
    }
    onInputchange = (event) => {
        const value = event.target.value;
        const regex = /^[a-z\d\s]+$/i;
        if (value.match(regex) || value === "") {
            this.setState({ [event.target.name]: event.target.value });
        }
    }
    onConSearchKeyPress = (e) => {        
        let regex = /^[a-z\d\s]+$/i;
        if (!regex.test(e.key)) {
            e.preventDefault();
        }
    }
   
    render() {
        const imageRemove = require('../../../css/images/close-btn.png');
        return (
            <>
                <div className="col-lg-5">
                    <div className="box box-border box-heightconsumerSearch">
                        <div class="col">
                            <div>
                                <h1 class="h1-title">Search</h1>
                            </div>
                            <label class="mt-0">Search by Consumer Name</label>
                            <div id="custom-search-input">
                                <div class="input-group">

                                    <input name="txtSearchInputText" maxLength="50" type="text" class="  search-query form-control" value={this.state.txtSearchInputText}
                                        onChange={this.onInputchange} onKeyPress={ this.onConSearchKeyPress} />
                                    <span class="input-group-btn">
                                        <button class="btn btn-secondary s-icon" type="button" onClick={this.handleSearchOnClick}>
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="mt-4 mb-4">
                                <button type="button" class="btn btn-primary menu-button" onClick={this.advanceSearchOnClick}>Advanced Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-5">
                    {this.state.loading && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
                    {this.state.isServerError && <div class="label-error-red ml-3"> <span>Unable to get the data due to internal server error. Please try after sometimes.</span></div>}
                </div>
                <div className="col-lg-12" style={this.state.isAdvanceSearchPaneOpen ? {} : { display: 'none' }}>
                    <div class="row mt-0 cusmr-advSearch">
                        <div class="col-lg-12">
                            <div role="alert" aria-live="assertive" aria-atomic="true">
                                <div>
                                    <button type="button" class="ml-2 mr-2 mb-1 close" aria-label="Close" onClick={this.hideAdvanceSearch}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="toast-body box gray-bg mt-1">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label for="">Consumer Attribute</label>
                                            <select class="form-control" name="fieldname1" value={this.state.fieldname1} onChange={this.onRowDropdownChangEvent}>
                                                {this.state.lookupFieldNameOptions.map((row) => <option value={row.fieldName}>{row.fieldText}</option>)}
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">Filter</label>
                                            <select class="form-control" name="fieldcondition1" value={this.state.fieldcondition1} onChange={this.onRowDropdownChangEvent}>
                                                {this.state.fieldname1conditions.map((row) => <option value={row.key}>{row.text}</option>)}
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">Value</label>
                                            {
                                                this.state.fieldname1lookup.length === 0 ?
                                                    <input type="text" class="form-control" maxLength="50" name="fieldvalue1" value={this.state.fieldvalue1} onChange={this.onInputchange} />
                                                    :
                                                    <select class="form-control" name="fieldvalue1" value={this.state.fieldvalue1} onChange={this.onRowDropdownChangEvent}>
                                                        {this.state.fieldname1lookup.map((row) => <option value={row.key}>{row.text}</option>)}
                                                    </select>
                                            }
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">And/Or</label>
                                            <select class="form-control" name="fieldsuffix1" value={this.state.fieldsuffix1} onChange={this.onRowDropdownChangEvent}>
                                                <option></option>
                                                <option>And</option>
                                                <option>Or</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn mt-4 btn-border" onClick={this.clearRow1OnClickEvent}>
                                                {/*<i class="fas fa-eraser"></i>*/}
                                                Clear
                                                </button>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-2">
                                            <label for="">Consumer Attribute</label>
                                            <select class="form-control" name="fieldname2" value={this.state.fieldname2} onChange={this.onRowDropdownChangEvent}>
                                                {this.state.lookupFieldNameOptions.map((row) => <option value={row.fieldName}>{row.fieldText}</option>)}
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">Filter</label>
                                            <select class="form-control" name="fieldcondition2" value={this.state.fieldcondition2} onChange={this.onRowDropdownChangEvent}>
                                                {this.state.fieldname2conditions.map((row) => <option value={row.key}>{row.text}</option>)}
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">Value</label>
                                            {
                                                this.state.fieldname2lookup.length === 0 ?
                                                    <input type="text" class="form-control" maxLength="50" name="fieldvalue2" value={this.state.fieldvalue2} onChange={this.onInputchange} />
                                                    :
                                                    <select class="form-control" name="fieldvalue2" value={this.state.fieldvalue2} onChange={this.onRowDropdownChangEvent}>
                                                        {this.state.fieldname2lookup.map((row) => <option value={row.key}>{row.text}</option>)}
                                                    </select>
                                            }
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">And/Or</label>
                                            <select class="form-control" name="fieldsuffix2" value={this.state.fieldsuffix2} onChange={this.onRowDropdownChangEvent}>
                                                <option></option>
                                                <option>Or</option>
                                                <option>And</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn mt-4 btn-border" onClick={this.clearRow2OnClickEvent}>
                                                {/*<i class="fas fa-eraser"></i>*/}
                                                Clear</button>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-2">
                                            <label for="">Consumer Attribute</label>
                                            <select class="form-control" name="fieldname3" value={this.state.fieldname3} onChange={this.onRowDropdownChangEvent}>
                                                {this.state.lookupFieldNameOptions.map((row) => <option value={row.fieldName}>{row.fieldText}</option>)}
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">Filter</label>
                                            <select class="form-control" name="fieldcondition3" value={this.state.fieldcondition3} onChange={this.onRowDropdownChangEvent}>
                                                {this.state.fieldname3conditions.map((row) => <option value={row.key}>{row.text}</option>)}
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="">Value</label>
                                            {
                                                this.state.fieldname3lookup.length === 0 ?
                                                    <input type="text" class="form-control" maxLength="50" name="fieldvalue3" value={this.state.fieldvalue3} onChange={this.onInputchange} />
                                                    :
                                                    <select class="form-control" name="fieldvalue3" value={this.state.fieldvalue3} onChange={this.onRowDropdownChangEvent}>
                                                        {this.state.fieldname3lookup.map((row) => <option value={row.key}>{row.text}</option>)}
                                                    </select>
                                            }
                                        </div>
                                        <div class="col-sm-2">

                                        </div>
                                        <div class="col-sm-1" >                                            
                                            <button class="btn mt-4 btn-border" onClick={this.clearRow3OnClickEvent}>
                                                {/*<i class="fas fa-eraser"></i>*/}
                                                Clear</button>                                            
                                        </div>
                                        <div class="col-sm-3 mt-4" >                                                                                        
                                                <button class="btn btn-primary menu-button" type="button" onClick={this.handleAdvSearchOnClickEvent}>
                                                    {/*<i class="fas fa-search">&nbsp;Search</i>*/}
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Dialog ref={(el) => { this.dialog = el }} />
            </>
        );
    }
}
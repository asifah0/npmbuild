﻿import React, { Component} from 'react';
import { Container } from 'reactstrap';

import { SearchConsumer } from './SearchConsumer';
import { SearchConsumerResults } from './SearchConsumerResults';

export class ConsumerSearchManager extends Component {

    constructor(props) {
        super(props);
            this.state = {
                searchInput: ''
                };
        
    }
    handleSearch = (input) => {
        this.setState({
            searchInput: input
        });
    };

    handleLookupList = (countryList, reasonList) => {
        this.setState({
            lkCountryList: countryList,
            lkReasonList: reasonList
        });
    };

    componentWillReceiveProps(nextProps) {
    }
    render() {
        return (
            <Container fluid className="content-section">
                <h1 className="h2-title bold">Consumer Search</h1>
                <div class="row mt-2 mb-0">
                    <SearchConsumer onSimpleSearch={this.handleSearch} onSetLookupParams={this.handleLookupList} />
                </div>
                <div className="row mt-2">
                    <div className="col-lg-12">
                        <div className="box box-border consumer">
                            <SearchConsumerResults dataFromParent={this.state.searchInput} countryList={this.state.lkCountryList} reissueReasonList={this.state.lkReasonList} />
                        </div>
                    </div>
                </div>
            </Container>
        );
    }
}

﻿import React from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import { getClientServiceURL } from '../../Shared/Util/GlobalUtils';
import { useFetch } from '../../Shared/Services/GetServiceCall';
import { numberWithCommas } from '../../Shared/Util/CommonFunctions';
import { getFormattedDate } from '../../Shared/Util/CommonFunctions';


const ConsumerSearchRecordDetails = (props) => {
    const urlsuffix = `/Consumers/${props.selectedRecord.consumerSk}`;
    const url = getClientServiceURL(urlsuffix);
    const { status, results, remedDocs } = useFetch(url);
    const loading = (status === 'fetching');
    const success = (status === 'fetched');
    const isError = (status === 'error');
    const deceased = (results && results.consumerSk) ? (results.deceasedFlag === true) : false;

    const addressLine2 = (results && results.consumerSk) ? (results.address2 + ', ' + results.city + ', ' + results.state + ', ' + results.zipCode) : '';

    const generateTableData = () => {
        return results.oldCheckList.map((item, index) => {

            return (
                <>
                    <tr key={item.remedSk} >
                        <td class="text-left">{item.checkNumber}</td>
                        <td class="text-left">{getFormattedDate(item.checkDate)}</td>
                        <td class="text-left">{item.checkStatus}</td>
                        <td class="text-left">{getFormattedDate(item.checkStatusDate)}</td>
                        <td class="text-left">${numberWithCommas(item.checkAmount)}.00</td>
                        <td class="text-left"><i className='far fa-file-alt'></i></td>
                        
                    </tr>

                </>
            )
        })
    }
    


    return (
        <>
            {loading && <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div>}
            {isError && <div class="label-error-red ml-3"> <span>Unable to display the data due to internal server error. Please try after sometimes.</span></div>}

            <div class="row mt-1 cusmr-Search">
                <div class="col-lg-12">

                    <div class="toast-body ">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="gray-bg cums-box-height box-noBdr">
                                    <div class="form-group row">
                                        <label class="col-sm-6">Consumer Name</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.consumerName : ''}
                                                    </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">Account #</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.accountNum : ''}
                                                    </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">BR Account #</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.accountNumBR : ''}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">Deceased Flag</label>
                                        <div class="col-sm-6">
                                            {deceased === true ?
                                                <input disabled class="form-check-input ml-1" type="checkbox" value="true" id="defaultCheck1" checked />
                                                :
                                                <input disabled class="form-check-input ml-1" type="checkbox" value="true" id="defaultCheck1" />
                                            }
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">Address</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.address1 : ''}<br />
                                            {addressLine2}
                                                    </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">Country</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.country : ''}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">Remed. #</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.remedNumber : ''}
                                                    </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">Remediation Name</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.remediationName : ''}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">BR Remed. #</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.broadridgeNum : ''}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">Check Stub</label>
                                        <div class="col-sm-6">
                                            {results && results.consumerSk ? results.checkStub : ''}
                                                    </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6">Payment Amount</label>
                                        <div class="col-sm-6">
                                            ${results && results.consumerSk ? results.paymentAmount : '0' }.00
                                                    </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="gray-bg cums-box1 box-noBdr">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Check # </th>
                                                <th scope="col">Check Date</th>
                                                <th scope="col">Check Status</th>
                                                <th scope="col">Status Date</th>
                                                <th scope="col">Check Amount </th>
                                                <th scope="col">Check Image</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {results && results.consumerSk ?
                                                generateTableData()

                                                :
                                                <div></div>}
                                          </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default ConsumerSearchRecordDetails;

                                
                                
﻿import React, { Component } from "react";

import { getFormattedDate } from '../../Shared/Util/CommonFunctions';

export default class PrintConsumerSearchResults extends Component {
    render() {
        return (
            <table id="clientlist" className="printContent">
                <thead>
                    <tr>
                        <th class="text-left">Consumer Name</th>
                        <th class="text-left">Account#</th>
                        <th class="text-left">Broadridge#</th>
                        <th class="text-left">Remed.#</th>
                        <th class="text-left">Address1</th>
                        <th class="text-left">Address2</th>
                        <th class="text-left">City</th>
                        <th class="text-left">State</th>
                        <th class="text-left">Zipcode</th>
                        <th class="text-left">Check Number</th>
                        <th class="text-left">Check Date</th>
                        <th class="text-left">Check Amount</th>
                        <th class="text-left">Check Status</th>
                        <th class="text-left">Check Status Date</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.results.map(result =>
                        <tr key={result.consumerSk}>
                            <td class="text-left">{result.consumerName}</td>
                            <td class="text-left">{result.accountNum}</td>
                            <td class="text-left">{result.broadridgeNum}</td>

                            <td class="text-left">{result.remedNumber}</td>
                            <td class="text-left">{result.address1}</td>
                            <td class="text-left">{result.address2}</td>
                            <td class="text-left">{result.city}</td>
                            <td class="text-left">{result.state}</td>
                            <td class="text-left">{result.zipCode}</td>
                            <td class="text-left">{result.checkNumber}</td>
                            <td class="text-left">{getFormattedDate(result.checkDate)}</td>
                            <td class="text-left">{result.checkAmount}</td>
                            <td class="text-left">{result.checkStatus}</td>
                            <td class="text-left">{getFormattedDate(result.checkStatusDate)}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }
}

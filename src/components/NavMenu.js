import React from 'react';
import { Container, Navbar, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import { useLocation } from "react-router-dom";

export const NavMenu = () => {

    const location = useLocation();
    const { pathname } = location;
    const splitLocation = pathname.split("/");

    const image = require('../css/images/dot-icon.png')

        return (

            <header className="second_header">
                <Navbar className="row nav-bg">
                    <Container fluid>
                    <div className="col-lg-12">
                        <ul className="nav tab-links" id="nav-tab" role="tablist">
                            <li>
                                 <NavLink tag={Link} className="nav-link tab-links-firstChild" to="/clientdashboard"> <img className="d-icon" src={image} alt="legalremediation" /> Legal Remediation</NavLink>
                            </li>

                             <li className={splitLocation[1] === "clientdashboard" ? "active" : ""}>
                                  <NavLink tag={Link}  exact activeClassName="active" to="/clientdashboard"> Dashboard</NavLink>
                             </li>

                             <li className={splitLocation[1] === "remediations" ? "active" : ""}>
                                    <NavLink tag={Link} activeClassName="active" to="/remediations"> All Remediations</NavLink>
                             </li>

                             <li className={splitLocation[1] === "consumersearch" ? "active" : ""}>
                                    <NavLink tag={Link} activeClassName="active" to="/consumersearch"> Consumer Search</NavLink>
                             </li>

                             <li className={splitLocation[1] === "reporting" ? "active" : ""}>
                                    <NavLink tag={Link} activeClassName="active" to="/reporting"> Reporting</NavLink>
                             </li>

                            <li className={splitLocation[1] === "submitfiles" ? "active" : ""}>
                                    <NavLink tag={Link} activeClassName="active" to="/submitfiles"> Submit Files</NavLink>
                            </li>

                        </ul>
                        </div>
                        
                    </Container>
                </Navbar>
            </header>           
        );
    }

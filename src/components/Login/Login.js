import React, { useEffect }  from 'react';
import { useState } from 'react';
import { useNavigate } from "react-router";
import * as ReactBootStrap from 'react-bootstrap';
import PropTypes from 'prop-types';
import jwt from 'jwt-decode'
import CommonDataManager from '../../CommonDataManager';
import { Container } from 'reactstrap';
import axios from 'axios';
import { getServiceURL } from '../Shared/Util/GlobalUtils';
import configData from '../Shared/Util/config.json';


async function validateSession(token) {

    const urlSuffix = `Authentication/ValidateSession`;
    const Url = getServiceURL(urlSuffix);
    return fetch(Url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    })
        .then(data => data.json())
        .catch(function (err) { });
}
async function authenticateFromCode(code) {

    const urlSuffix = `Authentication/Token?code=${encodeURIComponent(code)}`;
    const Url = getServiceURL(urlSuffix);
    return fetch(Url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(data => data.json())
        .catch(function (err) { });
}

export default function Login({ setToken, setTimeoutFlag, flag, authCode, props }) {
    const [tobeChecked, setTobeChecked] = useState(true);
    const [authError, setAuthError] = useState(false);
    const [loading, setLoading] = useState(true);

    const showSessionError = (flag === '1');

    const history = useNavigate();

    const handleExistingSession = async e => {
        setTimeoutFlag(0);
        setTobeChecked(false);

        let oldToken = localStorage.getItem('token');
        if (oldToken) {
            const token = await validateSession(oldToken);
            setLoading(false);

            if (token && token.data) {
                const user = jwt(token.data); // decode your token here
                localStorage.setItem('token', token.data);
                localStorage.setItem('tokendec', JSON.stringify(user));


                const roleInfo = token.roleinfo.sections;


                let commonData = CommonDataManager.getInstance();
                commonData.setTokenEnc(token.data);
                commonData.setTokenObj(token.data);
                commonData.setUserName(user.name);
                commonData.setRoleInfo(roleInfo);
                commonData.setClientID(user.clientid);

                axios.interceptors.request.use(request => {
                    let commonData1 = CommonDataManager.getInstance();
                    request.headers['Authorization'] = 'Bearer ' + commonData1.getTokenEnc();
                    return (request);
                });

                assignInterceptorAction();

                setToken(token);
            }
            else {
                if (token && token.Code && token.Code === 'BR-SESSION-01') {
                    localStorage.removeItem('token');
                    localStorage.removeItem('tokendec');
                    localStorage.clear();
                    let loginURL = configData.LOGIN_URL;
                    window.location.href = loginURL;
                    return;
                }
                else {
                    localStorage.removeItem('token');
                    localStorage.removeItem('tokendec');
                    localStorage.clear();
                    setAuthError(true);
                }
            }
        }
    }
    const assignInterceptorAction = () => {
        axios.interceptors.response.use(
            (response) => { return (response) },
            error => {
                if (error.response && error.response.data.Code) {
                    let errorCode = error.response.data.Code
                    let errorMessage = error.response.data.Message
                    if (errorCode === 'BR-SESSION-01') {
                        localStorage.removeItem('token');
                        localStorage.removeItem('tokendec');
                        localStorage.clear();
                        let commonData = CommonDataManager.getInstance();
                        commonData.setTokenEnc('');
                        commonData.setTokenObj('');
                        commonData.setRoleInfo(null);

                        setTimeoutFlag(1);



                        return Promise.reject(error);
                    }
                    else {
                        return Promise.reject(error);
                    }
                }
                else {
                    return Promise.reject(error);
                }

            }
        );
    }
    const handleTokenRequest = async e => {
        setTimeoutFlag(0);
        setTobeChecked(false);

        if (authCode) {
            const token = await authenticateFromCode(authCode);
            setLoading(false);

            if (token && token.data) {
                const user = jwt(token.data); // decode your token here
                localStorage.setItem('token', token.data);
                localStorage.setItem('tokendec', JSON.stringify(user));


                const roleInfo = token.roleinfo.sections;


                let commonData = CommonDataManager.getInstance();
                commonData.setTokenEnc(token.data);
                commonData.setTokenObj(token.data);
                commonData.setUserName(user.name);
                commonData.setRoleInfo(roleInfo);
                commonData.setClientID(user.clientid);

                axios.interceptors.request.use(request => {
                    let commonData1 = CommonDataManager.getInstance();
                    request.headers['Authorization'] = 'Bearer ' + commonData1.getTokenEnc();
                    return (request);
                });

                assignInterceptorAction();

                setToken(token);

                history.push('/');
            }
            else {
                setAuthError(true);
            }
        }
    }

    useEffect(() => {
        if (tobeChecked === true) {

            if (authCode) {
                handleTokenRequest();
            }
            else {

                if (showSessionError) {
                    setTimeout(function ()
                    {
                        let loginURL = configData.LOGIN_URL;
                        window.location.href = loginURL;
                    }, 2000);
                }
                else {
                    let oldToken = localStorage.getItem('token');
                    if (oldToken) {
                        handleExistingSession();
                    }
                }
            }
        }
    }, []);
    

    return (

        <div>
           
            <br/>
        <Container fluid>
                <div className="login-wrapper mt-3 mb-3" >
                    {showSessionError ? <div class="label-error-red"> <span>Session invalid or expired.</span> <br /><br /></div> : undefined}
                    {authError ? <div class="label-error-red"> <span>Unable to to authenticate due to internal server error.</span> <br /><br /></div> : undefined}
                    {loading ? <div class="ml-3"><ReactBootStrap.Spinner animation="border" /></div> : undefined}
                    {loading && showSessionError === false ? <h1>Please wait.....</h1> : undefined}
                </div>
            </Container>
            <br />
      </div>

    
    )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
};


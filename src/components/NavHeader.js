import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Logout } from '../Logout';

  export class NavHeader extends Component {
   // static displayName = "Broadridge";

      render() {
          const image = require('../css/images/Broadridge-logo.png')
          return (
              <header className="first_header">
                <Navbar className="navbar navbar-expand-lg header-section row">
                    <Container fluid>
                
                            <div className="col-lg-12">

                            <div className="row">
                                <div className="col-lg-2 pt-2"> <img src={image} alt="broadridge" /> </div>
                           

                                <div className="col-lg-10">
                                    <ul className="navbar-nav nav-icons justify-content-end">
                                        <NavItem>
                                            <NavLink tag={Link} className="nav-item dropdown text-align-right" to="/">
                                                <Logout/>
                                            </NavLink>
                                        </NavItem>                           
                                    </ul>
                            </div>
                        </div>
                        </div>
                    </Container>
                </Navbar>
            </header>           
        );
    }
}

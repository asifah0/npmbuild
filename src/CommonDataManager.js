export default class CommonDataManager {

    static myInstance = null;

    _tokenEnc = '';
    _tokenObj = '';

    _roleInfo = '';
    _userName = '';
    _clientID = 'undefined';

    /**
     * @returns {CommonDataManager}
     */
    static getInstance() {
        if (CommonDataManager.myInstance == null) {
            CommonDataManager.myInstance = new CommonDataManager();
        }

        return this.myInstance;
    }

    getClientID() {
        return this._clientID;
    }

    setClientID(clientid) {
        this._clientID = clientid;
    }

    getTokenObj() {
        return this._tokenObj;
    }

    setTokenObj(token) {
        this._tokenObj = token;
    }

    getTokenEnc() {
        return this._tokenEnc;
    }

    setTokenEnc(token) {
        this._tokenEnc = token;
    }

    getRoleInfo() {
        return this._roleInfo;
    }

    setRoleInfo(roleInfo) {
        this._roleInfo = roleInfo;
    }

    getAssignedRole(section) {
        let item = new RoleEntry();
        if (this._roleInfo != null) {
            let arrayLength = this._roleInfo.length;
            for (let i = 0; i < arrayLength; i++) {
                let row = this._roleInfo[i];
                let sectionName = row.sectionName;
                if (section === sectionName) {

                    item.SectionName = sectionName;
                    item.Enable = row.enable;
                    item.AllowEdit = row.allowEdit;
                    item.Visible = row.visible;             
                    break;
                }
            }
        }
        return (item);
    }


    getUserName() {
        return this._userName;
    }

    setUserName(uName) {
        this._userName = uName;
    }
}


export class RoleEntry {
    //{sectionName: "dashboard.upcomingremediation", visible: true, enable: true, allowEdit: true}
    Visible = false;
    Enable = false;

    AllowEdit = false;
    SectionName = "";

}

export const ROLE_DASHBOARD_UPCOMING_RMEDIATION = "dashboard.upcomingremediation";
export const ROLE_DASHBOARD_ACTIVE_REMEDIATION = "dashboard.activeremediation";
export const ROLE_DASHBOARD_TASKS = "dashboard.tasks";
export const ROLE_DASHBOARD_NOTIFICATIONS = "dashboard.notifications";
export const ROLE_CLIENTS_CREATECLIENT = "clients.createclient";
